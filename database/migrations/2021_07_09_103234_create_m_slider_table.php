<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_slider', function (Blueprint $table) {
            $table->integer('m_slider_id',1)->increment()->unsigned();
            $table->integer('m_slider_nomor')->unsigned();
            $table->string('m_slider_title',20)->nullable();
            $table->longText('m_slider_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_slider');
    }
}
