<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\MapGroupMenu;
class AddMenuPurchaseRequest extends Migration
{
    
    public $masteruser = [
        'category' => 'A',
        'sort' => '#',
        'icon' => 'mdi mdi-account-key',
        'menuname' => 'Pengguna',
        //'action' => 'View',
        'urlname' => '/account',
        'routename' => 'account',
        //'method' => 'GET'
    ];

    public $childuser1 = [
        'category' => 'A',
        'sort' => '1',
        'icon' => 'mdi mdi-account-multiple-plus',
        'menuname' => 'Akun Pengguna',
        'urlname' => '/useraccount',
        'routename' => 'user',
    ];

    public $childuser2 = [
        'category' => 'A',
        'sort' => '2',
        'icon' => 'mdi mdi-account-key',
        'menuname' => 'Akses Pengguna',
        'urlname' => '/userakses',
        'routename' => 'userakses',
    ];

    

    
    public function up()
    {
        /// add transaksi menu parent
        // DB::table('master_menus')->insert($this->dataParent);
        DB::table('master_menus')->insert($this->masteruser);
        DB::table('master_menus')->insert($this->childuser1);
        DB::table('master_menus')->insert($this->childuser2);

        //-------------------------------------------------------------------

        $count = DB::table('master_menus')->count();    
        //dd($count);
        // $count = '10';
        for ($i=1; $i <= $count; $i++) { 
            # code...
            $groupmenu                = new MapGroupMenu;
            $groupmenu->id_groups     = 1;
            $groupmenu->id_menus      = $i;
            $groupmenu->allow_view    = true;
            $groupmenu->allow_create  = true;
            $groupmenu->allow_update  = true;
            $groupmenu->allow_delete  = true;
            $groupmenu->allow_import  = false;
            $groupmenu->allow_export  = false;

            $groupmenu->option_view    = true;
            $groupmenu->option_create  = true;
            $groupmenu->option_update  = true;
            $groupmenu->option_delete  = true;
            $groupmenu->option_import  = false;
            $groupmenu->option_export  = false;

            $groupmenu->save();
        }
        //-------------------------------------------------------------------
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      
    }
}
