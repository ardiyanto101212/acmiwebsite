<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapGroupMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_groups_menus', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_groups');
            $table->unsignedBigInteger('id_menus');
            $table->string('key1')->nullable();
            $table->string('key2')->nullable();
            $table->string('value')->nullable();

            $table->boolean('allow_view')->nullable();
            $table->boolean('allow_create')->nullable();
            $table->boolean('allow_update')->nullable();
            $table->boolean('allow_delete')->nullable();
            $table->boolean('allow_import')->nullable();
            $table->boolean('allow_export')->nullable();

            $table->boolean('option_view')->nullable();
            $table->boolean('option_create')->nullable();
            $table->boolean('option_update')->nullable();
            $table->boolean('option_delete')->nullable();
            $table->boolean('option_import')->nullable();
            $table->boolean('option_export')->nullable();

            $table->timestamps();


            // $table->foreign('id_groups')->references('id')->on('master_groups');
            // $table->foreign('id_menus')->references('id')->on('master_menus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_groups_menus');
    }
}
