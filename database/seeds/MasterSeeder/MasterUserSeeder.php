<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;

class MasterUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new User;
        $user->username = 'admin';
        $user->fullname = 'admin';
        $user->email    = 'admin@gmail.com';
        $user->password = bcrypt('123123');
        $user->id_groups= 1; // as admin
        $user->save();

        $user = new User;
        $user->username = 'peserta';
        $user->fullname = 'peserta';
        $user->email    = 'peserta@gmail.com';
        $user->password = bcrypt('123123');
        $user->id_groups= 2; // as peserta
        $user->save();
        
    }
}
