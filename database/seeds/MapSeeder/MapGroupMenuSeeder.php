<?php

use Illuminate\Database\Seeder;
use App\Models\MapGroupMenu;

class MapGroupMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i=1; $i <= 2; $i++) { 
            # code...
            $groupmenu                = new MapGroupMenu;
            $groupmenu->id_groups     = 1;
            $groupmenu->id_menus      = $i;

            $groupmenu->allow_view        = true;
            $groupmenu->allow_create      = true;
            $groupmenu->allow_update      = true;
            $groupmenu->allow_delete      = true;
            $groupmenu->allow_import      = true;
            $groupmenu->allow_export      = true;
            $groupmenu->save();

            $groupmenu                = new MapGroupMenu;
            $groupmenu->id_groups     = 2;
            $groupmenu->id_menus      = $i;

            $groupmenu->allow_view        = false;
            $groupmenu->allow_create      = true;
            $groupmenu->allow_update      = true;
            $groupmenu->allow_delete      = true;
            $groupmenu->allow_import      = true;
            $groupmenu->allow_export      = true;
            $groupmenu->save();

            $groupmenu                = new MapGroupMenu;
            $groupmenu->id_groups     = 3; // user1
            $groupmenu->id_menus      = $i;
            $groupmenu->allow_view    = false;
            $groupmenu->allow_create  = true;
            $groupmenu->allow_update  = true;
            $groupmenu->allow_delete  = true;
            $groupmenu->allow_import  = true;
            $groupmenu->allow_export  = true;
            $groupmenu->save();

            $groupmenu                = new MapGroupMenu;
            $groupmenu->id_groups     = 4; // user2
            $groupmenu->id_menus      = $i;
            $groupmenu->allow_view    = false;
            $groupmenu->allow_create  = true;
            $groupmenu->allow_update  = true;
            $groupmenu->allow_delete  = true;
            $groupmenu->allow_import  = true;
            $groupmenu->allow_export  = true;
            $groupmenu->save();

            $groupmenu                = new MapGroupMenu;
            $groupmenu->id_groups     = 5; // user3
            $groupmenu->id_menus      = $i;
            $groupmenu->allow_view    = false;
            $groupmenu->allow_create  = true;
            $groupmenu->allow_update  = true;
            $groupmenu->allow_delete  = true;
            $groupmenu->allow_import  = true;
            $groupmenu->allow_export  = true;
            $groupmenu->save();

            $groupmenu                = new MapGroupMenu;
            $groupmenu->id_groups     = 6; // user4
            $groupmenu->id_menus      = $i;
            $groupmenu->allow_view    = false;
            $groupmenu->allow_create  = true;
            $groupmenu->allow_update  = true;
            $groupmenu->allow_delete  = true;
            $groupmenu->allow_import  = true;
            $groupmenu->allow_export  = true;
            $groupmenu->save();
        }

    }
}
