<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
  <title>{{ config('app.name', 'Laravel') }} | Login</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/vendors/iconfonts/mdi/css/materialdesignicons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/vendors/css/vendor.addons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/css/shared/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/css/demo_1/style.css') }}">
  <link rel="shortcut icon" href="{{ asset('logo/logo.png') }}">
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  @yield('cssvendor')

</head>

<body>
  
<div class="authentication-theme auth-style_1">
        <div class="row">
            <div class="col-12 logo-section"><a href="#" class="logo"><img
                        src="{{ asset('logo/logo.png') }}" alt="logo"></a></div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-md-7 col-sm-9 col-11 mx-auto">
                <div class="grid">
                    <div class="grid-body">
                        <div class="row">
                            <div class="col-lg-7 col-md-8 col-sm-9 col-12 mx-auto form-wrapper">
								<div class="col-sm-12">
									@if(session('alertprogress'))
										<div class="alert alert-info">{{ session('alertprogress') }}</div>
									@endif
								</div>
								
                                <form class="kt-form" method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group input-rounded"><input type="text" class="form-control" placeholder="Email" name="email"></div>
                                    <div class="form-group input-rounded"><input type="password" class="form-control" placeholder="Password" name="password"></div>
                                 
									<button type="submit" class="btn btn-primary btn-block">Login</button>
                                </form>
                                {{-- <div class="signup-link">
                                    <p>Don't have an account yet?</p><a href="#">Sign Up</a>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="auth_footer">
            <p class="text-muted text-center">Copyright © 2021
                <a href="https://ragdalion.com/" target="_blank">RAGDALION TECHNOLOGY</a>. All rights reserved</p>
        </div>
    </div>
	
  <script src="{{ asset('assets_ripleui/assets/vendors/js/core.js') }}"></script>
  <script src="{{ asset('assets_ripleui/assets/vendors/js/vendor.addons.js') }}"></script>
  <script src="{{ asset('assets_ripleui/assets/vendors/chartjs/Chart.min.js') }}"></script>
  <script src="{{ asset('assets_ripleui/assets/js/script.js') }}"></script>



</body>

</html>