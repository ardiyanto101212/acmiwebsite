@extends('template.ripleui')

@section('title')
Slider
@endsection

@section('cssvendor')

<style>
    .contenttable {
            width: 100%;
            height: auto;
            overflow: auto;
            position: relative;
        }
</style>

@endsection

@section('breadcumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb has-arrow">
      <li class="breadcrumb-item"><a href="#">Slider</a></li>
      {{-- <li class="breadcrumb-item active" aria-current="page">Overview</li> --}}
    </ol>
</nav>
@endsection

@section('content')
<div class="grid">
    <p class="grid-header">Tambah Data</p>
    <div class="grid-body">
        <div class="item-wrapper">
            <div class="row">
                
                <!-------------------- datanya disini ---------------------------->
                <div class="col-sm-12">
                    @if(session('alertprogress'))
                    <div class="alert alert-primary dismissible-alert" role="alert">
                        {{ session('alertprogress') }}<i class="alert-close mdi mdi-close"></i>
                    </div>
                    @endif
                </div>
               
                <div class="col-sm-12 contenttable">
                    <br>
                <form action="{{ route('slider_create_store') }}" method="POST" enctype='multipart/form-data'>
                    @csrf
                    <div class="row mb-3">
                        <div class="col-md-12 mx-auto">
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area"><label for="inputType1">Nomor</label></div>
                                <div class="col-md-9 showcase_content_area">
                                    <input type="number" class="form-control @error('m_slider_nomor') is-invalid @enderror" name="m_slider_nomor" value="{{ old('m_slider_nomor') }}">
                                    @error('m_slider_nomor')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area"><label for="inputType1">Judul</label></div>
                                <div class="col-md-9 showcase_content_area">
                                    <input type="text" class="form-control @error('m_slider_title') is-invalid @enderror" name="m_slider_title" value="{{ old('m_slider_title') }}">
                                    @error('m_slider_title')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area"><label for="inputType1">Gambar</label></div>
                                <div class="col-md-9 showcase_content_area">
                                    <input type="file" class="form-control @error('m_slider_image') is-invalid @enderror" name="m_slider_image">
                                    @error('m_slider_image')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3">
                                    &nbsp;
                                </div>
                                <div class="col-md-9">
                                    <input type="submit" class="btn btn-success form-control text-white" value="Save">
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('jsvendor')

@endsection