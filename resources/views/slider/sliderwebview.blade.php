<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
  <title>@yield('title')</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/vendors/iconfonts/mdi/css/materialdesignicons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/vendors/css/vendor.addons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/css/shared/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/css/demo_1/style.css') }}">
  <link rel="shortcut icon" href="{{ asset('logo/logo.png') }}">
  {{-- <link rel="shortcut icon" href="{{ asset('assets_ripleui/assets/vendors/summernote/dist/summernote-lite.css') }}"> --}}
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/summernote/summernote-lite.css') }}">

  <link rel="stylesheet" href="{{ asset('sweetalert/sweetalert2.min.css') }}">



  
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <body>

{{-- <img src="{{ asset('storage/slider/oV5l08P3JjAIqbUOagdYAMiYh15Gcp4uI0l2qWoE.jpg') }}"> --}}

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="{{ asset('storage/slider/oV5l08P3JjAIqbUOagdYAMiYh15Gcp4uI0l2qWoE.jpg') }}" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="{{ asset('storage/slider/I7E5mKkH6pR7qd3x2gBEyMuQ6Os7m0wVppHcnowR.jpg') }}" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="{{ asset('storage/slider/oV5l08P3JjAIqbUOagdYAMiYh15Gcp4uI0l2qWoE.jpg') }}" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


  <script src="{{ asset('assets_ripleui/assets/vendors/js/core.js') }}"></script>
  <script src="{{ asset('assets_ripleui/assets/vendors/js/vendor.addons.js') }}"></script>
  <script src="{{ asset('assets_ripleui/assets/vendors/chartjs/Chart.min.js') }}"></script>
  <script src="{{ asset('assets_ripleui/assets/js/script.js') }}"></script>
  <!-- summer note -->
  
  <script src="{{ asset('sweetalert/sweetalert2.all.min.js') }}"></script>
  
  <script src="{{ asset('assets_ripleui/assets/summernote/summernote-lite.js') }}"></script>
  {{-- <script src="{{ asset('assets_ripleui/assets/summernote/summernote.min.js') }}"></script> --}}

  {{-- <script src="{{ asset('assets_ripleui/assets/vendors/summernote/dist/summernote-lite.min.js') }}"></script> --}}

  <script>
    $(document).ready(function(){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
    });
  </script>

</body>

</html>