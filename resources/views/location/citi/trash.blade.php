@extends('template.ripleui')

@section('title')
Witel (kota/kab)
@endsection

@section('cssvendor')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css">


<style>
    .contenttable {
            width: 100%;
            height: auto;
            overflow: auto;
            position: relative;
        }
</style>

@endsection

@section('breadcumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb has-arrow">
      <li class="breadcrumb-item"><a href="#">Witel (kota/kab)</a></li>
      {{-- <li class="breadcrumb-item active" aria-current="page">Overview</li> --}}
    </ol>
</nav>
@endsection

@section('content')
<div class="grid">
    <p class="grid-header">List Data Terhapus</p>
    <div class="grid-body">
        <div class="item-wrapper">
            <div class="row">
                
                <!-------------------- datanya disini ---------------------------->
                <div class="col-sm-12">
                    @if(session('alertprogress'))
                    <div class="alert alert-primary dismissible-alert" role="alert">
                        {{ session('alertprogress') }}<i class="alert-close mdi mdi-close"></i>
                    </div>
                    @endif
                </div>
                <div class="col-sm-3">
                    <form action="{{ route('citi_view_trash') }}" method="GET">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="text" class="form form-control" name="search" placeholder="Search Data">
                        </div>
                    </form>
                </div>
                
                <br>
                <br>
                <div class="col-sm-12">
                  {{-- <img src="{{ 'storage/fotokaryawan/ngkZTbZVcxFQvhFk0aodtOxpmA4oeckSCYdYustA.jpg' }}" width="100px"> --}}
                   
                <table class="table table-bordered table-striped" id="example">
                   
                    <thead>
                        <tr>
                            <th style="text-align:center;">Provinsi</th>
                            <th style="text-align:center;">Witel (kota/kab)</th>
                            <th style="text-align:center;width:10%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['pageorder'] as $list)
                        <tr>
                            <td style="vertical-align:middle;">
                                {{ $list->provincename }}
                            </td>
                            <td style="vertical-align:middle;">
                                {{ $list->name }}
                            </td>
                            
                            <td style="vertical-align:middle;text-align:center;">
                                <a class="btn btn-primary btn-sm" href="{{ url('citirestore/?id=').$list->id}}">
                                    Restore
                                </a>
                            </td>
                        </tr>

                        @endforeach
                    </tbody>
                </table>
                <br>
                <div class="col-sm-12">
                    <?php 
                        if($data['totalpage'] > 0){
                            echo $data['pageorder']->appends(request()->all())->links();
                        }  
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('jsvendor')

<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>

<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            responsive: true,
            "paging": false,
            searching: false,
            "bInfo" : false,
            "ordering": false
        } );
     
        new $.fn.dataTable.FixedHeader( table );
    });
    </script>

@endsection