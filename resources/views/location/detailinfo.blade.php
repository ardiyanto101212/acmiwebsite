@extends('template.ripleui')

@section('title')
    Detail Provinsi
@endsection

@section('cssvendor')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css">

<style>
    .contenttable {
            width: 100%;
            height: auto;
            overflow: auto;
            position: relative;
        }
</style>

@endsection

@section('breadcumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb has-arrow">
      <li class="breadcrumb-item"><a href="#">Detail Provinsi</a></li>
      {{-- <li class="breadcrumb-item active" aria-current="page">Overview</li> --}}
    </ol>
</nav>
@endsection

@section('content')
<div class="grid">
    <p class="grid-header">Provinsi Info</p>
    <div class="grid-body">
        <div class="item-wrapper">
            <div class="row">
                
                <!-------------------- datanya disini ---------------------------->
                <div class="col-sm-12">
                    @if(session('alertprogress'))
                    <div class="alert alert-primary dismissible-alert" role="alert">
                        {{ session('alertprogress') }}<i class="alert-close mdi mdi-close"></i>
                    </div>
                    @endif
                </div>
                <div class="col-sm-12">
                    <table class="table table-bordered" id="example">
                   
                        <thead>
                            <tr>
                                <th style="text-align:center;">Provinsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="vertical-align:middle;">
                                    {{ $data['detaildata']->name }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>

            </div>
        </div>
    </div>
</div>


<div class="grid">
    <p class="grid-header">City<span class="btn btn-primary btn-sm mdi mdi-plus float-right" onclick="adddetaillist('{{ $data['detaildata']->id }}')"></span></p>
    <div class="grid-body">
        <div class="item-wrapper">
            <div class="row">
                
                <!-------------------- datanya disini ---------------------------->
                <div class="col-sm-12">
                    <div class="alert alert-primary dismissible-alert dataalertajaxdetail" style="display:none;" role="alert">
                        <span class="alertprogressajaxdetail"></span><i class="alert-close mdi mdi-close"></i>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="detailinfolist">

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="container" >	
	<!-- Modal -->
	<div class="modal fade" id="modaladddetail" role="dialog" data-backdrop="static" tabindex="-1">
  <div class="modal-dialog">
  
	<!-- Modal content-->
		<div class="modal-content">
	
		  <div class="modal-header">
            <b class="text text-primary"><b>Add City</b></b>
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		  
		  
		  </div>		
	  <div class="modal-body" >
		  <div class="card">
			  {{-- <div class="card-header">
                
			  </div> --}}
			  <div class="card-body">
                  <input type="hidden" class="form form-control detailid" value="{{ $data['detaildata']->province_id }}">
				  
                <div class="col-sm-12 contenttable">
                   
                    <div class="col-md-12 mx-auto">
                        <div class="form-group row showcase_row_area">
                            <div class="col-md-3 showcase_text_area"><label for="inputType1">Name</label></div>
                            <div class="col-md-9 showcase_content_area">
                                <input type="text" class="form-control namecity">
                            </div>
                        </div>
                        <div class="form-group row showcase_row_area">
                            <div class="col-md-3">
                                &nbsp;
                            </div>
                            <div class="col-md-9">
                                <span class="btn btn-success form-control text-white" onclick="savedetailproduct('{{ $data['detaildata']->province_id }}')">Save</span>
                            </div>
                            
                        </div>
                    </div>
                </div>

			  </div>
		  </div>
	  </div>
	  </div>
  </div>
   </div>
</div>

@endsection

@section('jsvendor')

<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>

<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            responsive: true,
            "paging": false,
            searching: false,
            "bInfo" : false,
            "ordering": false
        } );
     
        new $.fn.dataTable.FixedHeader( table );    
    });
</script>   

<script>
    $(document).ready(function() {
        detaillist();   
    });
</script>   

<script>

function adddetaillist(idproduct){
    $('#modaladddetail').modal('show');
}

function savedetailproduct(id){
            if($('.namecity').val() != ''){
                var form_data = new FormData();

                form_data.append('province_id', id);
                form_data.append('name', $('.namecity').val());

                    $.ajax({
                            url: "{{ route('location_view_ajaxaddcity') }}",
                            dataType: 'JSON', 
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            beforeSend: function (){
                                    $(".preloader").show();
                                },
                            success: function(x) {
                                //----------------
                                $('#modaladddetail').modal('hide');
                                $('.namecity').val("");

                                detaillist(); 
                                swal("Success", x.alertprogress, "success");
                            },
                            error: function (response) {
                                swal("Warning", 'Try again', "warning");
                            }
                    });
            }else{
                swal("Warning", 'Silakan isi data', "warning");
            }

}

function deletedetail(id){
    swal({
        title: 'Are you sure ?',
        text: "delete this data !",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
    }).then(function (dismiss) {
      if (dismiss.value == true) {
        var form_data = new FormData();
    	form_data.append('id', id);

			$.ajax({
                    url: "{{ route('location_view_ajaxdeletecity') }}",
    				dataType: 'JSON', 
    				cache: false,
    				contentType: false,
    				processData: false,
    				data: form_data,
    				type: 'post',
    				beforeSend: function (){
							$(".preloader").show();
    					},
    				success: function(x) {
                        //----------------
                        detaillist(); 
                        swal("Success", x.alertprogress, "success");
                        // $('.dataalertajaxdetail').show();
                        // $('.alertprogressajaxdetail').html(x.alertprogress);
    				},
    				error: function (response) {
    					swal("Warning", 'Try Again', "warning");
						$(".preloader").hide();
    				}
    		});
      }else{
        swal('Cancelled','Your data is safe','error');
      }
    });
   
}

function detaillist(){

    var id = $('.detailid').val();

    var form_data = new FormData();
    	form_data.append('id', id);

			$.ajax({
                    url: "{{ route('location_view_ajaxgetcity') }}",
    				dataType: 'JSON', 
    				cache: false,
    				contentType: false,
    				processData: false,
    				data: form_data,
    				type: 'post',
    				beforeSend: function (){
							$(".preloader").show();
    					},
    				success: function(x) {
                        //----------------
                        $(document).ready(function() {
                    
                        var table2 = $('#example2').DataTable( {
                                responsive: true,
                                // "paging": false,
                                // searching: false,
                                "bInfo" : false,
                                "ordering": false
                            } );
                        
                            new $.fn.dataTable.FixedHeader( table2 );
                        });
 
                        $('.detailinfolist').html(x.listdata);
    				},
    				error: function (response) {
    					alert('Try Again');
						$(".preloader").hide();
    				}
    		});
}

function reloadpage(){
    location.reload();
}

</script>

@endsection