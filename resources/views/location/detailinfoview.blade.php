<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css">

<table class="table table-bordered" id="example2">
    <thead>
        <tr>
            <th style="vertical-align:middle;text-align:center;">City</th>
            <th style="vertical-align:middle;text-align:center;width:10%;">Action</th>
        </tr>
    </thead>
    <tbody> 
        @foreach($data as $list)
        <tr>
            <td style="vertical-align:middle;text-align:center;">{{ $list->namecity }}</td>
            
            <td style="vertical-align:middle;text-align:center;">
                <span class="btn btn-danger btn-sm" onclick="deletedetail('{{ $list->idcity }}')">Delete</span>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
