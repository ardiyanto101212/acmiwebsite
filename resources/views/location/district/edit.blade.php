@extends('template.ripleui')

@section('title')
STO (Distrik)
@endsection

@section('cssvendor')

<style>
    .contenttable {
            width: 100%;
            height: auto;
            overflow: auto;
            position: relative;
        }
</style>

@endsection

@section('breadcumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb has-arrow">
      <li class="breadcrumb-item"><a href="#">STO (Distrik)</a></li>
      {{-- <li class="breadcrumb-item active" aria-current="page">Overview</li> --}}
    </ol>
</nav>
@endsection

@section('content')
<div class="grid">
    <p class="grid-header">Edit Data</p>
    <div class="grid-body">
        <div class="item-wrapper">
            <div class="row">
                
                <!-------------------- datanya disini ---------------------------->
                <div class="col-sm-12">
                    @if(session('alertprogress'))
                    <div class="alert alert-primary dismissible-alert" role="alert">
                        {{ session('alertprogress') }}<i class="alert-close mdi mdi-close"></i>
                    </div>
                    @endif
                </div>
               
                <div class="col-sm-12 contenttable">
                    <br>
                <form action="{{ route('district_update_update') }}" method="POST" enctype='multipart/form-data'>
                    @csrf

                    <input type="hidden" class="form-control @error('id') is-invalid @enderror" name="id" value="{{ old('id') ?? $dtarray['listdata']->id}}">
            
                    <div class="row mb-3">
                        <div class="col-md-8 mx-auto">

                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area"><label for="inputType1">Witel (kota/kab)</label></div>
                                <div class="col-md-9 showcase_content_area">
                                        <select id="js-select-example" class="form-control @error('regency_id') is-invalid @enderror" name="regency_id">
                                            @foreach($dtarray['dataoption'] as $list)
                                                <?php 
                                                    if($dtarray['listdata']->regency_id == $list->id){
                                                        $slc = 'selected';
                                                    }else{
                                                        $slc = '';
                                                    }
                                                ?>
                                                <option value="{{ $list->id }}" <?php echo $slc ?>>{{ $list->provincename.' - '.$list->citiname }}</option>
                                            @endforeach
                                            
                                           
                                        </select>
                                    @error('regency_id')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area"><label for="inputType1">STO (Distrik)</label></div>
                                <div class="col-md-9 showcase_content_area">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') ?? $dtarray['listdata']->name}}">
                                    @error('name')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3">
                                    &nbsp;
                                </div>
                                <div class="col-md-9">
                                    <input type="submit" class="btn btn-success form-control text-white" value="Update">
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('jsvendor')
<script>
    $(document).ready(function() {
        $('#content1').summernote({
            height: "300px",
            styleWithSpan: false,
            dialogsInBody: true
        });
    }); 
</script>
@endsection