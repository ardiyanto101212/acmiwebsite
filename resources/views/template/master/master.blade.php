@extends('template.metronic11')

@section('title', 'Master Menu')

@section('css_vendor')
    
@endsection

@section('css_custom')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"> 

    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net/js/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-bs4/js/dataTables.bootstrap4.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/js/global/integration/plugins/datatables.init.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-autofill/js/dataTables.autoFill.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-buttons/js/dataTables.buttons.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-buttons/js/buttons.colVis.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-buttons/js/buttons.flash.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-buttons/js/buttons.html5.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-buttons/js/buttons.print.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-colreorder/js/dataTables.colReorder.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-keytable/js/dataTables.keyTable.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-responsive/js/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-rowgroup/js/dataTables.rowGroup.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-rowreorder/js/dataTables.rowReorder.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-scroller/js/dataTables.scroller.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets_metronic/plugins/custom/datatables.net-select/js/dataTables.select.min.js')}}" type="text/javascript"></script>
		
    <style>
        .fontumum{
            font-size:1rem;
        }
    </style>
    <style>
        .contenttable {
                width: 100%;
                height: auto;
                overflow: auto;
                position: relative;
            }
        
        .tablelist>thead>tr>th, .tablelist>tbody>tr>th, .tablelist>tfoot>tr>th, .tablelist>thead>tr>td, .tablelist>tbody>tr>td, .tablelist>tfoot>tr>td {
                padding-top: 5px;
                padding-bottom: 5px;
                padding-left: 5px;
            }  
        .rowfixediflow{
            width:992px;
        }
    </style>
    {{-- isi dengan css --}}
@endsection

@section('breadcumbs')
    <h3 class="kt-subheader__title fontumum">Master </h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="#" class="kt-subheader__breadcrumbs-link text-warning">
                List
            </a>

            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>
@endsection

@section('body')
<div class="col">
    <div class="kt-portlet kt-portlet--height-fluid">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    List Menu
                </h3>
            </div>
            <div class="kt-portlet__head-label text-right">
                <input type="text" name="search" id="searh" class="form-control" placeholder="Search">
            </div>
        </div>
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
             {{-- <div class="contenttable"> --}}
                 {{-- <div class="col-xl">
                    <table class="table tableif1" id="datatable">
                        <thead>
                            <tr>
                                <td style="font-weight:bold">No</td>
                                <td style="font-weight:bold">Nama</td>
                                <td style="font-weight:bold">Jabatan</td>
                                <td style="font-weight:bold">Action</td>
                            </tr>
                        </thead>
                        
                    </table>
                 </div> --}}
                 {{-- <div class="col-lg-12"> --}}
                    <div class="row mt-4" id="scopeSearch">
                        {{-- {!! \App\Helper\ComponentBuilder::masterMenuBuilder() !!} --}}
                        {!! $masterMenu !!}
                    </div>
                    {{-- @for ($i = 0; $i < 25; $i++)
                        <div class="col-md-3 col-lg-3">
                            <a href="http://www.google.com">
                                <div class="kt-portlet kt-callout kt-callout--brand kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title text-center">User1</h3>
                                                <p class="kt-callout__desc">
                                                    Windows 10 automatically installs updates to make for sure
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="#" data-toggle="modal" data-target="#kt_chat_modal" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Make Call</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endfor --}}
                {{-- </div> --}}
             {{-- </div> --}}
        </div>
    </div>
</div>

@endsection

@section('js_vendor')
<!-- untuk datatable -->
<script src="{{ asset('assets_metronic/plugins/custom/datatables.net/js/jquery.dataTables.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets_metronic/plugins/custom/datatables.net-bs4/js/dataTables.bootstrap4.js')}}" type="text/javascript"></script>
<!-- untuk datatable -->
@endsection

@section('js_custom')
<script>
$(document).ready(function() {
    $("#searh").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#scopeSearch div").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    // $('#datatable').DataTable();

    // oTable = $('#datatable').DataTable({
    //     responsive: true,
    //     processing: true,
    //     // "language": {
    //     //     "processing": "<div class='circle-loader'></div>" //add a loading image,simply putting <img src="loader.gif" /> tag.
    //     // },
    //     serverSide: true,
    //     ajax: "#",
    //     columns: [
    //         // { data: 'IdClass', name: 'IdClass' },
    //         { data: 'id',
    //             render: function (data, type, row, meta) {
    //                 return meta.row + meta.settings._iDisplayStart + 1;
    //             }
    //         },
    //         { data: 'created_at', name: 'created_at' },
    //         { data: 'id_users', name: 'id_users' },
    //         { data: 'action', name: 'action' },
    //         { data: 'description', name: 'description' },
    //         // { data: 'id', //primary key dari tabel
    //         // render: function(data, type, row)
    //         //     {
    //         //     let buttonEdit = '<button type="button" class="btn-sm btn-inverse-primary mr-2" data-toggle="modal" data-target="#exampleModal" onclick="buttonEdit(\''+data+'\');" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></button>';
    //         //     let buttonHapus = '<button type="button" class="btn-sm btn-inverse-danger" onclick="buttonDelete(\''+data+'\');" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button>';

    //         //     // let buttonEdit = '<button type="button" class="btn-sm btn-info" data-toggle="modal" data-target="#showModalUpdate" style="margin-right:5px;" onclick="buttonEdit(\''+data+'\');">Update</button>';
    //         //     // let buttonHapus = '<button type="button" class="btn-sm btn-danger" onclick="buttonDelete(\''+data+'\');" >Delete</button>';
    //         //     return buttonEdit+buttonHapus;
    //         //     } 
    //         // }
    //     ],
    //     "order": [[ 1, "desc" ]]
    // });



    //----
            
    //----
} );

</script>
    {{-- isi dengan js --}}
@endsection