<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
  <title>@yield('title')</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/vendors/iconfonts/mdi/css/materialdesignicons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/vendors/css/vendor.addons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/css/shared/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/css/demo_1/style.css') }}">
  <link rel="shortcut icon" href="{{ asset('logo/logo.png') }}">
  {{-- <link rel="shortcut icon" href="{{ asset('assets_ripleui/assets/vendors/summernote/dist/summernote-lite.css') }}"> --}}
  <link rel="stylesheet" href="{{ asset('assets_ripleui/assets/summernote/summernote-lite.css') }}">

  <link rel="stylesheet" href="{{ asset('sweetalert/sweetalert2.min.css') }}">



  
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  @yield('cssvendor')

</head>

<body class="header-fixed">
  <!-- partial:partials/_header.html -->
  <nav class="t-header">
    <div class="t-header-brand-wrapper"><a href="{{ route('welcome_view_index') }}"><img class="logo" src="{{ asset('logo/logo.png') }}" alt="">
        <img class="logo-mini" src="{{ asset('logo/logo.png') }}" alt=""> </a><button
        class="t-header-toggler t-header-desk-toggler d-none d-lg-block"><svg class="logo" viewbox="0 0 200 200">
          <path class="top" d="
                M 40, 80
                C 40, 80 120, 80 140, 80
                C180, 80 180, 20  90, 80
                C 60,100  30,120  30,120
              "></path>
          <path class="middle" d="
                M 40,100
                L140,100
              "></path>
          <path class="bottom" d="
                M 40,120
                C 40,120 120,120 140,120
                C180,120 180,180  90,120
                C 60,100  30, 80  30, 80
              "></path>
        </svg></button></div>
    <div class="t-header-content-wrapper bg-white">
      <div class="t-header-content"><button class="t-header-toggler t-header-mobile-toggler d-block d-lg-none"><i class="mdi mdi-menu"></i></button>
        
        <ul class="nav ml-auto">
          
            <li class="nav-item dropdown text-dark" style="cursor:pointer;"><span class="nav-link" id="appsDropdown" data-toggle="dropdown"
                aria-expanded="false"><i class="mdi mdi-account-circle mdi-1x"></i> Hello {{ Auth::user()->fullname }}</span>
            <div class="dropdown-menu navbar-dropdown dropdown-menu-right" aria-labelledby="appsDropdown">
                {{-- <div class="dropdown-header">
                    <h6 class="dropdown-title">Apps</h6>
                    <p class="dropdown-title-text mt-2">Authentication required for 3 apps</p>
                </div> --}}
                
                <div class="dropdown-footer">
                    <a href="#" target="_blank" class="btn btn-primary btn-logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- partial -->
  <div class="page-body bg-primary">
    <!-- partial:partials/_sidebar.html -->
    <div class="sidebar">
      <ul class="navigation-menu">
        
        <li class="nav-category-divider">MENU</li>
            <?php
                // if(Route::currentRouteName() == 'welcome_view_index'){
                //     $activedashboard = 'active';
                // }else if(Route::currentRouteName() == 'home'){
                //     $activedashboard = 'active';
                // }else{
                //     $activedashboard = "";
                // }
            ?>
            {{-- <li class="<?php echo $activedashboard ?>">
                <a href="{{ route('dashboard') }}">
                    <span class="link-title">Dashboard</span> 
                    <i class="mdi mdi-gauge link-icon"></i>
                </a>
            </li> --}}
        {{-- untuk menu dinamis  --}}
        {!! \App\Helper\ComponentBuilder::sideMenuBuilder() !!}

        
        {{-- <li class="nav-category-divider">APPS</li>
        
        <li>
            <a href="pages/apps/email.html">
                <span class="link-title">Email</span> 
                <i class="mdi mdi-email-outline link-icon"></i>
            </a>
        </li> --}}
        
      </ul>
      <div class="sidebar_footer">
        <div class="user-account">
          <div class="user-profile-item-tittle">User</div>
            <a class="user-profile-item" href="#"><i class="mdi mdi-account"></i> Profile</a> 
            {{-- <a class="user-profile-item" href="#"><i class="mdi mdi-settings"></i> Account Settings</a>  --}}

            <a href="#" target="_blank" class="btn btn-primary btn-logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
					@csrf
                </form>

            {{-- <a class="btn btn-primary btn-logout" href="#">Logout</a> --}}
        </div>
        <div class="btn-group admin-access-level">
            <div class="avatar">
                <i class="mdi mdi-account-circle mdi-5x"></i>
              {{-- <img class="profile-img" src="{{ asset('assets_ripleui/assets/images/profile/male/image_1.png') }}" alt=""> --}}
            </div>
          <div class="user-type-wrapper">
            <p class="user_name">
                {{ Auth::user()->fullname }}
            </p>
            <div class="d-flex align-items-center">
              <div class="status-indicator small rounded-indicator bg-success"></div>
              <small class="user_access_level">Active</small>
            </div>
          </div><i class="arrow mdi mdi-chevron-right"></i>
        </div>
      </div>
    </div>
    <!-- partial -->
    <div class="page-content-wrapper" style="background:#f8f8f8e1;">
      {{-- <div class="page-content-wrapper-inner"> --}}
        <div class="">
        <div class="viewport-header">
          @yield('breadcumbs')
        </div>
        
        <div class="content-viewport">

            @yield('content')

        </div>
      <!-- content viewport ends -->
      <!-- partial:partials/_footer.html -->
      <footer class="footer">
        <div class="row">
            <div class="col-sm-6 text-center text-sm-left mt-3 mt-sm-0"><small class="text-muted d-block">Copyright © 2021
                <a href="https://ragdalion.com/" target="_blank">RAGDALION TECHNOLOGY</a>. All rights reserved</small>
            </div>
        </div>
      </footer><!-- partial -->
    </div><!-- page content ends -->
  </div>


  <script src="{{ asset('assets_ripleui/assets/vendors/js/core.js') }}"></script>
  <script src="{{ asset('assets_ripleui/assets/vendors/js/vendor.addons.js') }}"></script>
  <script src="{{ asset('assets_ripleui/assets/vendors/chartjs/Chart.min.js') }}"></script>
  <script src="{{ asset('assets_ripleui/assets/js/script.js') }}"></script>
  <!-- summer note -->
  
  <script src="{{ asset('sweetalert/sweetalert2.all.min.js') }}"></script>
  
  <script src="{{ asset('assets_ripleui/assets/summernote/summernote-lite.js') }}"></script>
  {{-- <script src="{{ asset('assets_ripleui/assets/summernote/summernote.min.js') }}"></script> --}}

  {{-- <script src="{{ asset('assets_ripleui/assets/vendors/summernote/dist/summernote-lite.min.js') }}"></script> --}}

  <script>
    $(document).ready(function(){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
    });
  </script>

  @yield('jsvendor')

</body>

</html>