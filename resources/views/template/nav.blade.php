<?php
//use Auth;
//use DB;

// $iduser = Auth::user()->id ?? "null";  // id user aktif
// $idtitle = Auth::user()->id_title ?? "null"; // id grup 1 untuk admin 2 untuk peserta



?>

<!-- begin:: Aside Menu -->
	<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
		<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
			{{-- {!! \App\Helper\ComponentBuilder::sideMenuBuilder() !!} --}}
			
					{{-- <ul class="kt-menu__nav">
						<li class="kt-menu__item {{ Route::currentRouteName() == 'users' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="#" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-user"></i><span class="kt-menu__link-text">Employee</span></a></li>
						<li class="kt-menu__item {{ Route::currentRouteName() == 'employee' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="#" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-upload"></i><span class="kt-menu__link-text">Import NM & OT</span></a></li>
						<li class="kt-menu__item {{ Route::currentRouteName() == 'incometaxinsurance'? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="#" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-list"></i><span class="kt-menu__link-text">Income Tax & Insurance</span></a></li>
						<li class="kt-menu__item {{ Route::currentRouteName() == 'report' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="#" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-envelope"></i><span class="kt-menu__link-text">Report</span></a></li>
					</ul>
					<ul class="kt-menu__nav ">
						<li class="kt-menu__item {{ Route::currentRouteName() == 'reportindividu' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="#" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-envelope"></i><span class="kt-menu__link-text">Report</span></a></li>
					</ul> --}}
					{{-- {!! \App\Helper\ComponentBuilder::sideMenuBuilder() !!} --}}
				<ul class="kt-menu__nav ">
					<li class="kt-menu__item {{ Route::currentRouteName() == 'dashboard' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="{{ route('dashboard') }}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-home"></i><span class="kt-menu__link-text">Dashboard</span></a></li>

					{!! \App\Helper\ComponentBuilder::sideMenuBuilder() !!}

					@if (\App\Helper\ComponentBuilder::countMasterMenu() >= 1)
						<li class="kt-menu__item {{ Route::currentRouteName() == 'master_view_index' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="{{ route('master_view_index') }}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-menu-1"></i><span class="kt-menu__link-text">Master</span></a></li>
					@endif

					{{-- <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-contract"></i><span class="kt-menu__link-text">Skins</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
						<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
							<ul class="kt-menu__subnav">
								<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Skins</span></span></li>
								<li class="kt-menu__item " aria-haspopup="true"><a href="layout/skins/aside-light&demo=demo11.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Light Aside</span></a></li>
								<li class="kt-menu__item " aria-haspopup="true"><a href="layout/skins/header-dark&demo=demo11.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Dark Header</span></a></li>
							</ul>
						</div>
					</li>
					<li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--open kt-menu__item--here" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-browser-2"></i><span class="kt-menu__link-text">Purchasing</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
						<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
							<ul class="kt-menu__subnav">
								<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Purchase</span></span></li>
								<li class="kt-menu__item  kt-menu__item--active" aria-haspopup="true"><a href="layout/subheader/classic&demo=demo11.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Purchase Request</span></a></li>
								<li class="kt-menu__item " aria-haspopup="true"><a href="layout/subheader/toolbar&demo=demo11.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Purchase Order</span></a></li>
								<li class="kt-menu__item " aria-haspopup="true"><a href="layout/subheader/actions&demo=demo11.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Purchase Retur</span></a></li>
								<li class="kt-menu__item " aria-haspopup="true"><a href="layout/subheader/none&demo=demo11.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">None</span></a></li>
							</ul>
						</div>
					</li> --}}
					{{-- <li class="kt-menu__item {{ Route::currentRouteName() == 'user_view_index' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="{{ route('user_view_index') }}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-envelope"></i><span class="kt-menu__link-text">User</span></a></li> --}}
				{{-- </ul>
				<ul class="kt-menu__nav "> --}}
					{{-- {{ Request::url() }}
					{{ dd(App\Models\MasterMenu::where('urlname', Request::url())->first()) }} --}}
				</ul>

			
		</div>
	</div>



{{-- <li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('home') }}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-home"></i><span class="kt-menu__link-text">Home</span></a></li>
			<li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('daftarproject') }}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-folder"></i><span class="kt-menu__link-text">Ruang Data</span></a></li>
			<li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('daftaruser') }}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-users"></i><span class="kt-menu__link-text">Daftar User</span></a></li>
			<li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('daftarupload') }}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-signs-1"></i><span class="kt-menu__link-text">Log</span></a></li>
			<li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('profile') }}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-profile"></i><span class="kt-menu__link-text">Profile</span></a></li>
			 --}}

