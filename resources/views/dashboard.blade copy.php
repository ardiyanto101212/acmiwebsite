@extends('template.ripleui')

@section('title')
    Dashboard
@endsection

@section('cssvendor')

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

{{-- <link rel="stylesheet" href="{{ asset('daterangepicker/daterangepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('daterangepicker/daterangepicker.css') }}"> --}}

<style>
  .contenttable {
          width: 100%;
          height: auto;
          overflow: auto;
          position: relative;
      }

      
.chart-container-1 {
  height: 100%;
  flex: 0 0 auto;
  width: 100%;
}

.main-container {
  display: flex;
  justify-content: space-between;
}

.main-container > div {
    padding: 0px 10px;
}
.select2-selection__rendered {
  height: 30px !important;
}
</style>

@endsection

@section('breadcumbs')
{{-- <nav aria-label="breadcrumb">
    <ol class="breadcrumb has-arrow">
      <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
    </ol>
</nav> --}}
@endsection

@section('content')
{{-- 
<div class="row">
  <div class="col-md-7 equel-grid order-md-2">
    <div class="grid d-flex flex-column justify-content-between overflow-hidden"><div class="chartjs-size-monitor" style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
      <div class="grid-body">
        <div class="d-flex justify-content-between">
          <p class="card-title">Sales Revenue</p>
          <div class="chartjs-legend" id="sales-revenue-chart-legend"><ul class="0-legend"><li><span style="background-color:#1A76CA"></span>Sales</li><li><span style="background-color:#2d92fe"></span>Marketing</li></ul></div>
        </div>
        <div class="d-flex">
          <p class="d-none d-xl-block">12.5% Growth compared to the last week</p>
          <div class="ml-auto">
            <h2 class="font-weight-medium text-gray"><i class="mdi mdi-menu-up text-success"></i><span class="animated-count">25.04</span>%</h2>
          </div>
        </div>
      </div><canvas class="mt-4 chartjs-render-monitor" id="sales-revenue-chart" height="372" style="display: block; height: 124px; width: 332px;" width="996"></canvas>
    </div>
  </div>
  <div class="col-md-5 order-md-0">
    <div class="row">

      <div class="col-6 equel-grid">
        <div class="grid d-flex flex-column align-items-center justify-content-center">
          <div class="grid-body text-center">
            <div class="profile-img img-rounded bg-inverse-primary no-avatar component-flat mx-auto mb-4"><i class="mdi mdi-wallet mdi-2x"></i></div>
            <h2 class="font-weight-medium"><span class="animated-count">21.2</span>k</h2><small class="text-gray d-block mt-3">Total Followers</small> <small class="font-weight-medium text-success"><i class="mdi mdi-menu-up"></i><span class="animated-count">1.50</span>%</small>
          </div>
        </div>
      </div>

      <div class="col-6 equel-grid">
        <div class="grid d-flex flex-column align-items-center justify-content-center">
          <div class="grid-body text-center">
            <div class="profile-img img-rounded bg-inverse-danger no-avatar component-flat mx-auto mb-4"><i class="mdi mdi-airballoon mdi-2x"></i></div>
            <h2 class="font-weight-medium"><span class="animated-count">1.6</span>k</h2><small class="text-gray d-block mt-3">Impression</small> <small class="font-weight-medium text-danger"><i class="mdi mdi-menu-down"></i><span class="animated-count">0.43</span>%</small>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-6 equel-grid">
        <div class="grid d-flex flex-column align-items-center justify-content-center">
          <div class="grid-body text-center">
            <div class="profile-img img-rounded bg-inverse-warning no-avatar component-flat mx-auto mb-4"><i class="mdi mdi-fire mdi-2x"></i></div>
            <h2 class="font-weight-medium animated-count">239</h2><small class="text-gray d-block mt-3">Reach</small> <small class="font-weight-medium text-danger"><i class="mdi mdi-menu-down"></i><span class="animated-count">1.08</span>%</small>
          </div>
        </div>
      </div>
      <div class="col-6 equel-grid">
        <div class="grid d-flex flex-column align-items-center justify-content-center">
          <div class="grid-body text-center">
            <div class="profile-img img-rounded bg-inverse-success no-avatar component-flat mx-auto mb-4"><i class="mdi mdi-charity mdi-2x"></i></div>
            <h2 class="font-weight-medium"><span class="animated-count">2.4</span>%</h2><small class="text-gray d-block mt-3">Engagement Rate</small> <small class="font-weight-medium text-success"><i class="mdi mdi-menu-up"></i><span class="animated-count">4.54</span>%</small>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> --}}
<div class="row">

  <div class="col-md-4">
      <div class="grid">
          <div class="grid-body">
            <div class="row">

              <div class="col-sm-2 mb-2">
                <b style="margin-right:10%;">Keuangan</b>
              </div>

              <div class="col-sm-4 mb-2">
                <div class="input-group">
                    <input type="text" readonly id="monthkeuangan" class="form-control input-group datepicker monthkeuangan" onchange="monthkeuangan()" value="{{ date('m-Y') }}" data-date-format="mm-yyyy"> 
                      <span class="input-group-addon input-group-append">
                        <span class="mdi mdi-calendar input-group-text"></span>
                      </span>
                  </div>
              </div>
            
              <div class="col-sm-6 mb-2">
                <select id="js-select-example" height="35px" class="form-control form-sm witelkeuangan" onchange="monthkeuangan()">
                  @foreach($data['witel'] as $list)
                      <option value="{{ $list->id }}">{{ $list->provincename.' - '.$list->citiname }}</option>
                  @endforeach
                </select>
              </div>


              <div class="col-sm-6">
                <div class="card bg-primary text-white">
                  <div class="card-body">
                    <center>
                      Total Fee Sales
                    </center>
                    <center>
                      <span class="btn btn-white btn-rounded mb-2">
                        <h1><b class="totalsalesfee"></b></h1>
                      </span>
                    </center>
                      <div class="keuangansalesfee"></div>
                  </div>
                </div>
              </div>

              <div class="col-sm-6">
                <div class="card bg-warning text-white">
                  <div class="card-body">
                    <center>
                      Total Fee Teknisi
                    </center>
                    <center>
                      <span class="btn btn-white btn-rounded mb-2">
                        <h1><b class="totalteknisifee"></b></h1>
                      </span>
                    </center>
                   
                      <div class="keuanganteknisifee"></div>
                    
                  </div>
                </div>
              </div>


            </div>

          </div>
      </div>
  </div>

  <div class="col-md-4">
    <div class="grid">
        <div class="grid-body">
                <div class="input-group mb-2" style="margin-top:;">
                        <b style="margin-right:10%;">Absensi</b>
                          <span class="btn btn-secondary btn-xs mdi mdi-arrow-left-drop-circle kurangweek" onclick="cekabsenline('min')"></span>
                          <span class="btn btn-white btn-xs tanggalweek">{{ $data['weekawal'].'-'.$data['weekakhir'] }}</span>
                          <span class="btn btn-secondary btn-xs mdi mdi-arrow-right-drop-circle tambahweek" onclick="cekabsenline('plus')"></span>
                </div>
                <input type="hidden" class="nilaiweek" value="0">
            <div class="item-wrapper main-container">
                <div class="chart-container-1 linechartabsen">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="grid">
        <div class="grid-body">
                <div class="mb-2" style="margin-top:;">
                  <center><b>Avg. Hours</b></center>
                </div>
               

            <div class="item-wrapper main-container">
                <div class="chart-container-1 linechartabsenavghours">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

</div>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb has-arrow">
    
    {{-- <li class="breadcrumb-item active" aria-current="page">Overview</li> --}}
    <div class="input-group col-sm-3">
      <li class="breadcrumb-item" style="margin-right:5%;"><a href="#">Sales Performance</a></li>

      <input type="text" readonly id="monthqtypenjualansales" class="form-control input-group datepicker monthqtypenjualansales" onchange="monthqtypenjualansales()" value="{{ date('m-Y') }}" data-date-format="mm-yyyy"> 
        <span class="input-group-addon input-group-append">
          <span class="mdi mdi-calendar input-group-text"></span>
        </span>
    </div>

  </ol>
</nav>

<div class="row">

  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Qty Penjualan</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 datamonthqtypenjualansales">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Top 5 Produk</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 produksales">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Top 5 Kota</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 wilayahsales">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Top 5 Sales</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 top5sales">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

  

</div>


<nav aria-label="breadcrumb">
  <ol class="breadcrumb has-arrow">
    
    {{-- <li class="breadcrumb-item active" aria-current="page">Overview</li> --}}
    <div class="input-group col-sm-3">
      <li class="breadcrumb-item" style="margin-right:5%;"><a href="#">Teknisi Performance</a></li>

      <input type="text" readonly id="monthtrknisiperformance" class="form-control input-group datepicker monthtrknisiperformance" onchange="monthtrknisiperformance()" value="{{ date('m-Y') }}" data-date-format="mm-yyyy"> 
        <span class="input-group-addon input-group-append">
          <span class="mdi mdi-calendar input-group-text"></span>
        </span>
    </div>

  </ol>
</nav>


<div class="row">

  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Qty Layanan</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 qtyteknisi">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Top 5 Layanan</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 produkteknisi">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Top 5 Kota</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 wilayahteknisi">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Top 5 Teknisi</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 top5teknisi">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

</div>

<div class="row">
  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Top 5 Trending Topik</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 trendingtopik">
                    
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

@endsection

@section('jsvendor')

{{-- <script src="{{ asset('daterangepicker/daterangepicker.min.js') }}"></script>
<script src="{{ asset('daterangepicker/daterangepicker.js') }}"></script> --}}

<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

<script>
$('#monthkeuangan').datepicker({
     minViewMode: 1,
     format: 'mm-yyyy'
});
$('#monthqtypenjualansales').datepicker({
     minViewMode: 1,
     format: 'mm-yyyy'
});
$('#monthtrknisiperformance').datepicker({
     minViewMode: 1,
     format: 'mm-yyyy'
});



$('#year1').datepicker({
     minViewMode: 2,
     format: 'yyyy'
});
</script>


<script type="text/javascript">
$(document).ready(function() {

  // $(".daterangepickernya").daterangepicker({
  //   alert('done');
  //   forceUpdate: true,
  //   callback: function(startDate, endDate, period){
  //     var title = startDate.format('L') + ' – ' + endDate.format('L');
  //     $(this).val(title)
  //   }
  // });

});
  $(function() {
      $('.daterangekeuangan').daterangepicker();

      // $('.daterangekeuangan').daterangepicker(
      //   forceUpdate: true,
      //   callback: function(startDate, endDate, period){
      //     var title = startDate.format('L') + ' – ' + endDate.format('L');
      //     $(this).val(title)
      //   }
      // );
  });
</script>

<script>
$(document).ready(function() {
  monthkeuangan();
  cekabsenline('nol');
  monthqtypenjualansales();
  monthtrknisiperformance();
  trendingtopik();

        // keuangan(); 
        
        // cekagroupsales();
        // cekagroupteknisi();
        
});
</script>

<script>
function monthkeuangan(){
  var monthkeuangan = $('.monthkeuangan').val();
  var witel = $('.witelkeuangan').val();

  var form_data = new FormData();

            form_data.append('monthkeuangan', monthkeuangan);
            form_data.append('witel', witel);

                $.ajax({
                        url: "{{ route('dashboard_view_keuangan') }}",
                        dataType: 'JSON', 
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        beforeSend: function (){
                                $(".preloader").show();
                            },
                        success: function(x) {
                          $('.totalsalesfee').html(x.totalsalesfee);
                          $('.totalteknisifee').html(x.totalteknisifee);

                          $('.keuangansalesfee').html(x.keuangansalesfee);
                          $('.keuanganteknisifee').html(x.keuanganteknisifee);
                        },
                        error: function (response) {
                            swal("Warning", 'Coba lagi', "warning");
                            $(".preloader").hide();
                        }
                });
                
}

function monthqtypenjualansales(){
  var monthkeuangan = $('.monthqtypenjualansales').val();
  // var witel = $('.witelkeuangan').val();

  var form_data = new FormData();

            form_data.append('monthkeuangan', monthkeuangan);
            // form_data.append('witel', witel);

                $.ajax({
                        url: "{{ route('dashboard_view_groupsales') }}",
                        dataType: 'JSON', 
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        beforeSend: function (){
                                $(".preloader").show();
                            },
                        success: function(x) {
                          $('.datamonthqtypenjualansales').html(x.qtysales);
                          $('.produksales').html(x.produksales);
                          $('.wilayahsales').html(x.wilayahsales);
                          $('.top5sales').html(x.top5sales);
                        },
                        error: function (response) {
                            swal("Warning", 'Coba lagi', "warning");
                            $(".preloader").hide();
                        }
                });
}

function monthtrknisiperformance(){
  var monthkeuangan = $('.monthtrknisiperformance').val();
  // var witel = $('.witelkeuangan').val();

  var form_data = new FormData();

            form_data.append('monthkeuangan', monthkeuangan);
            // form_data.append('witel', witel);

                $.ajax({
                        url: "{{ route('dashboard_view_groupteknisi') }}",
                        dataType: 'JSON', 
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        beforeSend: function (){
                                $(".preloader").show();
                            },
                        success: function(x) {
                          $('.qtyteknisi').html(x.qtyteknisi);
                          $('.produkteknisi').html(x.produkteknisi);
                          $('.wilayahteknisi').html(x.wilayahteknisi);
                          $('.top5teknisi').html(x.top5teknisi);
                        },
                        error: function (response) {
                            swal("Warning", 'Coba lagi', "warning");
                            $(".preloader").hide();
                        }
                });
}













function trendingtopik(){

  var form_data = new FormData();

            form_data.append('trendingtopik', '');

                $.ajax({
                        url: "{{ route('dashboard_view_trendingtopik') }}",
                        dataType: 'JSON', 
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        beforeSend: function (){
                                $(".preloader").show();
                            },
                        success: function(x) {
                          $('.trendingtopik').html(x.trendingtopik);
                        },
                        error: function (response) {
                            swal("Warning", 'Coba lagi', "warning");
                            $(".preloader").hide();
                        }
                });
}



function cekabsenline(minplus){
    var nilaiweek = parseInt($('.nilaiweek').val());

    if(minplus == 'nol'){
      var nilaiweek = 0;
    }else if(minplus == 'min'){
      var nilaiweek = nilaiweek-1;
    }else if(minplus == 'plus'){
      var nilaiweek = nilaiweek+1;
    }


    var form_data = new FormData();

   
            form_data.append('nilaiweek', nilaiweek);

                $.ajax({
                        url: "{{ route('dashboard_view_linechartabsen') }}",
                        dataType: 'JSON', 
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        beforeSend: function (){
                                $(".preloader").show();
                            },
                        success: function(x) {
                          $('.nilaiweek').val(nilaiweek);
                          $('.tanggalweek').html(x.tanggalweek);
                          $('.linechartabsen').html(x.linechartabsen);
                          $('.linechartabsenavghours').html(x.linechartabsenavghours);
                        },
                        error: function (response) {
                            swal("Warning", 'Coba lagi', "warning");
                            $(".preloader").hide();
                        }
                });

    

}





</script>



@endsection