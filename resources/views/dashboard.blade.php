@extends('template.ripleui')

@section('title')
    Dashboard
@endsection

@section('cssvendor')

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

{{-- <link rel="stylesheet" href="{{ asset('daterangepicker/daterangepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('daterangepicker/daterangepicker.css') }}"> --}}

<style>
  .contenttable {
          width: 100%;
          height: auto;
          overflow: auto;
          position: relative;
      }

.tablelist>thead>tr>th, .tablelist>tbody>tr>th, .tablelist>tfoot>tr>th, .tablelist>thead>tr>td, .tablelist>tbody>tr>td, .tablelist>tfoot>tr>td {
    padding-top: 2px;
		padding-bottom: 2px;
		padding-left: 2px;
}  

.chart-container-1 {
  height: 100%;
  flex: 0 0 auto;
  width: 100%;
}

.main-container {
  display: flex;
  justify-content: space-between;
}

.main-container > div {
    padding: 0px 10px;
}
.select2-selection__rendered {
  height: 30px !important;
}
</style>

<style>
    
    
  fieldset.scheduler-border {
      border: 1px groove #ddd !important;
      padding: 0.5em 0.5em 0.5em 0.5em !important;
      margin: 0 0 1.5em 0 !important;
      -webkit-box-shadow:  0px 0px 0px 0px #000;
              box-shadow:  0px 0px 0px 0px #000;
      border: 3px solid #0D76BC !important;
      border-radius: 12px;
      height:250px;
      
  }
   
  legend.scheduler-border {
      font-size: 1em !important;
      font-weight: bold !important;
      text-align: left !important;
      width:auto;
      padding:0 10px;
      border-bottom:none;
      margin-bottom: 5px;
      border-radius:7px 7px 7px 7px;
  }
   
  .login-form {
    margin-top: 20px;
  }
  
  </style>
@endsection

@section('breadcumbs')
{{-- <nav aria-label="breadcrumb">
    <ol class="breadcrumb has-arrow">
      <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
    </ol>
</nav> --}}
@endsection

@section('content')

<div class="row">
  <div class="col-md-4">
    {{-- F3E5F5 --}}
    <?php

            $useragent=$_SERVER['HTTP_USER_AGENT'];
            if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))

            { 
                $hghcontainerkeuangan = '100%';
                $heigfildlistkeuangan = '100%';
                $padinglrbox = '';
            }
            else{
                $hghcontainerkeuangan = '417px';
                $heigfildlistkeuangan = '365px';
                $padinglrbox = '5px';
            }
    ?>

    <div class="grid" style="background:;margin-top:-10px;height:{!! $hghcontainerkeuangan !!};">

        <div class="mb-1">
            <center><b style="font-size:20px;">Keuangan</b></center>
        </div>

        <div class="container" style="">
            <fieldset class="scheduler-border" style="background:white;height:{!! $heigfildlistkeuangan !!};">
                <legend class="scheduler-border" style="background:white;">Fee</legend>
                <div class="row">
                    <div class="col-sm-6 mb-2">
                      <div class="input-group">
                          <input type="text" readonly id="monthkeuangan" class="form-control input-group datepicker monthkeuangan" onchange="monthkeuangan()" value="{{ date('m-Y') }}" data-date-format="mm-yyyy"> 
                            <span class="input-group-addon input-group-append">
                              <span class="mdi mdi-calendar input-group-text"></span>
                            </span>
                        </div>
                    </div>
                  
                    <div class="col-sm-6 mb-2">
                      <select id="js-select-example" height="35px" style="width:100%;" class="form-control form-sm witelkeuangan" onchange="monthkeuangan()">
                        @foreach($data['witel'] as $list)
                            <option value="{{ $list->id }}">{{ $list->provincename.' - '.$list->citiname }}</option>
                        @endforeach
                      </select>
                    </div>

                <div class="col-sm-4 mb-2" style="padding-right:{!! $padinglrbox !!};">
                  <div class="card bg-primary text-white">
                    <div class="card-body" style="height:130px;padding-top:30px;">
                      <center>
                         Sales
                      </center>
                      <center>
                        <span class="badge badge-white btn-rounded mb-2">
                          <h4 style="font-size: 18px;"><b class="totalsalesfee"></b></h4>
                        </span>
                      </center>
                        {{-- <div class="keuangansalesfee"></div> --}}
                    </div>
                  </div>
                </div>

                <div class="col-sm-8 mb-2" style="padding-left:{!! $padinglrbox !!};">
                  <div class="card bg-primary text-white">
                    <div class="card-body" style="height:130px;">
                        <div class="keuangansalesfee"></div>
                    </div>
                  </div>
                </div>

                <div class="col-sm-4 mb-2" style="padding-right:{!! $padinglrbox !!};">
                  <div class="card bg-warning text-white">
                    <div class="card-body" style="height:130px;padding-top:30px;">
                      <center>
                         Teknisi
                      </center>
                      <center>
                        <span class="badge badge-white btn-rounded mb-2">
                          <h4 style="font-size: 18px;"><b class="totalteknisifee"></b></h4>
                        </span>
                      </center>
                        {{-- <div class="keuangansalesfee"></div> --}}
                    </div>
                  </div>
                </div>

                <div class="col-sm-8 mb-2" style="padding-left:{!! $padinglrbox !!};">
                  <div class="card bg-warning text-white">
                    <div class="card-body" style="height:130px;">
                        <div class="keuanganteknisifee"></div>
                    </div>
                  </div>
                </div>
{{-- 
                <div class="col-sm-8">
                  <div class="card bg-warning text-white">
                    <div class="card-body">
                      <center>
                        Total Fee Teknisi
                      </center>
                      <center>
                        <span class="btn btn-white btn-rounded mb-2">
                          <h4><b class="totalteknisifee"></b></h4>
                        </span>
                      </center>
                     
                        <div class="keuanganteknisifee"></div>
                      
                    </div>
                  </div>
                </div> --}}

            </fieldset>
        </div>
    </div>
  </div>

  <div class="col-md-4">
    {{-- F3E5F5 --}}
    <div class="grid" style="background:;margin-top:-10px;height:417px;">

        <div class="mb-1">
          <center><b style="font-size:20px;">Absensi</b></center>
            
        </div>

        <div class="container" style="margin-top:-6px;">
            <fieldset class="scheduler-border" style="background:white;height:100%;">
                <legend class="scheduler-border" style="background:white;">
                  <div class="input-group mb-2" style="margin-top:5px;">
                          <span class="btn btn-primary btn-xs mdi mdi-arrow-left kurangweek" onclick="cekabsenline('min')" style="cursor:pointer;"></span>
                          <span class="btn btn-white btn-xs tanggalweek">{{ $data['weekawal'].'-'.$data['weekakhir'] }}</span>
                          <span class="btn btn-primary btn-xs mdi mdi-arrow-right tambahweek" onclick="cekabsenline('plus')" style="cursor:pointer;"></span>
                          <input type="hidden" class="nilaiweek" value="0">
                  </div>
                </legend>
                
                
                
                <div class="linechartabsen" style="height:130px;padding-bottom:7px;">
                  
                </div>
            </fieldset>
        </div>

        <div class="container" style="margin-top:-15px;">
          <fieldset class="scheduler-border" style="background:white;height:100%;">
              <legend class="scheduler-border" style="background:white;">Rata-Rata Jam</legend>
              

              <div class="linechartabsenavghours" style="height:130px;padding-bottom:7px;">
                
              </div>
          </fieldset>
      </div>


    </div>
  </div>
  
  <div class="col-md-4">
    {{-- F3E5F5 --}}
    <div class="grid" style="background:;margin-top:-10px;height:417px;">

        <div class="mb-1">
            <center><b style="font-size:20px;">Top 5 Trending Topik</b></center>
        </div>

        <div class="container">
            <fieldset class="scheduler-border" style="background:white;height:364px;">
                <legend class="scheduler-border" style="background:white;">Topik</legend>
                

                <div class="trendingtopik">
                
                </div>
            </fieldset>
        </div>
    </div>
  </div>


  <div class="col-md-6">
    {{-- F3E5F5 --}}
    <div class="grid row" style="background:;margin-top:-10px;">

        <div class="mb-1 container row">
            <div class="col-sm-8">
              <center><b style="font-size:20px;">Sales Performance</b></center>
            </div>
            <div class="col-sm-4">
              <div class="input-group" style="margin-top:6px;">
                  <input type="text" readonly id="monthqtypenjualansales" class="form-control input-group datepicker monthqtypenjualansales" onchange="monthqtypenjualansales()" value="{{ date('m-Y') }}" data-date-format="mm-yyyy"> 
                  <span class="input-group-addon input-group-append">
                    <span class="mdi mdi-calendar input-group-text"></span>
                  </span>
              </div>
            </div>
        </div>

        <div class="col-sm-12">
          <fieldset class="scheduler-border" style="background:white;">
              <legend class="scheduler-border" style="background:white;">Top 5 sales</legend>
  
              <div class="top5sales">
              
              </div>
          </fieldset>
        </div>

        <div class="col-sm-12" style="margin-top:-15px;">
            <fieldset class="scheduler-border" style="background:white;">
                <legend class="scheduler-border" style="background:white;">Qty Penjualan</legend>
                

                <div class="datamonthqtypenjualansales">
                
                </div>
            </fieldset>
        </div>

        <div class="col-sm-6" style="margin-top:-15px;">
          <fieldset class="scheduler-border" style="background:white;height:100%;">
              <legend class="scheduler-border" style="background:white;">Top 5 Produk</legend>
              

              <div class="produksales">
              
              </div>
          </fieldset>
      </div>

      <div class="col-sm-6" style="margin-top:-15px;">
        <fieldset class="scheduler-border" style="background:white;height:100%;">
            <legend class="scheduler-border" style="background:white;">Top 5 Kota</legend>
            

            <div class="wilayahsales">
            
            </div>
        </fieldset>
      </div>


    </div>
  </div>

</div>

<div class="row">

  {{-- <div class="col-md-4">
      <div class="grid">
          <div class="grid-body">
            <div class="row">

              <div class="col-sm-2 mb-2">
                <b style="margin-right:10%;">Keuangan</b>
              </div>

              <div class="col-sm-4 mb-2">
                <div class="input-group">
                    <input type="text" readonly id="monthkeuangan" class="form-control input-group datepicker monthkeuangan" onchange="monthkeuangan()" value="{{ date('m-Y') }}" data-date-format="mm-yyyy"> 
                      <span class="input-group-addon input-group-append">
                        <span class="mdi mdi-calendar input-group-text"></span>
                      </span>
                  </div>
              </div>
            
              <div class="col-sm-6 mb-2">
                <select id="js-select-example" height="35px" class="form-control form-sm witelkeuangan" onchange="monthkeuangan()">
                  @foreach($data['witel'] as $list)
                      <option value="{{ $list->id }}">{{ $list->provincename.' - '.$list->citiname }}</option>
                  @endforeach
                </select>
              </div>
            </div>

          </div>
      </div>
  </div> --}}

  

</div>


<nav aria-label="breadcrumb">
  <ol class="breadcrumb has-arrow">
    
    {{-- <li class="breadcrumb-item active" aria-current="page">Overview</li> --}}
    <div class="input-group col-sm-3">
      <li class="breadcrumb-item" style="margin-right:5%;"><a href="#">Teknisi Performance</a></li>

      <input type="text" readonly id="monthtrknisiperformance" class="form-control input-group datepicker monthtrknisiperformance" onchange="monthtrknisiperformance()" value="{{ date('m-Y') }}" data-date-format="mm-yyyy"> 
        <span class="input-group-addon input-group-append">
          <span class="mdi mdi-calendar input-group-text"></span>
        </span>
    </div>

  </ol>
</nav>


<div class="row">

  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Qty Layanan</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 qtyteknisi">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Top 5 Layanan</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 produkteknisi">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Top 5 Kota</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 wilayahteknisi">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Top 5 Teknisi</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 top5teknisi">
                    
                </div>
            </div>
        </div>
    </div>
  </div>

</div>

{{-- <div class="row">
  <div class="col-md-3">
    <div class="grid">
        <div class="grid-body">
           
              <div class="col-sm-12 mb-2">
                <center><b>Top 5 Trending Topik</b></center>
              </div>

            <div class="item-wrapper main-container">
                <div class="chart-container-1 trendingtopik">
                    
                </div>
            </div>
        </div>
    </div>
  </div>
</div> --}}

@endsection

@section('jsvendor')

{{-- <script src="{{ asset('daterangepicker/daterangepicker.min.js') }}"></script>
<script src="{{ asset('daterangepicker/daterangepicker.js') }}"></script> --}}

<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

<script>
$('#monthkeuangan').datepicker({
     minViewMode: 1,
     format: 'mm-yyyy'
});
$('#monthqtypenjualansales').datepicker({
     minViewMode: 1,
     format: 'mm-yyyy'
});
$('#monthtrknisiperformance').datepicker({
     minViewMode: 1,
     format: 'mm-yyyy'
});



$('#year1').datepicker({
     minViewMode: 2,
     format: 'yyyy'
});
</script>


<script type="text/javascript">
$(document).ready(function() {

  // $(".daterangepickernya").daterangepicker({
  //   alert('done');
  //   forceUpdate: true,
  //   callback: function(startDate, endDate, period){
  //     var title = startDate.format('L') + ' – ' + endDate.format('L');
  //     $(this).val(title)
  //   }
  // });

});
  $(function() {
      $('.daterangekeuangan').daterangepicker();

      // $('.daterangekeuangan').daterangepicker(
      //   forceUpdate: true,
      //   callback: function(startDate, endDate, period){
      //     var title = startDate.format('L') + ' – ' + endDate.format('L');
      //     $(this).val(title)
      //   }
      // );
  });
</script>

<script>
$(document).ready(function() {
  monthkeuangan();
  cekabsenline('nol');
  monthqtypenjualansales();
  monthtrknisiperformance();
  trendingtopik();

        // keuangan(); 
        
        // cekagroupsales();
        // cekagroupteknisi();
        
});
</script>

<script>
function monthkeuangan(){
  var monthkeuangan = $('.monthkeuangan').val();
  var witel = $('.witelkeuangan').val();

  var form_data = new FormData();

            form_data.append('monthkeuangan', monthkeuangan);
            form_data.append('witel', witel);

                $.ajax({
                        url: "{{ route('dashboard_view_keuangan') }}",
                        dataType: 'JSON', 
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        beforeSend: function (){
                                $(".preloader").show();
                            },
                        success: function(x) {
                          $('.totalsalesfee').html(x.totalsalesfee);
                          $('.totalteknisifee').html(x.totalteknisifee);

                          $('.keuangansalesfee').html(x.keuangansalesfee);
                          $('.keuanganteknisifee').html(x.keuanganteknisifee);
                        },
                        error: function (response) {
                            swal("Warning", 'Coba lagi', "warning");
                            $(".preloader").hide();
                        }
                });
                
}

function monthqtypenjualansales(){
  var monthkeuangan = $('.monthqtypenjualansales').val();
  // var witel = $('.witelkeuangan').val();

  var form_data = new FormData();

            form_data.append('monthkeuangan', monthkeuangan);
            // form_data.append('witel', witel);

                $.ajax({
                        url: "{{ route('dashboard_view_groupsales') }}",
                        dataType: 'JSON', 
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        beforeSend: function (){
                                $(".preloader").show();
                            },
                        success: function(x) {
                          $('.datamonthqtypenjualansales').html(x.qtysales);
                          $('.produksales').html(x.produksales);
                          $('.wilayahsales').html(x.wilayahsales);
                          $('.top5sales').html(x.top5sales);
                        },
                        error: function (response) {
                            swal("Warning", 'Coba lagi', "warning");
                            $(".preloader").hide();
                        }
                });
}

function monthtrknisiperformance(){
  var monthkeuangan = $('.monthtrknisiperformance').val();
  // var witel = $('.witelkeuangan').val();

  var form_data = new FormData();

            form_data.append('monthkeuangan', monthkeuangan);
            // form_data.append('witel', witel);

                $.ajax({
                        url: "{{ route('dashboard_view_groupteknisi') }}",
                        dataType: 'JSON', 
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        beforeSend: function (){
                                $(".preloader").show();
                            },
                        success: function(x) {
                          $('.qtyteknisi').html(x.qtyteknisi);
                          $('.produkteknisi').html(x.produkteknisi);
                          $('.wilayahteknisi').html(x.wilayahteknisi);
                          $('.top5teknisi').html(x.top5teknisi);
                        },
                        error: function (response) {
                            swal("Warning", 'Coba lagi', "warning");
                            $(".preloader").hide();
                        }
                });
}













function trendingtopik(){

  var form_data = new FormData();

            form_data.append('trendingtopik', '');

                $.ajax({
                        url: "{{ route('dashboard_view_trendingtopik') }}",
                        dataType: 'JSON', 
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        beforeSend: function (){
                                $(".preloader").show();
                            },
                        success: function(x) {
                          $('.trendingtopik').html(x.trendingtopik);
                        },
                        error: function (response) {
                            swal("Warning", 'Coba lagi', "warning");
                            $(".preloader").hide();
                        }
                });
}



function cekabsenline(minplus){
    var nilaiweek = parseInt($('.nilaiweek').val());

    if(minplus == 'nol'){
      var nilaiweek = 0;
    }else if(minplus == 'min'){
      var nilaiweek = nilaiweek-1;
    }else if(minplus == 'plus'){
      var nilaiweek = nilaiweek+1;
    }


    var form_data = new FormData();

   
            form_data.append('nilaiweek', nilaiweek);

                $.ajax({
                        url: "{{ route('dashboard_view_linechartabsen') }}",
                        dataType: 'JSON', 
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        beforeSend: function (){
                                $(".preloader").show();
                            },
                        success: function(x) {
                          $('.nilaiweek').val(nilaiweek);
                          $('.tanggalweek').html(x.tanggalweek);
                          $('.linechartabsen').html(x.linechartabsen);
                          $('.linechartabsenavghours').html(x.linechartabsenavghours);
                        },
                        error: function (response) {
                            swal("Warning", 'Coba lagi', "warning");
                            $(".preloader").hide();
                        }
                });

    

}





</script>



@endsection