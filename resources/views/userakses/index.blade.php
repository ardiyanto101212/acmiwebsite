@extends('template.ripleui')

@section('title')
    User Akses
@endsection

@section('cssvendor')
{{-- <link href="{{ asset('assets/bootstrap453/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"> --}}

<style>
    .contenttable {
            width: 100%;
            height: auto;
            overflow: auto;
            position: relative;
        }
</style>

@endsection

@section('breadcumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb has-arrow">
      <li class="breadcrumb-item"><a href="#">User Akses</a></li>
      {{-- <li class="breadcrumb-item active" aria-current="page">Overview</li> --}}
    </ol>
</nav>
@endsection

@section('content')
<div class="grid">
    <p class="grid-header">List Data <a href="{{ route('userakses_create_create') }}" class="btn btn-primary btn-sm mdi mdi-plus float-right"></a></p>
    <div class="grid-body">
        <div class="item-wrapper">
            <div class="row">
                
                <!-------------------- datanya disini ---------------------------->
                <div class="col-sm-12">
                    @if(session('alertprogress'))
                    <div class="alert alert-primary dismissible-alert" role="alert">
                        {{ session('alertprogress') }}<i class="alert-close mdi mdi-close"></i>
                    </div>
                    @endif
                </div>
                <div class="col-sm-3">
                    <form action="{{ route('userakses_view_index') }}" method="GET">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="text" class="form form-control" name="search" placeholder="Search Data">
                            
                        </div>
                    </form>
                </div>
                
                <br>
                <br>
                <div class="col-sm-12 contenttable">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Name</th>
                            <th style="text-align:center;width:10%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['pageorder'] as $list)
                        <tr>
                            <td style="vertical-align:middle;">
                                {{ $list->name }}
                            </td>
                            <td style="vertical-align:middle;"> 
                                @if($list->id != 1)
                                <div class="dropdown">
                                            
                                    <span class="btn btn-primary btn-sm dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Action
                                    </span>
                                
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="{{ url('useraksesedit/?id=').$list->id}}">
                                                Edit
                                            </a>
                                            <a class="dropdown-item" href="#" onclick="deletedata('{{ $list->id }}')">
                                                Delete
                                            </a>
                                            <a class="dropdown-item" href="#" onclick="mapgroupmenu('{{ $list->id }}')">
                                                Map Group Menu
                                            </a>
                                    </div>
                                </div>
                                @endif
                               
                            </td>
                        </tr>
                        
                        @endforeach
                    </tbody>
                </table>
                <br>
                <div class="col-sm-12">
                    <?php 
                        if($data['totalpage'] > 0){
                            echo $data['pageorder']->appends(request()->all())->links();
                        }  
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container" >	
	<!-- Modal -->
	<div class="modal fade" id="modaldata" role="dialog" >
  <div class="modal-dialog modal-lg">
  
	<!-- Modal content-->
		<div class="modal-content">
	
		  <div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		  <!-- <h3 class="text text-danger"><b>Input Data Admin</b></h3>
		  -->
		  </div>		
	  <div class="modal-body" >
		  <div class="card">
			  <div class="card-header">
                    Maping Menu
			  </div>
			  <div class="card-body">
				  <div class="datamenugrup">

                  </div>
			  </div>
		  </div>
	  </div>
	  </div>
  </div>
   </div>
</div>

@endsection

@section('jsvendor')

<script>
function deletedata(id) {

    swal({
        title: 'Are you sure ?',
        text: "delete this data !",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
    }).then(function (dismiss) {
      if (dismiss.value == true) {
        var form_data = new FormData();
    	form_data.append('id', id);

			$.ajax({
    				url: "{{ route('userakses_delete_destroy') }}", 
    				dataType: 'JSON', 
    				cache: false,
    				contentType: false,
    				processData: false,
    				data: form_data,
    				type: 'post',
    				beforeSend: function (){
							$(".preloader").show();
    					},
    				success: function(x) {
						// location.reload();
                        swal("Done", x.progress, "success");
                        window.location.href = "{{ route('userakses_view_index') }}";
    				},
    				error: function (response) {
    					swal("Warning", 'Try Again', "warning");
						$(".preloader").hide();
    				}
    		});
      }else{
        swal('Cancelled','Your data is safe','error');
      }
    });
    
}

function mapgroupmenu(id){
    

    var form_data = new FormData();
    	form_data.append('id', id);

			$.ajax({
    				url: "{{ route('userakses_view_ajaxmapmenu') }}", 
    				dataType: 'JSON', 
    				cache: false,
    				contentType: false,
    				processData: false,
    				data: form_data,
    				type: 'post',
    				beforeSend: function (){
							$(".preloader").show();
    					},
    				success: function(x) {
                        //alert(x.listdata);
                        $("#modaldata").modal('show');

                        $('.datamenugrup').html(x.listdata);
    				},
    				error: function (response) {
    					alert('Try Again');
						$(".preloader").hide();
    				}
    		});
}

//$onclick1 = "updateallow('allow_view','".$list->id."','".$id_groups."')";

function updateallow(fild,id_menus,id_groups){

    //alert(fild+' '+id_menus+' '+id_groups);

    var form_data = new FormData();
    	form_data.append('fild', fild);
        form_data.append('id_menus', id_menus);
        form_data.append('id_groups', id_groups);

			$.ajax({
    				url: "{{ route('userakses_update_ajaxupdatemap') }}", 
    				dataType: 'JSON', 
    				cache: false,
    				contentType: false,
    				processData: false,
    				data: form_data,
    				type: 'post',
    				beforeSend: function (){
							$(".preloader").show();
    					},
    				success: function(x) {
                        //alert(x.listdata);
                        //$("#modaldata").modal('show');

                        //$('.datamenugrup').html(x.listdata);
    				},
    				error: function (response) {
    					alert('Try Again');
						$(".preloader").hide();
    				}
    		});
            
}

</script>

@endsection