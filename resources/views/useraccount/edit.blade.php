@extends('template.ripleui')

@section('title')
    User Account
@endsection

@section('cssvendor')

<style>
    .contenttable {
            width: 100%;
            height: auto;
            overflow: auto;
            position: relative;
        }
</style>

@endsection

@section('breadcumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb has-arrow">
      <li class="breadcrumb-item"><a href="#">User Account</a></li>
      {{-- <li class="breadcrumb-item active" aria-current="page">Overview</li> --}}
    </ol>
</nav>
@endsection

@section('content')
<div class="grid">
    <p class="grid-header">Edit Data</p>
    <div class="grid-body">
        <div class="item-wrapper">
            <div class="row">
                
                <!-------------------- datanya disini ---------------------------->
                <div class="col-sm-12">
                    @if(session('alertprogress'))
                    <div class="alert alert-primary dismissible-alert" role="alert">
                        {{ session('alertprogress') }}<i class="alert-close mdi mdi-close"></i>
                    </div>
                    @endif
                </div>
               
                <div class="col-sm-12 contenttable">
                    <br>
                <form action="{{ route('user_update_update') }}" method="POST">
                    @csrf

                    <input type="hidden" class="form-control" name="id" value="{{ $dtarray['listdata']->id }}">

                    <div class="row mb-3">
                        <div class="col-md-12 mx-auto">
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area"><label for="inputType1">Name</label></div>
                                <div class="col-md-9 showcase_content_area">
                                    <input type="text" class="form-control @error('fullname') is-invalid @enderror" name="fullname" value="{{ old('fullname') ?? $dtarray['listdata']->fullname }}">
                                    @error('fullname')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area"><label for="inputType1">Username</label></div>
                                <div class="col-md-9 showcase_content_area">
                                    <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') ?? $dtarray['listdata']->username }}">
                                    @error('username')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area"><label for="inputType1">Password</label></div>
                                <div class="col-md-9 showcase_content_area">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}">
                                    @error('password')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area"><label for="inputType1">Email</label></div>
                                <div class="col-md-9 showcase_content_area">
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ?? $dtarray['listdata']->email }}">
                                    @error('email')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area"><label for="inputType1">Telepon</label></div>
                                <div class="col-md-9 showcase_content_area">
                                    <input type="text" class="form-control @error('telpnumber') is-invalid @enderror" name="telpnumber" value="{{ old('telpnumber') ?? $dtarray['listdata']->telpnumber }}">
                                    @error('telpnumber')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3 showcase_text_area"><label for="inputType1">Akses</label></div>
                                <div class="col-md-9 showcase_content_area">
                                    <select class="form-control" name="id_groups">
                                        @foreach( $dtarray['group'] as $list)
                                        <?php 
                                            if($dtarray['listdata']->id_groups == $list->id){
                                                $selected = 'selected';
                                            }else{
                                                $selected = '';
                                            }
                                        ?>
                                        
                                        @if($list->id != '1')
                                            <option value="{{ $list->id }}" <?php echo $selected ?>>{{ $list->name }}</option>
                                        @endif

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row showcase_row_area">
                                <div class="col-md-3">
                                    &nbsp;
                                </div>
                                <div class="col-md-9">
                                    <input type="submit" class="btn btn-success form-control text-white" value="Update">
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('jsvendor')

@endsection