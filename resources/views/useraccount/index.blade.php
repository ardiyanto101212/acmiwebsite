@extends('template.ripleui')

@section('title')
    User Account
@endsection

@section('cssvendor')
{{-- <link href="{{ asset('assets/bootstrap453/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"> --}}

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css">

<style>
    .contenttable {
            width: 100%;
            height: auto;
            overflow: auto;
            position: relative;
        }
</style>

@endsection

@section('breadcumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb has-arrow">
      <li class="breadcrumb-item"><a href="#">User Account</a></li>
      {{-- <li class="breadcrumb-item active" aria-current="page">Overview</li> --}}
    </ol>
</nav>
@endsection

@section('content')
<div class="grid">
    <p class="grid-header">List Data
        <a href="{{ route('user_create_create') }}" class="btn btn-primary btn-sm mdi mdi-plus float-right"></a>
        <a href="{{ route('user_view_indextrash') }}" class="btn btn-primary btn-sm float-right" style="margin-left:10px;margin-right:10px;"><i class="mdi mdi-delete-sweep"></i> Trash</a>
    </p>
    <div class="grid-body">
        <div class="item-wrapper">
            <div class="row">
                
                <!-------------------- datanya disini ---------------------------->
                <div class="col-sm-12">
                    @if(session('alertprogress'))
                    <div class="alert alert-primary dismissible-alert" role="alert">
                        {{ session('alertprogress') }}<i class="alert-close mdi mdi-close"></i>
                    </div>
                    @endif
                </div>
                <div class="col-sm-3">
                    <form action="{{ route('user_view_index') }}" method="GET">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="text" class="form form-control" name="search" placeholder="Search Data">
                        </div>
                    </form>
                </div>
                
                <br>
                <br>
                <div class="col-sm-12">
                <table class="table table-bordered table-striped" id="example">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Username</th>
                            <th style="text-align:center;">Nama</th>
                            <th style="text-align:center;">Email</th>
                            <th style="text-align:center;">Akses</th>
                            <th style="text-align:center;width:10%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['pageorder'] as $list)
                        <tr>
                            <td style="vertical-align:middle;">
                                {{ $list->username }}
                            </td>
                            <td style="vertical-align:middle;">
                                {{ $list->fullname }}
                            </td>
                            <td style="vertical-align:middle;">
                                {{ $list->email }}
                            </td>
                            <td style="vertical-align:middle;"> 
                                {{ $list->name }}
                            </td>
                            <td style="vertical-align:middle;"> 
                                @if($list->id_groups != '1')
                                <div class="dropdown">
                                            
                                    <span class="btn btn-primary btn-sm dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Action
                                    </span>
                                
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="{{ url('useraccountedit/?id=').$list->id}}">
                                                Edit
                                            </a>
                                            <a class="dropdown-item" href="#" onclick="deletedata('{{ $list->id }}')">
                                                Delete
                                            </a>
                                    </div>
                                </div>
                                @endif
                               
                            </td>
                        </tr>
                        
                        @endforeach
                    </tbody>
                </table>
                <br>
                <div class="col-sm-12">
                    <?php 
                        if($data['totalpage'] > 0){
                            echo $data['pageorder']->appends(request()->all())->links();
                        }  
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('jsvendor')

<script>
function deletedata(id) {

    swal({
        title: 'Are you sure ?',
        text: "delete this data !",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
    }).then(function (dismiss) {
      if (dismiss.value == true) {
        var form_data = new FormData();
    	form_data.append('id', id);

			$.ajax({
    				url: "{{ route('user_delete_destroy') }}", 
    				dataType: 'JSON', 
    				cache: false,
    				contentType: false,
    				processData: false,
    				data: form_data,
    				type: 'post',
    				beforeSend: function (){
							$(".preloader").show();
    					},
    				success: function(x) {
						// location.reload();
                        swal("Done", x.progress, "success");
                        window.location.href = "{{ route('user_view_index') }}";
    				},
    				error: function (response) {
    					swal("Warning", 'Try Again', "warning");
						$(".preloader").hide();
    				}
    		});
      }else{
        swal('Cancelled','Your data is safe','error');
      }
    });

}
</script>

<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>

<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            responsive: true,
            "paging": false,
            searching: false,
            "bInfo" : false,
            "ordering": false
        } );
     
        new $.fn.dataTable.FixedHeader( table );
    });
    </script>
@endsection