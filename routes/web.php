<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('landingpage.welcome');
    return redirect()->route('login');
    //return view('auth/login');
});

Route::get('/welcomde', function () {
    return view('welcome');
})->name('welcome_view_index');

Route::get('/sessionapihabis', function () {
    return response()->json([
        "response" => [
            "status"    => 200,
            "message"   => "Failed"
        ],
        "data" => null
    ], 200);
})->name('sessionapihabis');


Route::group(['middleware' =>['auth']], function () {
    
    Route::group(['middleware' => 'dynamicprivilege'], function () {
        
        //---------------- for user account
        Route::get('/useraccounttrash', 'Master\MasterUserController@indextrash')->name('user_view_indextrash');
        Route::get('/useraccountrestore', 'Master\MasterUserController@trashrestore')->name('user_view_restoredata');
        Route::get('/useraccountedit', 'Master\MasterUserController@edit')->name('user_update_edit');
        Route::post('/useraccountupdate', 'Master\MasterUserController@update')->name('user_update_update');
        Route::post('/useraccountdelete', 'Master\MasterUserController@destroy')->name('user_delete_destroy');
        Route::resource('useraccount', 'Master\MasterUserController', [
            'names' => [
                'index'     => 'user_view_index',
                'show'      => 'user_view_show',
                'create'    => 'user_create_create', // user/create with methode GET
                'store'     => 'user_create_store', // user with methode POST
                'edit'      => 'user_update_edit', // user/{{id}}/edit with methode GET
                //'update'    => 'user_update_update', // user/{{id}} with methode PUT
                //'destroy'   => 'user_delete_destroy', // user/{{id}} with methode DELETE
            ]
        ]);
        //---------------- 

        //---------------- for user akses
        Route::get('/useraksesedit', 'Master\MasterGroupController@edit')->name('userakses_update_edit');
        Route::post('/useraksesupdate', 'Master\MasterGroupController@update')->name('userakses_update_update');
        Route::post('/useraksesdelete', 'Master\MasterGroupController@destroy')->name('userakses_delete_destroy');
        Route::post('/ajaxmapmenu', 'Master\MasterGroupController@ajaxmapmenu')->name('userakses_view_ajaxmapmenu');
        Route::post('/ajaxupdatemapmenu', 'Master\MasterGroupController@ajaxupdatemapmenu')->name('userakses_update_ajaxupdatemap');

        Route::resource('userakses', 'Master\MasterGroupController', [
            'names' => [
                'index'     => 'userakses_view_index',
                'show'      => 'userakses_view_show',
                'create'    => 'userakses_create_create', // user/create with methode GET
                'store'     => 'userakses_create_store', // user with methode POST
                'edit'      => 'userakses_update_edit', // user/{{id}}/edit with methode GET
                //'update'    => 'user_update_update', // user/{{id}} with methode PUT
                //'destroy'   => 'user_delete_destroy', // user/{{id}} with methode DELETE
            ]
        ]);
        
        //---------------- for slider
        Route::get('/slideredit', 'Master\SliderController@edit')->name('slider_update_edit');
        Route::post('/sliderupdate', 'Master\SliderController@update')->name('slider_update_update');
        Route::post('/sliderdelete', 'Master\SliderController@destroy')->name('slider_delete_destroy');

        Route::resource('slider', 'Master\SliderController', [
            'names' => [
                'index'     => 'slider_view_index',
                'show'      => 'slider_view_show',
                'create'    => 'slider_create_create', // user/create with methode GET
                'store'     => 'slider_create_store', // user with methode POST
                'edit'      => 'slider_update_edit', // user/{{id}}/edit with methode GET
                //'update'    => 'user_update_update', // user/{{id}} with methode PUT
                //'destroy'   => 'user_delete_destroy', // user/{{id}} with methode DELETE
            ]
        ]);

        //---------------- for location
        Route::get('/locationedit', 'Master\ProvinceController@edit')->name('location_update_edit');
        Route::get('/locationtrash', 'Master\ProvinceController@indextrash')->name('location_view_trash');
        Route::get('/locationrestore', 'Master\ProvinceController@trashrestore')->name('location_update_trashrestore');

        Route::post('/locationupdate', 'Master\ProvinceController@update')->name('location_update_update');
        Route::post('/locationdelete', 'Master\ProvinceController@destroy')->name('location_delete_destroy');

        Route::resource('location', 'Master\ProvinceController', [
            'names' => [
                'index'     => 'location_view_index',
                'show'      => 'location_view_show',
                'create'    => 'location_create_create', // user/create with methode GET
                'store'     => 'location_create_store', // user with methode POST
                'edit'      => 'location_update_edit', // user/{{id}}/edit with methode GET
                //'update'    => 'user_update_update', // user/{{id}} with methode PUT
                //'destroy'   => 'user_delete_destroy', // user/{{id}} with methode DELETE
            ]
        ]);

        //---------------- for citi
        Route::get('/citiedit', 'Master\CitiController@edit')->name('citi_update_edit');
        Route::get('/cititrash', 'Master\CitiController@indextrash')->name('citi_view_trash');
        Route::get('/citirestore', 'Master\CitiController@trashrestore')->name('citi_update_trashrestore');

        Route::post('/citiupdate', 'Master\CitiController@update')->name('citi_update_update');
        Route::post('/citidelete', 'Master\CitiController@destroy')->name('citi_delete_destroy');

        Route::resource('citi', 'Master\CitiController', [
            'names' => [
                'index'     => 'citi_view_index',
                'show'      => 'citi_view_show',
                'create'    => 'citi_create_create', // user/create with methode GET
                'store'     => 'citi_create_store', // user with methode POST
                'edit'      => 'citi_update_edit', // user/{{id}}/edit with methode GET
                //'update'    => 'user_update_update', // user/{{id}} with methode PUT
                //'destroy'   => 'user_delete_destroy', // user/{{id}} with methode DELETE
            ]
        ]);

        //---------------- for district
        Route::get('/districtedit', 'Master\DistrictsController@edit')->name('district_update_edit');
        Route::get('/districttrash', 'Master\DistrictsController@indextrash')->name('district_view_trash');
        Route::get('/districtrestore', 'Master\DistrictsController@trashrestore')->name('district_update_trashrestore');

        Route::post('/districtupdate', 'Master\DistrictsController@update')->name('district_update_update');
        Route::post('/districtdelete', 'Master\DistrictsController@destroy')->name('district_delete_destroy');

        Route::resource('district', 'Master\DistrictsController', [
            'names' => [
                'index'     => 'district_view_index',
                'show'      => 'district_view_show',
                'create'    => 'district_create_create', // user/create with methode GET
                'store'     => 'district_create_store', // user with methode POST
                'edit'      => 'district_update_edit', // user/{{id}}/edit with methode GET
                //'update'    => 'user_update_update', // user/{{id}} with methode PUT
                //'destroy'   => 'user_delete_destroy', // user/{{id}} with methode DELETE
            ]
        ]);

        //---------------- for village
        Route::get('/villageedit', 'Master\VillageController@edit')->name('village_update_edit');
        Route::get('/villagetrash', 'Master\VillageController@indextrash')->name('village_view_trash');
        Route::get('/villagerestore', 'Master\VillageController@trashrestore')->name('village_update_trashrestore');

        Route::post('/villageupdate', 'Master\VillageController@update')->name('village_update_update');
        Route::post('/villagedelete', 'Master\VillageController@destroy')->name('village_delete_destroy');

        Route::resource('village', 'Master\VillageController', [
            'names' => [
                'index'     => 'village_view_index',
                'show'      => 'village_view_show',
                'create'    => 'village_create_create', // user/create with methode GET
                'store'     => 'village_create_store', // user with methode POST
                'edit'      => 'village_update_edit', // user/{{id}}/edit with methode GET
                //'update'    => 'user_update_update', // user/{{id}} with methode PUT
                //'destroy'   => 'user_delete_destroy', // user/{{id}} with methode DELETE
            ]
        ]);

        //----------------- detail location 
        Route::get('/locationdetailinfo', 'Master\ProvinceController@detaillocation')->name('location_view_productlistinfo');
        Route::post('/ajaxaddcity', 'Master\ProvinceController@ajaxaddcity')->name('location_view_ajaxaddcity');
        Route::post('/ajaxdeletecity', 'Master\ProvinceController@ajaxdeletecity')->name('location_view_ajaxdeletecity');
        Route::post('/ajaxgetcity', 'Master\ProvinceController@ajaxgetcity')->name('location_view_ajaxgetcity');
        //--------------------------------
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

