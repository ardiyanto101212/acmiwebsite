<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/getabsenbyid', function () {
//     return 'hallo';
// })->name('user_view_list');

Route::get('/getnotifikasibroadcast', 'Api\NotifikasiController@getnotifikasibroadcast')->name('user_view_getnotifikasibroadcast');
Route::post('/updatenotifikasibroadcastterbaca', 'Api\NotifikasiController@updatenotifikasibroadcastterbaca')->name('user_view_updatenotifikasibroadcastterbaca');

Route::group(['middleware' =>'auth:api'], function () {

    Route::group(['middleware' => 'dynamicprivilege'], function () {
        Route::get('/user/list', 'Master\MasterUserController@list')->name('user_view_list');
        
        Route::get('/pengajuan', function () {
            // $tes = Auth::user()->token();
            // dd($tes);
            return 'hallo';
        })->name('user_view_list');

        Route::post('/postabsensi', 'Api\AbsensiController@storeabsensi')->name('user_view_cekabsensi');

        //------------------ input order 
        // Route::post('/inputorder', 'Api\AbsensiController@storeabsensi')->name('user_view_cekabsensi');
    });
    //Route::post('/login', 'Auth\LoginController@login')->name('LoginApi');
});   

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();

//     // Route::post('/login', 'Auth\LoginController@login')->name('LoginApi');
//     // Route::post('/login', 'Auth\LoginController@login')->name('LoginApi');
//     // Route::post('/login', 'Auth\LoginController@login')->name('LoginApi');
//     // Route::post('/login', 'Auth\LoginController@login')->name('LoginApi');
// });


Route::post('/login', 'Auth\LoginController@login')->name('LoginApi');
