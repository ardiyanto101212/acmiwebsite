<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\Shared\SharedController;
use App\Http\Controllers\Shared\CarbonperiodeController;

use App\Models\User;
use App\Models\MasterGroup;
use App\Models\Mprovince;
use App\Models\Mcitie;
use App\Models\District;
use App\Models\Village;

use DataTables;
use Auth;
use DB;

use Illuminate\Support\Str;

use Carbon\Carbon;

class MasterUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $menus;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('User');
            return $next($request);
        });

        $this->mdistric = new District;
    }

    public function index()
    {   
        date_default_timezone_set('Asia/Jakarta');

        $userid = Auth::User()->id;

        $arayselect = [
            "master_users.id",
            "master_users.username",
            "master_users.fullname",
            "master_users.email",
            "master_users.id_groups",
            "master_groups.name"
        ];

        $searchdata = DB::table('master_users')->join("master_groups",'master_users.id_groups','=','master_groups.id')
        ->orderby('master_users.fullname','ASC')
        ->orderby('master_groups.name','ASC')
        ->select($arayselect)
        ->when(request()->search, function($searchdata) {
            $searchdata = $searchdata
            ->where(function($searchdata){
                $searchdata
                ->where('master_users.fullname', 'like', '%'. request()->search . '%' )
                ->orwhere('master_users.email', 'like', '%'. request()->search . '%' )
                ->orwhere('master_groups.name', 'like', '%'. request()->search . '%' )
                ;
            });
        })
        ->whereNull('master_users.deleted_at')
        ->paginate(50);

        $data = [
            "totalpage" => $searchdata->total(),
            "pageorder" => $searchdata
        ];
        //  if($searchdata->total() == 0){
        //      return redirect()->route('user_view_index')
        //      ->with('alertprogress',"No Data");
        //  }else{
            return view('useraccount.index',compact('data'));
        //  }
    }

    public function indextrash()
    {   
        date_default_timezone_set('Asia/Jakarta');

        $userid = Auth::User()->id;

        $arayselect = [
            "master_users.id",
            "master_users.username",
            "master_users.fullname",
            "master_users.email",
            "master_groups.name"
        ];

        $searchdata = DB::table('master_users')->join("master_groups",'master_users.id_groups','=','master_groups.id')
        ->orderby('master_users.fullname','ASC')
        ->orderby('master_groups.name','ASC')
        ->select($arayselect)
        ->when(request()->search, function($searchdata) {
            $searchdata = $searchdata
            ->where(function($searchdata){
                $searchdata
                ->where('master_users.fullname', 'like', '%'. request()->search . '%' )
                ->orwhere('master_users.email', 'like', '%'. request()->search . '%' )
                ->orwhere('master_groups.name', 'like', '%'. request()->search . '%' )
                ;
            });
        })
        ->whereNotNull('master_users.deleted_at')
        ->paginate(50);

        $data = [
            "totalpage" => $searchdata->total(),
            "pageorder" => $searchdata
        ];
        //  if($searchdata->total() == 0){
        //      return redirect()->route('user_view_index')
        //      ->with('alertprogress',"No Data");
        //  }else{
            return view('useraccount.trash',compact('data'));
        //  }
    }

    public function list()
    {
        date_default_timezone_set('Asia/Jakarta');

        $user = Auth::user();
        $userGroup = $user->group;
      
        $model = User::query(); //with('group'); //->query();

        $dataTables = DataTables::of($model)
                        ->editColumn('id_groups', function ($data) {
                            return $data->group->name;
                        })
                        ->addColumn('show', function ($data) {
                            return route('user_view_show', ['user' => $data->id]);
                        })
                        ->addColumn('edit', function ($data) {
                            $url = route('user_update_edit', ['user' => $data->id]);
                            return $url;
                        });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        date_default_timezone_set('Asia/Jakarta');

        $group = MasterGroup::get();
        return view('useraccount.create', compact('group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();

        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            'fullname'   => 'Name',
            'username'   => 'Username',
            'password'   => 'Password',
            'email'      => 'Email',
            'telpnumber' => 'Telepon'
        ]; 

        $required = $this->validate($request, [
            'fullname'   => 'required|max:500',
            'username'   => 'required',
            'password'   => 'required',
            'email'      => 'required',
            'telpnumber' => 'required'
            //'id_groups'  => 'required'
        ],$messages,$niceNames);

        if($required){
            
            $qcekemail = User::where('email',strtolower($data['email']))->get()->first();
            // $group = MasterGroup::get();
            // dd($qcekemail);
            try {
                DB::beginTransaction();
                //code...
               
                if(!empty($qcekemail == null)){
                   
                    $saveData = new User;
                    $saveData->username        = $data['username'];//$this->documentRencanaKota;
                    $saveData->fullname        = $data['fullname'];//$documentStep->id;
                    $saveData->email           = strtolower($data['email']);
                    $saveData->password        = bcrypt($data['password']);
                    $saveData->telpnumber      = $data['telpnumber']; // nullable
                    $saveData->id_groups       = $data['id_groups'];
                    
                    $saveData->save();
                   
                    DB::commit();
                    return redirect()->route('user_view_index')->with('alertprogress', 'User berhasil di buat');
                }else{
                    DB::commit();
                    return redirect()->route('user_create_create')->with(['alertprogress' => 'Email tidak boleh sama']);
                }
                
                
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('user_create_create')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('user_create_create')->with(['alertprogress' => 'Data Gagal Dibuat!']);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        date_default_timezone_set('Asia/Jakarta');

        $model = User::where('id', $id)->first();
        return view('master.master_user.show', ['model'=>$model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {   
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['id'];
        $model = User::where('id', $id)->first();
        $group = MasterGroup::get();

        $dtarray = [
            'listdata'  => $model,
            'group'     => $group
        ];

        return view('useraccount.edit', compact('dtarray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();

        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            'fullname'   => 'Name',
            'username'   => 'Username',
            // 'password'   => 'Password',
            'email'      => 'Email',
            'telpnumber' => 'Telepon'
        ]; 

        $required = $this->validate($request, [
            'fullname'   => 'required|max:500',
            'username'   => 'required',
            // 'password'   => 'required',
            'email'      => 'required',
            'telpnumber' => 'required'
            //'id_groups'  => 'required'
        ],$messages,$niceNames);
        
        if($required){
            
            $qcekemail = User::where('email',strtolower($data['email']))->get()->first();
            // $group = MasterGroup::get();
            // dd($qcekemail);
            if($data['password'] == ''){
                $dtarray = [
                    'username'      => $data['username'],
                    'fullname'      => $data['fullname'],
                    'email'         => strtolower($data['email']),
                    // 'password'      => bcrypt($data['password']),
                    'telpnumber'    => $data['telpnumber'],
                    'id_groups'     => $data['id_groups']
                ];
            }else{
                $dtarray = [
                    'username'      => $data['username'],
                    'fullname'      => $data['fullname'],
                    'email'         => strtolower($data['email']),
                    'password'      => bcrypt($data['password']),
                    'telpnumber'    => $data['telpnumber'],
                    'id_groups'     => $data['id_groups']
                ];
            }
            
            try {
                DB::beginTransaction();
                //code...
               
                if(!empty($qcekemail == null)){
                   
                    User::where('id',$data['id'])->update($dtarray);
                   
                    DB::commit();
                    return redirect()->route('user_view_index')->with('alertprogress', 'User berhasil di edit');
                }else{
                    if($qcekemail->id == $data['id']){
                        User::where('id',$data['id'])->update($dtarray);
                       
                        DB::commit();
                        return redirect()->route('user_view_index')->with('alertprogress', 'User berhasil di edit');
                    }else{
                        DB::commit();
                        return redirect()->route('user_update_update')->with(['alertprogress' => 'Email tidak boleh sama']);
                    }
                    
                }
                
                
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('user_update_update')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('user_update_update')->with(['alertprogress' => 'Data Gagal Di Edit!']);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $request->all();
        $id = $data['id'];

        DB::table('master_users')
        ->where('id',$id)
        ->update(['deleted_at' => Carbon::now()]);


        // $data = $request->all();
        // $id = $data['id'];
        // User::destroy($id);

        $jsonarray = [
            'listdata' => 'success'
        ];

        echo json_encode($jsonarray);
    }

    public function trashrestore()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id    = $_GET['id'];

        // User::withTrashed()
        // ->where('id', $id)
        // ->restore();

        User::where('id', $id)
        ->update(['deleted_at' => null]);
        
        return redirect()->route('user_view_indextrash')->with('alertprogress', 'Data berhasil di kembalikan');
    }


    //--------------------------- dashboard
    

    public function dashboard()
    {   
        date_default_timezone_set('Asia/Jakarta');
        $witel  = $this->mdistric->getcitiprovince();

        $shared = new CarbonperiodeController;
        $dtperiode = $shared->logikamingguan(0);

        $tglawal  = $dtperiode['awal'];
        $tglakhir = $dtperiode['akhir'];

        $data = [
            "witel"     => $witel,
            'weekawal'  => date('d/m/Y',strtotime($tglawal)),
            'weekakhir' => date('d/m/Y',strtotime($tglakhir)),
        ];
        return view('dashboard',compact('data'));
    }
    
}
