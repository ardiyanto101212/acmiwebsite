<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\User;
use App\Models\MasterGroup;

use App\Models\Mprovince;
use App\Models\Mcitie;

use Carbon\Carbon;
use DB;
use Auth;

class ProvinceController extends Controller
{
    protected $menus;
    private $modelbuilder;
    private $modelbuilderciti;
  
    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('User');
            return $next($request);
        });

        $this->modelbuilder = new Mprovince;
        $this->modelbuilderciti = new Mcitie;
    }

    public function index()
    {
        date_default_timezone_set('Asia/Jakarta');

        $searchdata = $this->modelbuilder->qalldata();

        $data = [
            "totalpage" => $searchdata->total(),
            "pageorder" => $searchdata
        ];

        return view('location.index',compact('data'));
    }

    public function indextrash()
    {
        date_default_timezone_set('Asia/Jakarta');

        $searchdata = $this->modelbuilder->qalldatatrash();

        $data = [
            "totalpage" => $searchdata->total(),
            "pageorder" => $searchdata
        ];

        return view('location.trash',compact('data'));
    }

    public function trashrestore()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id    = $_GET['id'];

        $this->modelbuilder->restoreprovince($id);
        
        return redirect()->route('location_view_trash')->with('alertprogress', 'Data berhasil di kembalikan');
    }

    public function create()
    {
        date_default_timezone_set('Asia/Jakarta');

        $group = "";
        return view('location.create', compact('group'));
    }

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();

        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            "name"  => 'Province'
        ]; 

        $required = $this->validate($request, [
            "name"  => 'required'
        ],$messages,$niceNames);
       
        if($required){
            
            try {
                DB::beginTransaction();
                //code...
            
                $this->modelbuilder->insertdata($data);
                $alertprogress = 'Data berhasil di buat';

                DB::commit();
                return redirect()->route('location_view_index')->with('alertprogress', $alertprogress);
              
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('location_create_create')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('location_create_create')->with(['alertprogress' => 'Data Gagal Dibuat!']);
        }
    }

    public function show($id)
    {
        
    }

    public function edit()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id         = $_GET['id'];
        $model      = $this->modelbuilder->getprovincebyid($id);
        $dtarray    = [
            'listdata'  => $model
        ];

        return view('location.edit', compact('dtarray'));
    }

    public function update(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();
        
        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            "name"  => 'Province'
        ]; 

        $required = $this->validate($request, [
            "name"  => 'required'
        ],$messages,$niceNames);
       
        if($required){
            
            try {
                DB::beginTransaction();
                //code...
                
                $this->modelbuilder->updatedata($data);
                
                DB::commit();
                return redirect()->route('location_view_index')->with('alertprogress', 'Data berhasil di edit');
              
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('location_update_update')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('location_update_update')->with(['alertprogress' => 'Data Gagal Di Edit!']);
        }
    }

    public function destroy(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data   = $request->all();
        $id     = $data['id'];
        
        $this->modelbuilder->deletedata($id);

        $progress = "Delete Success";
        
        $jsonarray = [
            'progress' => $progress
        ];

        echo json_encode($jsonarray);
    }

    //--------------- detail location 
    public function detaillocation()
    {
   
        date_default_timezone_set('Asia/Jakarta');

        $id     = $_GET['id'];
        
        $model  = $this->modelbuilder->getprovincebyid($id);
        $data   = [
            'detaildata'  => $model
        ];

        return view('location.detailinfo', compact('data'));
    }

    public function ajaxgetcity(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();
        $id   = $data['id'];

        $view = $this->templatedetail($id);

        $json = [
            'listdata'  => $view
        ];

        echo json_encode($json);
    }

    public function templatedetail($id){
        
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->modelbuilderciti->getcitibyidprovince($id);
        
        $view = '';
        $view .= view('location.detailinfoview',compact('data'));

        return $view;
    }
    
    public function ajaxaddcity(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();
        
        $norandom = mt_rand(1000, 9999);

        $array = [
            'province_id'   => $data['province_id'],
            'city_id'       => date('His').$norandom,
            'name'          => $data['name']
        ];

        Mcitie::create($array);

        $json = [
            'alertprogress'  => 'Data berhasil dibuat'
        ];

        echo json_encode($json);
    }

    public function ajaxdeletecity(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();
        
        $this->modelbuilderciti->deletecitie($data['id']);

        $json = [
            'alertprogress'  => 'Data berhasil dihapus'
        ];

        echo json_encode($json);
    }
    //-------------------------------
}
