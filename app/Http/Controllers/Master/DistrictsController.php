<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\User;
use App\Models\MasterGroup;

use App\Models\Mprovince;
use App\Models\Mcitie;
use App\Models\District;
use App\Models\Village;

use Carbon\Carbon;
use DB;
use Auth;


class DistrictsController extends Controller
{
    protected $menus;
    private $modelbuilder;
    private $modelbuilderciti;
  
    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('User');
            return $next($request);
        });

        $this->modelbuilder = new District;
    }

    public function index()
    {
        date_default_timezone_set('Asia/Jakarta');

        $searchdata = $this->modelbuilder->qalldata();

        $data = [
            "totalpage" => $searchdata->total(),
            "pageorder" => $searchdata
        ];

        return view('location.district.index',compact('data'));
    }

    public function indextrash()
    {
        date_default_timezone_set('Asia/Jakarta');

        $searchdata = $this->modelbuilder->qalldatatrash();

        $data = [
            "totalpage" => $searchdata->total(),
            "pageorder" => $searchdata
        ];

        return view('location.district.trash',compact('data'));
    }

    public function trashrestore()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id    = $_GET['id'];

        $this->modelbuilder->restoredata($id);
        
        return redirect()->route('district_view_trash')->with('alertprogress', 'Data berhasil di kembalikan');
    }

    public function create()
    {   
        $dataoption  = $this->modelbuilder->getcitiprovince();
        $group = [
            'dataoption' => $dataoption
        ];
        return view('location.district.create', compact('group'));
    }

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();

        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            "regency_id"  => 'Citi',
            "name"        => 'District'
        ]; 

        $required = $this->validate($request, [
            "regency_id"   => 'required',
            "name"         => 'required'
        ],$messages,$niceNames);
       
        if($required){
            
            try {
                DB::beginTransaction();
                //code...
            
                $this->modelbuilder->insertdata($data);
                $alertprogress = 'Data berhasil di buat';

                DB::commit();
                return redirect()->route('district_view_index')->with('alertprogress', $alertprogress);
              
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('district_create_create')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('district_create_create')->with(['alertprogress' => 'Data Gagal Dibuat!']);
        }
    }

    public function show($id)
    {
        
    }

    public function edit()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id          = $_GET['id'];
        $model       = $this->modelbuilder->getdatabyid($id);
        $dataoption  = $this->modelbuilder->getcitiprovince();
        $dtarray     = [
            'listdata'      => $model,
            'dataoption'    => $dataoption
        ];

        return view('location.district.edit', compact('dtarray'));
    }

    public function update(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();
        
        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            "name"          => 'District',
            "regency_id"    => "Citi"
        ]; 

        $required = $this->validate($request, [
            "name"          => 'required',
            "regency_id"    => 'required'
        ],$messages,$niceNames);
       
        if($required){
            
            try {
                DB::beginTransaction();
                //code...
                
                $this->modelbuilder->updatedata($data);
                
                DB::commit();
                return redirect()->route('district_view_index')->with('alertprogress', 'Data berhasil di edit');
              
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('district_update_update')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('district_update_update')->with(['alertprogress' => 'Data Gagal Di Edit!']);
        }
    }

    public function destroy(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data   = $request->all();
        $id     = $data['id'];
        
        $this->modelbuilder->deletedata($id);

        $progress = "Delete Success";
        
        $jsonarray = [
            'progress' => $progress
        ];

        echo json_encode($jsonarray);
    }
}
