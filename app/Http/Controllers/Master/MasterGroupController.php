<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\MapGroupMenu;

use Auth;
use DB;

class MasterGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $menus;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('User');
            return $next($request);
        });
    }

    public function index()
    {
        date_default_timezone_set('Asia/Jakarta');

        $arayselect = [
            "master_groups.id",
            "master_groups.name"
        ];

        $searchdata = DB::table('master_groups')
        ->orderby('master_groups.name','ASC')
        ->select($arayselect)
        ->when(request()->search, function($searchdata) {
            $searchdata = $searchdata
            ->where(function($searchdata){
                $searchdata
                ->where('master_groups.name', 'like', '%'. request()->search . '%' )
                ;
            });
        })
        ->paginate(50);

        
        $data = [
            "totalpage" => $searchdata->total(),
            "pageorder" => $searchdata
        ];
        //  if($searchdata->total() == 0){
        //      return redirect()->route('userakses_view_index')
        //      ->with('alertprogress',"No Data");
        //  }else{
            return view('userakses.index',compact('data'));
        //  }
        //return view('master.master_user.index', ['menu' => $this->menus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        date_default_timezone_set('Asia/Jakarta');

        $group = "";
        return view('userakses.create', compact('group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();

        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            'name'   => 'Name'
        ]; 

        $required = $this->validate($request, [
            'name'   => 'required|max:100'
        ],$messages,$niceNames);
        
        if($required){
            
            try {
                DB::beginTransaction();
                //code...
                
                $cekdulu  = MasterGroup::where('name',$data['name'])->get()->first();
                
                if(!empty($cekdulu->id)){

                }else{
                    
                    $saveData = new MasterGroup;
                    $saveData->name        = $data['name'];
                    $saveData->save();
                    
                    $datauserakses  = MasterGroup::where('name',$data['name'])->get()->first();
                    $idmastergrup   = $datauserakses->id;
                    
                    $qmenuloop = MasterMenu::get();
                    // dd($qmenuloop);
                    foreach($qmenuloop as $list){
                        $idmenu = $list->id;

                        $qsameadmin = DB::table('map_groups_menus')
                                    ->where('id_groups','1') //1 samakan allownya dengan admin
                                    ->where('id_menus',$idmenu)
                                    ->get()
                                    ->first();

                        $dtarray = [
                                        'id_groups'      => $idmastergrup,
                                        'id_menus'       => $idmenu,
                                        'allow_view'     => '0',
                                        'allow_create'   => '0',
                                        'allow_update'   => '0',
                                        'allow_delete'   => '0',
                                        'allow_import'   => '0',
                                        'allow_export'   => '0',
                                        'option_view'    => $qsameadmin->option_view,
                                        'option_create'  => $qsameadmin->option_create,
                                        'option_update'  => $qsameadmin->option_update,
                                        'option_delete'  => $qsameadmin->option_delete,
                                        'option_import'  => $qsameadmin->option_import,
                                        'option_export'  => $qsameadmin->option_export
                                    ];

                        MapGroupMenu::create($dtarray);
                    }
                    
                }
                    
                    DB::commit();
                    return redirect()->route('userakses_view_index')->with('alertprogress', 'Data berhasil di buat');
              
                
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('userakses_create_create')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('userakses_create_create')->with(['alertprogress' => 'Data Gagal Dibuat!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id    = $_GET['id'];
        $model = MasterGroup::where('id', $id)->first();

        $dtarray = [
            'listdata'  => $model
        ];

        return view('userakses.edit', compact('dtarray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();

        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            'name'   => 'Name'
        ];

        $required = $this->validate($request, [
            'name'   => 'required|max:100'
        ],$messages,$niceNames);
        
        if($required){
            
           
            $dtarray = [
                'name'      => $data['name']
            ];

            try {
                DB::beginTransaction();
                //code...
               
                    MasterGroup::where('id',$data['id'])->update($dtarray);

                    // $dtarray2 = [
                    //     'option_view'    => '1',
                    //     'option_create'  => '1',
                    //     'option_update'  => '1',
                    //     'option_delete'  => '1',
                    //     'option_import'  => '0',
                    //     'option_export'  => '0'

                    // ];

                    // MapGroupMenu::where('id_groups',$data['id'])->update($dtarray2);

                   
                    DB::commit();
                    return redirect()->route('userakses_view_index')->with('alertprogress', 'Data berhasil di edit');
              
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('userakses_update_update')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('userakses_update_update')->with(['alertprogress' => 'Data Gagal Di Edit!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data   = $request->all();
        $id     = $data['id'];

        $qcekgroup = DB::table('master_users')->where('id_groups',$id)->get()->first();

        //dd($qcekgroup->id_groups);
        if(!empty($qcekgroup->id_groups)){
            $progress = "Gagal delete, Map Menu sedang di pakai";
        }else{
            MasterGroup::destroy($id);
            MapGroupMenu::where('id_groups',$id)->delete();

            $progress = "Delete Success";
        }
        

        $jsonarray = [
            'progress' => $progress
        ];

        echo json_encode($jsonarray);
    }

    public function ajaxupdatemapmenu(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data       = $request->all();
        $fild       = $data['fild'];
        $id_menus   = $data['id_menus'];
        $id_groups  = $data['id_groups'];

        $select = [
            'id',$fild
        ];

        $cekallow = MapGroupMenu::where('id_menus',$id_menus)
                ->where('id_groups',$id_groups)
                ->select($select)
                ->get()
                ->first();
        if($cekallow->$fild == "0"){
            $updatefild = '1';
        }else{
            $updatefild = '0';
        }
        //dd($updatefild);
        $dtarray = [
            $fild => $updatefild
        ];
        
        MapGroupMenu::where('id',$cekallow->id)
                    ->update($dtarray);

        $jsonarray = [
            'listdata'  => ''
        ];

        echo json_encode($jsonarray);
    }

    public function ajaxmapmenu(Request $request)
    {
        
        date_default_timezone_set('Asia/Jakarta');
        
        $data   = $request->all();
        $id_groups     = $data['id'];
        
        $user = Auth::user(); // auth user active

        //$id_groups      = $user->id_groups; // dapat id group
        //$name_groups    = $user->group->name; // dapat name di table master_groups
        
            $parentMenu = MasterMenu::where('sort', '=', '#')
                        ->orderby('category','ASC')
                        ->get();
     
        $print = '';
        $no = 1;
    
        foreach ($parentMenu as $menu) {
            $noplus = $no++; // buat id plus untuk head child menu

            $category   = $menu->category;
            $childMenu  = MasterMenu::where('sort', '!=', '#')
                            ->where('category',$category)
                            ->orderby('category','ASC')
                            ->orderby('sort','ASC')
                            ->get();

                            if(count($childMenu) <= 0){

                                $cekallow = MapGroupMenu::where('id_groups',$id_groups)
                                            ->where('id_menus',$menu->id)
                                            ->get()
                                            ->first();
                                
                                if($cekallow->allow_view == '1'){
                                    $view = 'checked';
                                }else{
                                    $view = '';
                                }

                                if($cekallow->allow_create == '1'){
                                    $create = 'checked';
                                }else{
                                    $create = '';
                                }

                                if($cekallow->allow_update == '1'){
                                    $update = 'checked';
                                }else{
                                    $update = '';
                                }

                                if($cekallow->allow_delete == '1'){
                                    $delete = 'checked';
                                }else{
                                    $delete = '';
                                }

                                if($cekallow->allow_import == '1'){
                                    $import = 'checked';
                                }else{
                                    $import = '';
                                }

                                if($cekallow->allow_export == '1'){
                                    $export = 'checked';
                                }else{
                                    $export = '';
                                }

                                $onclick1 = "updateallow('allow_view','".$menu->id."','".$id_groups."')";
                                $onclick2 = "updateallow('allow_create','".$menu->id."','".$id_groups."')";
                                $onclick3 = "updateallow('allow_update','".$menu->id."','".$id_groups."')";
                                $onclick4 = "updateallow('allow_delete','".$menu->id."','".$id_groups."')";
                                $onclick5 = "updateallow('allow_import','".$menu->id."','".$id_groups."')";
                                $onclick6 = "updateallow('allow_export','".$menu->id."','".$id_groups."')";

                                $print .='
                                <div class="grid">
                                    <p class="grid-header">'.$menu->menuname.'</p>
                                    <div class="item-wrapper">
                                    <div class="">
                                        <div class="row">';
                                        if($cekallow->option_view == '1'){
                                            $print .='
                                            <div class="col-sm ml-4">
                                                <input type="checkbox" '.$view.' onclick="'.$onclick1.'"> View
                                            </div>
                                            ';
                                        }

                                        if($cekallow->option_create == '1'){
                                            $print .='
                                            <div class="col-sm ml-4">
                                                <input type="checkbox" '.$create.' onclick="'.$onclick2.'"> Create
                                            </div>  
                                            ';
                                        }

                                        if($cekallow->option_update == '1'){
                                            $print .='
                                            <div class="col-sm ml-4">
                                                <input type="checkbox" '.$update.' onclick="'.$onclick3.'"> Update
                                            </div>
                                            ';
                                        }

                                        if($cekallow->option_delete == '1'){
                                            $print .='
                                            <div class="col-sm ml-4">
                                                <input type="checkbox" '.$delete.' onclick="'.$onclick4.'"> Delete
                                            </div>
                                            ';
                                        }

                                        if($cekallow->option_import == '1'){
                                            $print .='
                                            <div class="col-sm ml-4">
                                                <input type="checkbox" '.$import.' onclick="'.$onclick5.'"> Import
                                            </div>
                                            ';
                                        }

                                        if($cekallow->option_export == '1'){
                                            $print .='
                                            <div class="col-sm ml-4">
                                                <input type="checkbox" '.$export.' onclick="'.$onclick6.'"> Export
                                            </div>
                                            ';
                                        }

                                $print .='
                                        </div>
                                    </div>
                                    </div>
                                </div>';
                            }else{
                                $print .='
                                <div class="grid">
                                    <p class="grid-header">'.$menu->menuname.'</p>
                                    <div class="item-wrapper">
                                        <div class="">';

                                        foreach($childMenu as $list){
                                        
                                        $cekallow = MapGroupMenu::where('id_groups',$id_groups)
                                            ->where('id_menus',$list->id)
                                            ->get()
                                            ->first();
                                        
                                            if($cekallow->allow_view == '1'){
                                                $view = 'checked';
                                            }else{
                                                $view = '';
                                            }
            
                                            if($cekallow->allow_create == '1'){
                                                $create = 'checked';
                                            }else{
                                                $create = '';
                                            }
            
                                            if($cekallow->allow_update == '1'){
                                                $update = 'checked';
                                            }else{
                                                $update = '';
                                            }
            
                                            if($cekallow->allow_delete == '1'){
                                                $delete = 'checked';
                                            }else{
                                                $delete = '';
                                            }
            
                                            if($cekallow->allow_import == '1'){
                                                $import = 'checked';
                                            }else{
                                                $import = '';
                                            }
            
                                            if($cekallow->allow_export == '1'){
                                                $export = 'checked';
                                            }else{
                                                $export = '';
                                            }

                                        $onclick1 = "updateallow('allow_view','".$list->id."','".$id_groups."')";
                                        $onclick2 = "updateallow('allow_create','".$list->id."','".$id_groups."')";
                                        $onclick3 = "updateallow('allow_update','".$list->id."','".$id_groups."')";
                                        $onclick4 = "updateallow('allow_delete','".$list->id."','".$id_groups."')";
                                        $onclick5 = "updateallow('allow_import','".$list->id."','".$id_groups."')";
                                        $onclick6 = "updateallow('allow_export','".$list->id."','".$id_groups."')";
                                        
                                        $print .='
                                            
                                            <div class="row">
                                                <div class="col-sm-12 ml-4">
                                                    <i class="mdi mdi-arrow-right-drop-circle"></i> <b>'.$list->menuname.'</b>
                                                </div>';

                                                if($cekallow->option_view == '1'){
                                                    $print .='
                                                    <div class="col-sm ml-4">
                                                        <input type="checkbox" '.$view.' onclick="'.$onclick1.'"> View
                                                    </div>
                                                    ';
                                                }

                                                if($cekallow->option_create == '1'){
                                                    $print .='
                                                    <div class="col-sm ml-4">
                                                        <input type="checkbox" '.$create.' onclick="'.$onclick2.'"> Create
                                                    </div>
                                                    ';
                                                }

                                                if($cekallow->option_update == '1'){
                                                    $print .='
                                                    <div class="col-sm ml-4">
                                                        <input type="checkbox" '.$update.' onclick="'.$onclick3.'"> Update
                                                    </div>
                                                    ';
                                                }

                                                if($cekallow->option_delete == '1'){
                                                    $print .='
                                                    <div class="col-sm ml-4">
                                                        <input type="checkbox" '.$delete.' onclick="'.$onclick4.'"> Delete
                                                    </div>
                                                    ';
                                                }

                                                if($cekallow->option_import == '1'){
                                                    $print .='
                                                    <div class="col-sm ml-4">
                                                        <input type="checkbox" '.$import.' onclick="'.$onclick5.'"> Import
                                                    </div>
                                                    ';
                                                }

                                                if($cekallow->option_export == '1'){
                                                    $print .='
                                                    <div class="col-sm ml-4">
                                                        <input type="checkbox" '.$export.' onclick="'.$onclick6.'"> Export
                                                    </div>
                                                    ';
                                                }
                                             
                                               
                                    $print .='       
                                            </div>
                                                <div class="col-sm-12">
                                                    <br>
                                                </div>
                                                ';
                                        }


                                $print .='
                                        </div>
                                    </div>
                                </div>';
                            }

                    
                
        }

        //-- lempar return
        //return $printParentMenu;


        $jsonarray = [
            "listdata" =>$print
        ];

        echo json_encode($jsonarray);
    }
}
