<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\User;
use App\Models\MasterGroup;

use App\Models\Mprovince;
use App\Models\Mcitie;
use App\Models\District;
use App\Models\Village;

use Carbon\Carbon;
use DB;
use Auth;
class VillageController extends Controller
{
    protected $menus;
    private $modelbuilder;
    private $modelbuilderciti;
  
    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('User');
            return $next($request);
        });

        $this->modelbuilder = new Village;
    }

    public function index()
    {
        date_default_timezone_set('Asia/Jakarta');

        $searchdata = $this->modelbuilder->qalldata();

        $data = [
            "totalpage" => $searchdata->total(),
            "pageorder" => $searchdata
        ];

        return view('location.village.index',compact('data'));
    }

    public function indextrash()
    {
        date_default_timezone_set('Asia/Jakarta');

        $searchdata = $this->modelbuilder->qalldatatrash();

        $data = [
            "totalpage" => $searchdata->total(),
            "pageorder" => $searchdata
        ];

        return view('location.village.trash',compact('data'));
    }

    public function trashrestore()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id    = $_GET['id'];

        $this->modelbuilder->restoredata($id);
        
        return redirect()->route('village_view_trash')->with('alertprogress', 'Data berhasil di kembalikan');
    }

    public function create()
    {   
        date_default_timezone_set('Asia/Jakarta');

        $dataoption  = $this->modelbuilder->getcitiprovincedistrict();
        $group = [
            'dataoption' => $dataoption
        ];
        return view('location.village.create', compact('group'));
    }

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();

        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            "district_id" => 'District',
            "name"        => 'Village'
        ]; 

        $required = $this->validate($request, [
            "district_id"  => 'required',
            "name"         => 'required'
        ],$messages,$niceNames);
       
        if($required){
            
            try {
                DB::beginTransaction();
                //code...
            
                $this->modelbuilder->insertdata($data);
                $alertprogress = 'Data berhasil di buat';

                DB::commit();
                return redirect()->route('village_view_index')->with('alertprogress', $alertprogress);
              
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('village_create_create')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('village_create_create')->with(['alertprogress' => 'Data Gagal Dibuat!']);
        }
    }

    public function show($id)
    {
        
    }

    public function edit()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id          = $_GET['id'];
        $model       = $this->modelbuilder->getdatabyid($id);
        $dataoption  = $this->modelbuilder->getcitiprovincedistrict();
        $dtarray     = [
            'listdata'      => $model,
            'dataoption'    => $dataoption
        ];

        return view('location.village.edit', compact('dtarray'));
    }

    public function update(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();
        
        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            "name"          => 'Village',
            "district_id"   => 'District',
        ]; 

        $required = $this->validate($request, [
            "name"          => 'required',
            "district_id"   => 'required'
        ],$messages,$niceNames);
       
        if($required){
            
            try {
                DB::beginTransaction();
                //code...
                
                $this->modelbuilder->updatedata($data);
                
                DB::commit();
                return redirect()->route('village_view_index')->with('alertprogress', 'Data berhasil di edit');
              
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('village_update_update')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('village_update_update')->with(['alertprogress' => 'Data Gagal Di Edit!']);
        }
    }

    public function destroy(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data   = $request->all();
        $id     = $data['id'];
        
        $this->modelbuilder->deletedata($id);

        $progress = "Delete Success";
        
        $jsonarray = [
            'progress' => $progress
        ];

        echo json_encode($jsonarray);
    }
}
