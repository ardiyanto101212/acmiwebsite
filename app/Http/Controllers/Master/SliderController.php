<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\MasterGroup;

use Illuminate\Support\Facades\Storage;

use Carbon\Carbon;

use DB;
class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $menus;
    private $mbuilder;

  
    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('User');
            return $next($request);
        });

        $this->mbuilder = new Mslider;
    }

    public function index()
    {
        date_default_timezone_set('Asia/Jakarta');

        $searchdata = $this->mbuilder->qallkaryawan();

        $data = [
            "totalpage" => $searchdata->total(),
            "pageorder" => $searchdata
        ];

        return view('slider.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        date_default_timezone_set('Asia/Jakarta');

        $group = "";
        return view('slider.create', compact('group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();

        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            "m_slider_nomor"       => 'Nomor',
            "m_slider_title"       => 'Title',
            "m_slider_image"       => 'Image'
        ];

        $required = $this->validate($request, [
            "m_slider_nomor"       => 'required',
            "m_slider_title"       => 'required|max:20',
            'm_slider_image'       => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ],$messages,$niceNames);
       
        if($required){
            try {
                DB::beginTransaction();
                //code...
            
                    $image = $request->file('m_slider_image');
                    if($image != ''){
                        $image->storeAs('public/slider/', $image->hashName());

                        $dataarray = [
                            'm_slider_nomor'        => $data['m_slider_nomor'],
                            'm_slider_title'        => ucwords($data['m_slider_title']),
                            'm_slider_image'        => 'slider/'.$image->hashName()
                        ];
    
                        Mslider::create($dataarray);
                        $alertprogress = 'Data berhasil di buat';
                    }else{
                        $alertprogress = 'Data gagal di buat, silahkan masukan image';
                    }

                    DB::commit();
                    return redirect()->route('slider_view_index')->with('alertprogress', $alertprogress);
              
                
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('slider_create_create')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('slider_create_create')->with(['alertprogress' => 'Data Gagal Dibuat!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id    = $_GET['id'];
        $model = Mslider::where('m_slider_id', $id)->first();
        
        $dtarray = [
            'listdata'=> $model
        ];

        return view('slider.edit', compact('dtarray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();

        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            "m_slider_nomor"       => 'Nomor',
            "m_slider_title"       => 'Title'
        ];

        $required = $this->validate($request, [
            "m_slider_nomor"       => 'required',
            "m_slider_title"       => 'required|max:20'
            // 'm_slider_image'        => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ],$messages,$niceNames);

        if($required){
            
            try {
                
                DB::beginTransaction();
                //code...
             
                $image = $request->file('m_slider_image');
                if($image != ''){
                    $imageupdate = 'slider/'.$image->hashName();
                }else{
                    $imageupdate = $data['m_slider_imagelama'];
                }
                $dataarray = [
                    'm_slider_nomor'        => $data['m_slider_nomor'],
                    'm_slider_title'        => ucwords($data['m_slider_title']),
                    'm_slider_image'        => $imageupdate
                ];

                Mslider::where('m_slider_id',$data['m_slider_id'])->update($dataarray);
                
                if($image != ''){
                    $photo = 'public/'.$data['m_slider_imagelama'];
                    
                    if(!empty($data['m_slider_imagelama'])){
                        Storage::disk('local')->delete($photo);
                    }
                    $image->storeAs('public/slider', $image->hashName());
                }
                //-------
               
                DB::commit();
                return redirect()->route('slider_view_index')->with('alertprogress', 'Data berhasil di edit');
              
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('slider_update_update')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('slider_update_update')->with(['alertprogress' => 'Data Gagal Di Edit!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data   = $request->all();
        $id     = $data['id'];
        
        $qcek = Mslider::where('m_slider_id',$id)->get()->first();

        if(!empty($qcek->m_slider_image)){
            Storage::disk('local')->delete('public/'.$qcek->m_slider_image);
        }
        
        Mslider::where('m_slider_id',$id)->delete();

        $progress = "Delete Success";
        
        $jsonarray = [
            'progress' => $progress
        ];

        echo json_encode($jsonarray);
    }
}
