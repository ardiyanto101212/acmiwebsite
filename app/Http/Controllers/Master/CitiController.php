<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\User;
use App\Models\MasterGroup;

use App\Models\Mprovince;
use App\Models\Mcitie;

use Carbon\Carbon;
use DB;
use Auth;

class CitiController extends Controller
{
    protected $menus;
    private $modelbuilder;
    private $modelbuilderciti;
  
    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('User');
            return $next($request);
        });

        $this->modelbuilder = new Mcitie;
    }

    public function index()
    {
        date_default_timezone_set('Asia/Jakarta');

        $searchdata = $this->modelbuilder->qalldata();

        $data = [
            "totalpage" => $searchdata->total(),
            "pageorder" => $searchdata
        ];

        return view('location.citi.index',compact('data'));
    }

    public function indextrash()
    {
        date_default_timezone_set('Asia/Jakarta');

        $searchdata = $this->modelbuilder->qalldatatrash();

        $data = [
            "totalpage" => $searchdata->total(),
            "pageorder" => $searchdata
        ];

        return view('location.citi.trash',compact('data'));
    }

    public function trashrestore()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id    = $_GET['id'];

        $this->modelbuilder->restoreprovince($id);
        
        return redirect()->route('citi_view_trash')->with('alertprogress', 'Data berhasil di kembalikan');
    }

    public function create()
    {   
        $mprovince  = $this->modelbuilder->getprovince();
        $group = [
            'province' => $mprovince
        ];
        return view('location.citi.create', compact('group'));
    }

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();

        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            "province_id"  => 'Province',
            "name"  => 'Citi'
        ]; 

        $required = $this->validate($request, [
            "province_id"   => 'required',
            "name"          => 'required'
        ],$messages,$niceNames);
       
        if($required){
            
            try {
                DB::beginTransaction();
                //code...
            
                $this->modelbuilder->insertdata($data);
                $alertprogress = 'Data berhasil di buat';

                DB::commit();
                return redirect()->route('citi_view_index')->with('alertprogress', $alertprogress);
              
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('citi_create_create')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('citi_create_create')->with(['alertprogress' => 'Data Gagal Dibuat!']);
        }
    }

    public function show($id)
    {
        
    }

    public function edit()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id         = $_GET['id'];
        $model      = $this->modelbuilder->getdatabyid($id);
        $mprovince  = $this->modelbuilder->getprovince();
        $dtarray    = [
            'listdata'  => $model,
            'province'  => $mprovince
        ];

        return view('location.citi.edit', compact('dtarray'));
    }

    public function update(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $request->all();
        
        $messages = [
            'required' => ':attribute Required'
        ];

        $niceNames = [
            "name"          => 'Citi',
            "province_id"   => "Province"
        ]; 

        $required = $this->validate($request, [
            "name"          => 'required',
            "province_id"   => 'required'
        ],$messages,$niceNames);
       
        if($required){
            
            try {
                DB::beginTransaction();
                //code...
                
                $this->modelbuilder->updatedata($data);
                
                DB::commit();
                return redirect()->route('citi_view_index')->with('alertprogress', 'Data berhasil di edit');
              
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->route('citi_update_update')->with(['alertprogress' => $e]);
            }
            
        }else{
            return redirect()->route('citi_update_update')->with(['alertprogress' => 'Data Gagal Di Edit!']);
        }
    }

    public function destroy(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data   = $request->all();
        $id     = $data['id'];
        
        $this->modelbuilder->deletedata($id);

        $progress = "Delete Success";
        
        $jsonarray = [
            'progress' => $progress
        ];

        echo json_encode($jsonarray);
    }
}
