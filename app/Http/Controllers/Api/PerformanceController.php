<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Shared\SharedController;
use App\Http\Controllers\Shared\CarbonperiodeController;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;

use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Mcustomer;
use App\Models\Mslider;

use App\Models\Mcitie;
use App\Models\District;
use App\Models\Village;
use App\Models\Province;

use App\Models\Morderteknisi;
use App\Models\Mordersales;

use Carbon\Carbon;
use DB;
use Auth;
use URL;


class PerformanceController extends Controller
{
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->morderteknisi   = new Morderteknisi;
        $this->mordersales     = new Mordersales;
        $this->mcustomer       = new Mcustomer;
        $this->mkaryawan       = new Mkaryawan;
        $this->mslider         = new Mslider;
        $this->muser           = new User;
    }

    public function performancesales(Request $request){
        if($request->type == 'week'){
            $data = $this->monthperformancesales($request);
            if(!empty($data['show'])){
                return response()->json([
                    "response" => [
                        "status"    => 200,
                        "message"   => "Success"
                    ],
                    "data" => $data['datashow']
                ], 200);
            }else{
                return response()->json([
                    "response" => [
                        "status"    => 201,
                        "message"   => "Failed"
                    ],
                    "data" => null
                ], 201);
            }
            

        }else{
            $data = $this->yearsperformancesales($request);

            if(!empty($data['show'])){
                return response()->json([
                    "response" => [
                        "status"    => 200,
                        "message"   => "Success"
                    ],
                    "data" => $data['datashow']
                ], 200);
            }else{
                return response()->json([
                    "response" => [
                        "status"    => 201,
                        "message"   => "Failed"
                    ],
                    "data" => null
                ], 201);
            }
        }
    }

    public function monthperformancesales($request){
        $year = date('Y',strtotime($request->periode));
        $month = date('m',strtotime($request->periode));

        $shared = new CarbonperiodeController;
        $periode = $shared->getWeekRange($year,$month);
   
        $sumcace = '';
        $selectsumcase = '';

        $idkaryawan = $request->m_karyawan_idkaryawan;
        
        $no = 1;
        
        foreach($periode as $key => $value){
            $noplus = $no++;

            $explode = explode('&&',$value);
            $awal    = $explode[0];
            $akhir   = $explode[1];

            $selectsumcase .= "SUM(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' and t_ordersales_tanggal between '$awal' and '$akhir' THEN t_ordersales_fee ELSE 0 END) AS 'Minggu $noplus',";

            // array_push($selectsumcase,$selectsumcasepush);

            // $sumcace .= $awal;
        }
// dd(substr($selectsumcase, 0, -1));
        $arrayselect = [
            DB::raw(substr($selectsumcase, 0, -1)),
            DB::raw("count(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_ordersales_tanggal) = $year and MONTH(t_ordersales_tanggal) = $month THEN t_ordersales_fee ELSE 0 END) AS 'totalssl'"),
            DB::raw("CONCAT('Rp. ',FORMAT(sum(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_ordersales_tanggal) = $year and MONTH(t_ordersales_tanggal) = $month THEN t_ordersales_fee ELSE 0 END),0 )) totalallfeeperiodeini "),
            DB::raw("CONCAT('Rp. ',FORMAT(sum(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' THEN t_ordersales_fee ELSE 0 END),0 )) totalallfee "),
            DB::raw("count(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' THEN t_ordersales_fee ELSE 0 END) AS 'totalseluruhssl'")
        ];
        
        // array_merge($arrayselect,$selectsumcase);

        $query = DB::table('t_ordersales')
                ->whereYear('t_ordersales.t_ordersales_tanggal',$year)
                ->whereMonth('t_ordersales.t_ordersales_tanggal',$month)
                ->where('t_ordersales.m_karyawan_idkaryawan',$idkaryawan)
                ->groupby('t_ordersales.m_karyawan_idkaryawan')
                ->select($arrayselect)
                ->first();

        //--------------------------------------------------------------------------------------

        $tanggalrequest = date('Y-m-d',strtotime($request->periode));

        $kurangmonth = date('Y-m-d', strtotime('-1 month', strtotime($tanggalrequest)));
        $yearbefore = date('Y',strtotime($kurangmonth));
        $monthbefore = date('m',strtotime($kurangmonth));

        $arrayselect2 = [
            DB::raw("count(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_ordersales_tanggal) = $yearbefore and MONTH(t_ordersales_tanggal) = $monthbefore THEN t_ordersales_fee ELSE 0 END) AS 'totalssl'")
        ];

        $query2 = DB::table('t_ordersales')
                ->whereYear('t_ordersales.t_ordersales_tanggal',$yearbefore)
                ->whereMonth('t_ordersales.t_ordersales_tanggal',$monthbefore)
                ->where('t_ordersales.m_karyawan_idkaryawan',$idkaryawan)
                ->groupby('t_ordersales.m_karyawan_idkaryawan')
                ->select($arrayselect)
                ->first();


        if(!empty($query2->totalpemasangan)){
            $nilainow = $query->totalpemasangan;
            $hasiltotalpasang = ($nilainow-$query2->totalpemasangan)/$query2->totalpemasangan*100;
        }else{
            $hasiltotalpasang = '0';
        }

        if($hasiltotalpasang < 0){
            $nilaipersen = $hasiltotalpasang.'%';
        }else if($hasiltotalpasang == 0){
            $nilaipersen = $hasiltotalpasang.'%';
        }else if($hasiltotalpasang > 0){
            $nilaipersen = '+'.$hasiltotalpasang.'%';
        }

        $arraymergeplus = [
            "updownpersen" => $nilaipersen
        ];

        $arraypush = [];
        foreach($query as $key => $value){
            $arrayup = [
                $key => $value
            ];
            $arraypush += $arrayup;
        }

        $arraypush += $arraymergeplus;

        $datatoshow = [
            'show' => $query,
            'datashow' => $arraypush
        ];

        return $datatoshow;
    }

    public function yearsperformancesales($request){
        $years = $request->periode;
        $idkaryawan = $request->m_karyawan_idkaryawan;

        $selectsumcase = '';

        for($i=1;$i<=12;$i++){
            $datenya = $years.'-'.$i.'-01';
            $dateshow = date('F',strtotime($datenya));
            // YEAR(Date) = 2011 AND MONTH(Date) = 5
            $selectsumcase .= "SUM(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_ordersales_tanggal) = $years and MONTH(t_ordersales_tanggal) = $i THEN t_ordersales_fee ELSE 0 END) AS '$dateshow',";

        }

        $arrayselect = [
            DB::raw(substr($selectsumcase, 0, -1)),
            DB::raw("count(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_ordersales_tanggal) = $years THEN t_ordersales_fee ELSE 0 END) AS 'totalssl'"),
            DB::raw("CONCAT('Rp. ',FORMAT(sum(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_ordersales_tanggal) = $years THEN t_ordersales_fee ELSE 0 END),0 )) totalallfeeperiodeini "),
            DB::raw("CONCAT('Rp. ',FORMAT(sum(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' THEN t_ordersales_fee ELSE 0 END),0 )) totalallfee "),
            DB::raw("count(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' THEN t_ordersales_fee ELSE 0 END) AS 'totalseluruhssl'")
        ];
        
        // array_merge($arrayselect,$selectsumcase);

        $query = DB::table('t_ordersales')
                ->whereYear('t_ordersales.t_ordersales_tanggal',$years)
                ->where('t_ordersales.m_karyawan_idkaryawan',$idkaryawan)
                ->groupby('t_ordersales.m_karyawan_idkaryawan')
                ->select($arrayselect)
                ->first();

        $yearskurang = $years-1;

                $arrayselect2 = [
                    DB::raw("count(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_ordersales_tanggal) = $yearskurang THEN t_ordersales_fee ELSE 0 END) AS 'totalssl'"),
                        ];
        
                $query2 = DB::table('t_ordersales')
                        ->whereYear('t_ordersales.t_ordersales_tanggal',$yearskurang)
                        ->where('t_ordersales.m_karyawan_idkaryawan',$idkaryawan)
                        ->groupby('t_ordersales.m_karyawan_idkaryawan')
                        ->select($arrayselect)
                        ->first();
                
                if(!empty($query2->totalpemasangan)){
                    $nilainow = $query->totalpemasangan;
                    $hasiltotalpasang = ($nilainow-$query2->totalpemasangan)/$query2->totalpemasangan*100;
                }else{
                    $hasiltotalpasang = '0';
                }
        
                if($hasiltotalpasang < 0){
                    $nilaipersen = $hasiltotalpasang.'%';
                }else if($hasiltotalpasang == 0){
                    $nilaipersen = $hasiltotalpasang.'%';
                }else if($hasiltotalpasang > 0){
                    $nilaipersen = '+'.$hasiltotalpasang.'%';
                }
        
                $arraymergeplus = [
                    "updownpersen" => $nilaipersen
                ];
                
                $arraypush = [];
                foreach($query as $key => $value){
                    $arrayup = [
                        $key => $value
                    ];
                    $arraypush += $arrayup;
                }
        
                $arraypush += $arraymergeplus;
    $datatoshow = [
                    'show' => $query,
                    'datashow' => $arraypush
                ];
        
        return $datatoshow;


    }
    

    //---------------------- teknisi 

    public function performanceteknisi(Request $request){
        if($request->type == 'week'){
            $data = $this->monthperformanceteknisi($request);
            if(!empty($data['show'])){
                return response()->json([
                    "response" => [
                        "status"    => 200,
                        "message"   => "Success"
                    ],
                    "data" => $data['datashow']
                ], 200);
            }else{
                return response()->json([
                    "response" => [
                        "status"    => 201,
                        "message"   => "Failed"
                    ],
                    "data" => null
                ], 201);
            }
            

        }else{
            $data = $this->yearsperformanceteknisi($request);

            if(!empty($data['show'])){
                return response()->json([
                    "response" => [
                        "status"    => 200,
                        "message"   => "Success"
                    ],
                    "data" => $data['datashow']
                ], 200);
            }else{
                return response()->json([
                    "response" => [
                        "status"    => 201,
                        "message"   => "Failed"
                    ],
                    "data" => null
                ], 201);
            }
        }
    }

    public function monthperformanceteknisi($request){
        $year = date('Y',strtotime($request->periode));
        $month = date('m',strtotime($request->periode));

        $shared = new CarbonperiodeController;
        $periode = $shared->getWeekRange($year,$month);
   
        $sumcace = '';
        $selectsumcase = '';

        $idkaryawan = $request->m_karyawan_idkaryawan;
        
        $no = 1;

        foreach($periode as $key => $value){
            $noplus = $no++;

            $explode = explode('&&',$value);
            $awal    = $explode[0];
            $akhir   = $explode[1];

            $selectsumcase .= "SUM(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' and t_order_tanggalpasang between '$awal' and '$akhir' THEN t_teknisijob.t_teknisijob_totalfee ELSE 0 END) AS 'Minggu $noplus',";

            // array_push($selectsumcase,$selectsumcasepush);

            // $sumcace .= $awal;
        }
// dd(substr($selectsumcase, 0, -1));
        $arrayselect = [
            DB::raw(substr($selectsumcase, 0, -1)),
            DB::raw("count(CASE WHEN t_teknisijob.m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_teknisijob.t_order_tanggalpasang) = $year and MONTH(t_teknisijob.t_order_tanggalpasang) = $month THEN t_teknisijob.t_teknisijob_totalfee ELSE 0 END) AS 'totalpemasangan'"),
            DB::raw("CONCAT('Rp. ',FORMAT(sum(CASE WHEN t_teknisijob.m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_teknisijob.t_order_tanggalpasang) = $year and MONTH(t_teknisijob.t_order_tanggalpasang) = $month THEN t_teknisijob.t_teknisijob_totalfee ELSE 0 END),0 )) totalallfeeperiodeini "),
            DB::raw("CONCAT('Rp. ',FORMAT(sum(CASE WHEN t_teknisijob.m_karyawan_idkaryawan = '$idkaryawan' THEN t_teknisijob.t_teknisijob_totalfee ELSE 0 END),0 )) totalallfee "),
            DB::raw("count(CASE WHEN t_teknisijob.m_karyawan_idkaryawan = '$idkaryawan' THEN t_teknisijob.t_teknisijob_totalfee ELSE 0 END) AS 'totalseluruhpemasangan'")

        ];
        
        // array_merge($arrayselect,$selectsumcase);

        $query = DB::table('t_teknisijob')
                ->whereYear('t_teknisijob.t_order_tanggalpasang',$year)
                ->whereMonth('t_teknisijob.t_order_tanggalpasang',$month)
                ->where('t_teknisijob.m_karyawan_idkaryawan',$idkaryawan)
                ->groupby('t_teknisijob.m_karyawan_idkaryawan')
                ->select($arrayselect)
                ->first();

        //--------------------------------------------------------------------------------------

        $tanggalrequest = date('Y-m-d',strtotime($request->periode));

        $kurangmonth = date('Y-m-d', strtotime('-1 month', strtotime($tanggalrequest)));
        $yearbefore = date('Y',strtotime($kurangmonth));
        $monthbefore = date('m',strtotime($kurangmonth));

        $arrayselect2 = [
            DB::raw("count(CASE WHEN t_teknisijob.m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_teknisijob.t_order_tanggalpasang) = $yearbefore and MONTH(t_teknisijob.t_order_tanggalpasang) = $monthbefore THEN t_teknisijob.t_teknisijob_totalfee ELSE 0 END) AS 'totalpemasangan'")
        ];

        $query2 = DB::table('t_teknisijob')
                ->whereYear('t_teknisijob.t_order_tanggalpasang',$yearbefore)
                ->whereMonth('t_teknisijob.t_order_tanggalpasang',$monthbefore)
                ->where('t_teknisijob.m_karyawan_idkaryawan',$idkaryawan)
                ->groupby('t_teknisijob.m_karyawan_idkaryawan')
                ->select($arrayselect2)
                ->first();


        if(!empty($query2->totalpemasangan)){
            $nilainow = $query->totalpemasangan;
            $hasiltotalpasang = ($nilainow-$query2->totalpemasangan)/$query2->totalpemasangan*100;
        }else{
            $hasiltotalpasang = '0';
        }

        if($hasiltotalpasang < 0){
            $nilaipersen = $hasiltotalpasang.'%';
        }else if($hasiltotalpasang == 0){
            $nilaipersen = $hasiltotalpasang.'%';
        }else if($hasiltotalpasang > 0){
            $nilaipersen = '+'.$hasiltotalpasang.'%';
        }

        $arraymergeplus = [
            "updownpersen" => $nilaipersen
        ];

        $arraypush = [];
        foreach($query as $key => $value){
            $arrayup = [
                $key => $value
            ];
            $arraypush += $arrayup;
        }

        $arraypush += $arraymergeplus;

        $datatoshow = [
            'show' => $query,
            'datashow' => $arraypush
        ];

        return $datatoshow;

    }

    public function yearsperformanceteknisi($request){
        $years = $request->periode;
        $idkaryawan = $request->m_karyawan_idkaryawan;

        $selectsumcase = '';

        for($i=1;$i<=12;$i++){
            $datenya = $years.'-'.$i.'-01';
            $dateshow = date('F',strtotime($datenya));
            // YEAR(Date) = 2011 AND MONTH(Date) = 5
            $selectsumcase .= "SUM(CASE WHEN m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_order_tanggalpasang) = $years and MONTH(t_order_tanggalpasang) = $i THEN t_teknisijob_totalfee ELSE 0 END) AS '$dateshow',";

        }

        $arrayselect = [
            DB::raw(substr($selectsumcase, 0, -1)),
            DB::raw("count(CASE WHEN t_teknisijob.m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_teknisijob.t_order_tanggalpasang) = $years THEN t_teknisijob.t_teknisijob_totalfee ELSE 0 END) AS 'totalpemasangan'"),
            DB::raw("CONCAT('Rp. ',FORMAT(sum(CASE WHEN t_teknisijob.m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_teknisijob.t_order_tanggalpasang) = $years THEN t_teknisijob.t_teknisijob_totalfee ELSE 0 END),0 )) totalallfeeperiodeini "),
            DB::raw("CONCAT('Rp. ',FORMAT(sum(CASE WHEN t_teknisijob.m_karyawan_idkaryawan = '$idkaryawan' THEN t_teknisijob.t_teknisijob_totalfee ELSE 0 END),0 )) totalallfee "),
            DB::raw("count(CASE WHEN t_teknisijob.m_karyawan_idkaryawan = '$idkaryawan' THEN t_teknisijob.t_teknisijob_totalfee ELSE 0 END) AS 'totalseluruhpemasangan'")

        ];
        
        // array_merge($arrayselect,$selectsumcase);
    
        $query = DB::table('t_teknisijob')
                ->whereYear('t_teknisijob.t_order_tanggalpasang',$years)
                ->where('t_teknisijob.m_karyawan_idkaryawan',$idkaryawan)
                ->groupby('t_teknisijob.m_karyawan_idkaryawan')
                ->select($arrayselect)
                ->first();
        
        $yearskurang = $years-1;

        $arrayselect2 = [
                    DB::raw("count(CASE WHEN t_teknisijob.m_karyawan_idkaryawan = '$idkaryawan' and YEAR(t_teknisijob.t_order_tanggalpasang) = $yearskurang THEN t_teknisijob.t_teknisijob_totalfee ELSE 0 END) AS 'totalpemasangan'")
                ];

        $query2 = DB::table('t_teknisijob')
                ->whereYear('t_teknisijob.t_order_tanggalpasang',$yearskurang)
                ->where('t_teknisijob.m_karyawan_idkaryawan',$idkaryawan)
                ->groupby('t_teknisijob.m_karyawan_idkaryawan')
                ->select($arrayselect2)
                ->first();
        
        if(!empty($query2->totalpemasangan)){
            $nilainow = $query->totalpemasangan;
            $hasiltotalpasang = ($nilainow-$query2->totalpemasangan)/$query2->totalpemasangan*100;
        }else{
            $hasiltotalpasang = '0';
        }

        if($hasiltotalpasang < 0){
            $nilaipersen = $hasiltotalpasang.'%';
        }else if($hasiltotalpasang == 0){
            $nilaipersen = $hasiltotalpasang.'%';
        }else if($hasiltotalpasang > 0){
            $nilaipersen = '+'.$hasiltotalpasang.'%';
        }

        $arraymergeplus = [
            "updownpersen" => $nilaipersen
        ];
        
        $arraypush = [];
        foreach($query as $key => $value){
            $arrayup = [
                $key => $value
            ];
            $arraypush += $arrayup;
        }

        $arraypush += $arraymergeplus;

        $datatoshow = [
            'show' => $query,
            'datashow' => $arraypush
        ];

        return $datatoshow;
    }
    
    public function dataslider(){
        
        $data = $this->mslider->dataslider();

        if($data->count() > 0) {
            // $img = URL::to('/storage/'.$datalist->t_orderteknisi_badigital);

            $dtarray = [];
            $listimg = '';
            foreach($data as $list){
                $dtpush = [
                    // "m_slider_id"          => $list->m_slider_id,
                    // "m_slider_nomor"       => $list->m_slider_nomor,
                    // "m_slider_title"       => $list->m_slider_title,
                    "m_slider_image"       => URL::to('/storage/'.$list->m_slider_image)
                ];

                array_push($dtarray,$dtpush);

                $listimg .=URL::to('/storage/'.$list->m_slider_image).',';
            }

            $hasilimage = substr($listimg, 0, -1);

            // return response()->json([
            //     "response" => [
            //         "status"    => 200,
            //         "message"   => "Success"
            //     ],
            //     "data" => $dtarray
            // ], 200);

            return $hasilimage;

        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }
        
    }

    public function topperformancesales(){
        
        $data = $this->mordersales->qtopperformancesales();

        if($data->count() > 0) {
            // $img = URL::to('/storage/'.$datalist->t_orderteknisi_badigital);

            $dtarray = [];
            foreach($data as $list){
                $dtpush = [
                    "m_karyawan_idkaryawan"     => $list->m_karyawan_idkaryawan,
                    "m_karyawan_nik"            => $list->m_karyawan_nik,
                    "m_karyawan_nama"           => $list->m_karyawan_nama,
                    "m_title_name"              => $list->m_title_name,
                    "total"                     => $list->totaljual,
                    'province'                  => $list->provincename,
                    'city'                      => $list->cityname,
                    'sto'                       => $list->districtname
                ];

                array_push($dtarray,$dtpush);
            }

         
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $dtarray
            ], 200);

        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }
        
    }

    public function topperformanceteknisi(){
        
        $data = $this->morderteknisi->qtopperformanceteknisi();

        if($data->count() > 0) {
            // $img = URL::to('/storage/'.$datalist->t_orderteknisi_badigital);

            $dtarray = [];
            foreach($data as $list){
                $dtpush = [
                    "m_karyawan_idkaryawan"     => $list->m_karyawan_idkaryawan,
                    "m_karyawan_nik"            => $list->m_karyawan_nik,
                    "m_karyawan_nama"           => $list->m_karyawan_nama,
                    "m_title_name"              => $list->m_title_name,
                    "total"                     => $list->totalpasang,
                    'province'                  => $list->provincename,
                    'city'                      => $list->cityname,
                    'sto'                       => $list->districtname
                ];

                array_push($dtarray,$dtpush);
            }

         
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $dtarray
            ], 200);

        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }
        
    }
    
    public function getonofnotification(){
        $id = $_GET['m_karyawan_idkaryawan'] ?? null;

        $data = $this->muser->qgetonofnotification($id);

        if(!empty($data->m_karyawan_idkaryawan)) {
            
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }
        
    }

    public function updateonofnotification(Request $request){
        $data = $this->muser->qupdateonofnotification($request);

        if($data){
            
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => "Berhasil di update"
            ], 200);

        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }
    }
    
    
}
