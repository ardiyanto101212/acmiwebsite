<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Shared\SharedController;
use App\Http\Controllers\Shared\NotifikasiController;

use Illuminate\Support\Facades\Storage;

use App\Models\Mabsensi;
use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\Mcategoryproduct;
use App\Models\Mcitie;
use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Mchatkaryawan;
use App\Models\Mdiskusi;
use App\Models\Mdiskusidetail;
use App\Models\Mkontakmember;
use App\Models\Mbroadcast;
use App\Models\Mchatcustomer;
use App\Models\Mcustomerakun;

use Carbon\Carbon;
use DB;
use Auth;

class ChatcustomerController extends Controller
{
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->mkaryawan = new Mkaryawan;
        $this->mcatkaryawan = new Mchatkaryawan;
        $this->mdiskusi = new Mdiskusi;
        $this->mdiskusidetail = new Mdiskusidetail;
        $this->mkontakmember = new Mkontakmember;
        $this->muser = new User;
        $this->mbroadcast = new Mbroadcast;
        $this->mchatcustomer = new Mchatcustomer;
        $this->mcustomerakun = new Mcustomerakun;
    }

    public function getchatcustomerlist(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mchatcustomer->qgetchatcustomerlist($request);
        
        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function storechatcustomer(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
    
        $data = $this->mchatcustomer->storechat($request);
        
        if($data){
            
            $qdttokendevice = $this->mkaryawan->gettokendevicekaryawan($request->m_karyawan_idkaryawan);
            
            if(!empty($qdttokendevice->master_users_devicekey)){

                $devicetoken = $qdttokendevice->master_users_devicekey;
                
                if($qdttokendevice->master_users_devicekeyonof == 'On'){
                    
                    $message = [
                        "title" => "Pesan Baru", 
                        "body" => $request->t_chatcustomer_message
                    ];
    
                    $shared = new NotifikasiController;
                    $shared->sendnotifchat($devicetoken,$message);
                    
                }
                
            }else{
                
                $qcekakuncus = $this->mcustomerakun->cekdevicekey($request->m_karyawan_idkaryawan);
               
                if(!empty($qcekakuncus->master_users_devicekey)){
                   
                    if($qcekakuncus->master_users_devicekeyonof == 'On'){
                        
                        $message = [
                            "title" => "Pesan Baru", 
                            "body" => $request->t_chatcustomer_message
                        ];
        
                        $shared = new NotifikasiController;
                        $shared->sendnotifchat($qcekakuncus->master_users_devicekey,$message);
                        
                    }
                }
                
            }
            
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di input'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }

            
    }

    public function gethistorychatkaryawantocustomer()
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $id = $_GET['m_karyawan_idkaryawan'] ?? null;

        $data = $this->mchatcustomer->historychatkaryawantocustomer($id);
        
        if($data->count() > 0) {

            $arraydt = [];
            foreach($data as $list){
                $idrecheiver = $list->m_customerakun_id;

                $qmessage = $this->mchatcustomer->listchatcustomertokaryawan($id,$idrecheiver);

                $arraypush = [
                    't_chatcustomer_message' => $qmessage->t_chatcustomer_message ?? '',
                    'm_customerakun_id'      => $idrecheiver,
                    'm_customerakun_nik'     => $list->m_customerakun_nik,
                    'm_customerakun_nama'    => $list->m_customerakun_nama,
                    "m_customerakun_email"   => $list->m_customerakun_email,
                    'm_customerakun_hp'      => $list->m_customerakun_hp,
                    'created_at'             => date('d-m-Y H:i:s',strtotime($qmessage->created_at))
                ];
    
                array_push($arraydt,$arraypush);
            }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydt
            ], 200);

        }else{
            
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }


    public function gethistorychatcustomertokaryawan()
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $id = $_GET['m_customerakun_id'] ?? null;

        $data = $this->mchatcustomer->qgethistorychatkaryawan($id);
        
        if($data->count() > 0) {

            $arraydt = [];
            foreach($data as $list){
                $idrecheiver = $list->m_karyawan_idkaryawan;

                $qmessage = $this->mchatcustomer->qgethistorychatkaryawantocustomer($id,$idrecheiver);

                $arraypush = [
                    't_chatcustomer_message'    => $qmessage->t_chatcustomer_message ?? '',
                    'm_karyawan_idkaryawan'     => $idrecheiver,
                    'm_karyawan_nama'           => $list->m_karyawan_nama,
                    'title_id'                  => $list->title_id,
                    "title_name"                => $list->title_name,
                    'm_karyawan_lattitude'      => $list->m_karyawan_lattitude,
                    'm_karyawan_longtitude'     => $list->m_karyawan_longtitude,
                    'created_at'                => date('d-m-Y H:i:s',strtotime($qmessage->created_at))
                ];
    
                array_push($arraydt,$arraypush);
            }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydt
            ], 200);

        }else{
            
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
        
    }
    
    

}
