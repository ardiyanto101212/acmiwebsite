<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Http\Controllers\Shared\SharedController;
use Illuminate\Support\Facades\Storage;

use App\Models\Mabsensi;
use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\Mcategoryproduct;

use App\Models\Mprovince;
use App\Models\Mcitie;
use App\Models\District;
use App\Models\Village;

use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Morder;
use App\Models\Mcustomer;
use App\Models\Mconfigurasi;

use Carbon\Carbon;
use DB;
use Auth;

class ProvinceController extends Controller
{
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->morder       = new Morder;
        $this->mcustomer    = new Mcustomer;
        $this->mconfigurasi = new Mconfigurasi;
        $this->mkaryawan    = new Mkaryawan;
        $this->mprovice     = new Mprovince;
        $this->mciti        = new Mcitie;
        $this->mdistrict    = new District;
        $this->mvillage     = new Village;

    }
    
    public function dataprovince()
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mprovice->qdataprovince();

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function citybyprovince()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['province_id'];

        $data = $this->mciti->getcitibyidprovince($id);

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function citybyprovincename(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $data = $this->mciti->qcitybyprovincename($datapost);

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function kecamatanbycityname(Request $request) 
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mdistrict->qkecamatanbycityname($request);

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function kelurahanbykecamatanname(Request $request) 
    {
        date_default_timezone_set('Asia/Jakarta');
     
        $data = $this->mvillage->qkelurahanbykecamatanname($request);

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    

    public function teknisibyprovcity(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $data = $this->mprovice->qteknisibyprovcity($datapost);

        if($data->count() >0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
    
}
