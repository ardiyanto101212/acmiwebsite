<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Shared\SharedController;
use Illuminate\Support\Facades\Storage;

use App\Models\Mabsensi;
use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\Mcategoryproduct;

use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Mcustomer;

use App\Models\Mcitie;
use App\Models\District;
use App\Models\Village;
use App\Models\Province;
use App\Models\Morder;
use App\Models\Mconfigurasi;

use Carbon\Carbon;
use DB;
use Auth;

class OrderController extends Controller
{
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->morder       = new Morder;
        $this->mcustomer    = new Mcustomer;
        $this->mconfigurasi = new Mconfigurasi;
        $this->mkaryawan    = new Mkaryawan;
    }

    // 200 OK
    // 202 Accepted
    // 200 No Content
    // 400 Bad Request
    // 200 Not Found

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $qceknik = Mcustomer::where('m_customer_nik',$datapost['m_customer_nik'])->get()->first();
        if(!empty($qceknik->m_customer_nik)){
            $returndata = 'Data gagal di input, karena NIK customer sudah ada yang memiliki';
        }else{
            $this->morder->storeorder($datapost);

            $qcekorderno = Morder::where('t_order_idnumber',$datapost['t_order_idnumber'])
                            ->where('m_karyawan_idkaryawan',$datapost['m_karyawan_idkaryawan'])    
                            ->get()->first();
    
            $this->mcustomer->storecustomer($datapost,$qcekorderno);

            $returndata = 'Data berhasil di input';
        }

        

        // $data = [
        //     "order"    => $returndata
        // ];

        $data = $returndata;

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function storecustomer(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $datenow = date('Y-m-d');

        $datapost = $request->all();

        $qceknik = Mcustomer::where('m_customer_nik',$datapost['m_customer_nik'])->get()->first();

        if(!empty($qceknik->m_customer_nik)){
            $returndata = 'Data gagal di input, karena NIK customer sudah ada yang memiliki';
        }else{
            $this->morder->storecustomeronly($datapost);

            $qcekidorder = Morder::where('m_karyawan_idkaryawan', $datapost['m_karyawan_idkaryawan'])
                            ->whereNotIn('t_order_id', DB::table('m_customer')->pluck('m_customer.t_order_id'))
                            ->whereNull('t_order_idnumber')
                            ->get()
                            ->first();
                            
    
            $this->mcustomer->storecustomeronly($datapost,$qcekidorder);

            $returndata = 'Data berhasil di input';
            
        }

        // $data = [
        //     "order"    => $returndata
        // ];

        $data = $returndata;

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function updatestoreorder(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $qcekcustomer = Mcustomer::where('m_customer_nik',$datapost['m_customer_nik'])
                        ->get()
                        ->first();
        

        $this->morder->updatestoreorder($datapost,$qcekcustomer);
        $this->mcustomer->updatestorecustomer($qcekcustomer);

        // $qcekorderno = Morder::where('t_order_idnumber',$datapost['t_order_idnumber'])
        //             ->where('m_karyawan_idkaryawan',$datapost['m_karyawan_idkaryawan'])    
        //             ->get()->first();

        // $this->mcustomer->updatestorecustomer($qcekorderno);

        // $data = [
        //     "order"    => 'Data berhasil di input'
        // ];

        $data = 'Data berhasil di update';

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function updatestatusorder(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $datapost = $request->all();

        $this->morder->updatestatusorder($datapost);
        $this->mconfigurasi->savenullconfigurasi($datapost);

        // $data = [
        //     "order"    => 'Data berhasil di input'
        // ];

        $data = 'Data berhasil di update';

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function storeconfigurasi(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $qcekorder = Morder::where('t_order_idnumber',$datapost['t_order_idnumber'])
                    ->get()->first();

        $saveconv = $this->mconfigurasi->saveconfiguration($datapost,$qcekorder);

        // $data = [
        //     "order"    => 'Data berhasil di input'
        // ];

        $data = 'Data berhasil di input';

        if($saveconv) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function storeconfigurasinotdate(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $datapost = $request->all();

        $this->morder->updatestatusorder($datapost);
        $this->mconfigurasi->savenullconfigurasinotdate($datapost);

        // $data = [
        //     "order"    => 'Data berhasil di input'
        // ];

        $data = 'Data berhasil di update';

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }
    

    public function dtcustomernik(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $data = $this->mcustomer->dtcustomernik($datapost);

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function dtorderstore(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $data = $this->morder->dtorderbyorderno($datapost);

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function dtconfigurasi(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $data = $this->mconfigurasi->dtconfbynikandorderno($datapost);

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function ajukanorderketeknisi(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $data = $this->mconfigurasi->ajukanteknisi($datapost);

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function updatetglpasangteknisionly(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $data = $this->mconfigurasi->updatetglpasangteknisionly($datapost);

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }
    
    public function tolakordersales(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $data = $this->mconfigurasi->tolakordersales($datapost);

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }
    
    
    public function dtorderbyidsales()
    {
        date_default_timezone_set('Asia/Jakarta');

        $nik = $_GET['idsales'] ?? null;

        $data = $this->morder->qdtorderbyidsales($nik);

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function dtordercustomerbynik()
    {
        date_default_timezone_set('Asia/Jakarta');

        $nik = $_GET['nik'] ?? null;

        $data = $this->morder->dtorderbynikcustomer($nik);

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }
    
    public function dtorderteknisi()
    {
        date_default_timezone_set('Asia/Jakarta');

        $nik = $_GET['idteknisi'] ?? null;

        $data = $this->morder->qdtorderteknisi($nik);

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function rprtsales()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['idsales'] ?? null;

        $data = $this->morder->qreportordersales($id);

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function rprtsalesdetail()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['ordernumber'] ?? null;

        $data = $this->morder->qrprtsalesdetail($id);

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }
    

    public function rprtsalestotalpenjualan()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['idsales'] ?? null;

        $data = $this->morder->qrprtsalestotalpenjualan($id);

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function rprtteknisi()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['idteknisi'] ?? null;

        $data = $this->morder->qrprtteknisi($id);

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function rprtteknisidetail()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['ordernumber'] ?? null;

        $data = $this->morder->qrprtteknisidetail($id);

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function rprttotaljobteknisi()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['idteknisi'] ?? null;

        $data = $this->morder->qtotalselesai($id);
        $data2 = $this->morder->qtotalseluruh($id);

        if($data) {

            $dataup = [
                'totalselesai' => $data->totalselesai,
                'totalseluruh' => $data2->totalseluruh,
            ];

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $dataup
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }
    
    public function teknisibywitelsales()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['idsales'] ?? null;

        $datacek = $this->mkaryawan->cekkaryawan($id);
        $data    =  $this->mkaryawan->cekteknisiwitelsales($datacek->m_karyawan_witel ?? null);

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function salesall()
    {
        date_default_timezone_set('Asia/Jakarta');

        $datacek = $this->mkaryawan->qsalesall();

        if($datacek->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $datacek
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function teknisiall()
    {
        date_default_timezone_set('Asia/Jakarta');

        $datacek = $this->mkaryawan->qteknisiall();

        if($datacek->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $datacek
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }
    
    
    public function historyorderstatus()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['t_order_idnumber'] ?? null;

        $data    =  $this->morder->historyorder($id);

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }
    

}
