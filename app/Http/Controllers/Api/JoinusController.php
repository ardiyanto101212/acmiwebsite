<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Shared\SharedController;
use App\Http\Controllers\Shared\CarbonperiodeController;
use App\Http\Controllers\Shared\NotifikasiController;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;

use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Mcustomer;

use App\Models\Mcitie;
use App\Models\District;
use App\Models\Village;
use App\Models\Province;
use App\Models\Mkasbonrequest;
use App\Models\Mjoinus;

use App\Models\Morderteknisi;

use Carbon\Carbon;
use DB;
use Auth;
use URL;

class JoinusController extends Controller
{
    protected $menus;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->mjoinus   = new Mjoinus;
    }

    public function storejoinus(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        // $datapost = $request->all();

        $cekdata = $this->mjoinus->ceknikandemail($request);

        if(empty($cekdata->m_joinus_id)){

            $imagedata = $request->file('m_joinus_fotoselfie');
            if($imagedata != ''){
                $imagedata->storeAs('public/joinus/', $imagedata->hashName());
                $filename      = 'joinus/'.$imagedata->hashName();
            }else{
                $filename = null;
            }


            $imagedata2 = $request->file('m_joinus_fotoktp');
            if($imagedata2 != ''){
                $imagedata2->storeAs('public/joinus/', $imagedata2->hashName());
                $filename2      = 'joinus/'.$imagedata2->hashName();
            }else{
                $filename2 = null;
            }

            $this->mjoinus->storejoinus($request,$filename,$filename2);

            $returndata = 'Data berhasil di input';
            $resstat = '1';
        }else{
            $returndata = 'Data telah ada, Nik atau Email sudah ada yang memiliki';
            $resstat = '2';
        }

        if($resstat == '1'){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $returndata
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => $returndata
            ], 201);
        }

        
    }
}
