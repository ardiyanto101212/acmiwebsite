<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Shared\SharedController;
use Illuminate\Support\Facades\Storage;

use App\Models\Mabsensi;
use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\Mcategoryproduct;
use App\Models\Mcitie;
use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Mchatkaryawan;
use App\Models\Mcustomer;
use App\Models\Mcustomerakun;

use Carbon\Carbon;
use DB;
use Auth;

class DaftarcustomerController extends Controller
{
    
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->mcustomer = new Mcustomer;
        $this->muser = new User;
        $this->mcustomerakun = new Mcustomerakun;
    }

    public function daftarcustomerlogin(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $qcekemail = User::where('email',strtolower($datapost['m_customer_email']))->get()->first();

        if(!empty($qcekemail->email)){
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => 'Email sudah ada, Gunakan email lain'
            ], 201);
        }else{
            $cekemialandnik = $this->mcustomerakun->cekemailandnikcsakun($datapost);
            
            if(!empty($cekemialandnik->m_customerakun_nik)){
                $this->muser->qdaftarcustomerlogin($datapost);
            }else{
                $this->mcustomerakun->qdaftarcustomerlogin($datapost);
                $this->muser->qdaftarcustomerlogin($datapost);
            }

            $subject    = "Notifikasi Login";
            $to         = strtolower($datapost['m_customer_email']);
            $receiver   = ucwords(strtolower($datapost['m_customer_nama']));
            $content    = '
                    <p>Kepada Yth.
                        <br>
                        Berikut adalah akses login anda ke dalam applikasi VEM : 
                    </p>
                    <br>
                    <p>
                        Username : '.strtolower($datapost['m_customer_email']).'
                        <br>
                        Password : '.$datapost['m_customer_password'].'
                    </p>
                    <br>
                    Silahkan Jaga Kerahasiaan Akun Anda !
                    ';

            $shared = new SharedController;
            $shared->sendemail($subject,$to,$receiver,$content);

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil disimpan, Silahkan login'
            ], 200);
        }

    }

    public function ubahusernamecustomer(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $cekdataakun = $this->mcustomerakun->cekdataakun($request);

        $emaildata = $cekdataakun->m_customerakun_email;
        $updatedata = $this->muser->editusernamebyemail($emaildata,$request->m_customerakun_username);

        if($updatedata){

            $subject    = "Notifikasi Rubah Username";
            $to         = $emaildata;
            $receiver   = ucwords(strtolower($cekdataakun->m_customerakun_nama));
            $content    = '
                    <p>Kepada Yth.
                        <br>
                        Berikut adalah username anda yang berhasil di rubah : 
                    </p>
                    <br>
                    <p>
                        Username : '.$request->m_customerakun_username.'
                    </p>
                    <br>
                    Silahkan Jaga Kerahasiaan Akun Anda !
                    ';

            $shared = new SharedController;
            $shared->sendemail($subject,$to,$receiver,$content);
            
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di update'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => 'Data gagal di update'
            ], 201);
        }
    }

    public function ubahpasswordcustomer(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $cekdataakun = $this->mcustomerakun->cekdataakun($request);

        $emaildata = $cekdataakun->m_customerakun_email;
        $updatedata = $this->muser->editpasswordbyemail($emaildata,$request->m_customerakun_password);

        if($updatedata){

            $subject    = "Notifikasi Rubah Password";
            $to         = $emaildata;
            $receiver   = ucwords(strtolower($cekdataakun->m_customerakun_nama));
            $content    = '
                    <p>Kepada Yth.
                        <br>
                        Berikut adalah password anda yang berhasil di rubah : 
                    </p>
                    <br>
                    <p>
                        Username : '.$request->m_customerakun_password.'
                    </p>
                    <br>
                    Silahkan Jaga Kerahasiaan Akun Anda !
                    ';

            $shared = new SharedController;
            $shared->sendemail($subject,$to,$receiver,$content);
            
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di update'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => 'Data gagal di update'
            ], 201);
        }
    }

    public function ubahemailcustomer(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');


        $qcekemail = User::where('email',strtolower($request->m_customerakun_email))->get()->first();

        if(!empty($qcekemail->email)){
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => 'Email sudah ada, Gunakan email lain'
            ], 201);
        }else{
            $cekdataakun = $this->mcustomerakun->cekdataakun($request);

            $emaildata = $cekdataakun->m_customerakun_email;
            $updatedata = $this->muser->editemailcustomer($emaildata,$request->m_customerakun_email);
            $this->mcustomerakun->ubahemailcustomerakun($request->m_customerakun_id,$request->m_customerakun_email);

            if($updatedata){

                $subject    = "Notifikasi Rubah Email";
                $to         = strtolower($request->m_customerakun_email);
                $receiver   = ucwords(strtolower($cekdataakun->m_customerakun_nama));
                $content    = '
                        <p>Kepada Yth.
                            <br>
                            Berikut adalah password anda yang berhasil di rubah : 
                        </p>
                        <br>
                        <p>
                            Email : '.strtolower($request->m_customerakun_email).'
                        </p>
                        <br>
                        Silahkan Jaga Kerahasiaan Akun Anda !
                        ';
    
                $shared = new SharedController;
                $shared->sendemail($subject,$to,$receiver,$content);
                
                return response()->json([
                    "response" => [
                        "status"    => 200,
                        "message"   => "Success"
                    ],
                    "data" => 'Data berhasil di update'
                ], 200);
            }else{
                return response()->json([
                    "response" => [
                        "status"    => 201,
                        "message"   => "Failed"
                    ],
                    "data" => 'Data gagal di update'
                ], 201);
            }
        }

       

        
    }

    
}
