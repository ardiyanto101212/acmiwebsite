<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Http\Controllers\Shared\SharedController;
use App\Http\Controllers\Shared\NotifikasiController;

use Illuminate\Support\Facades\Storage;

use App\Models\Mabsensi;
use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\Mcategoryproduct;
use App\Models\Mcitie;
use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Mchatkaryawan;
use App\Models\Mdiskusi;
use App\Models\Mdiskusidetail;
use App\Models\Mkontakmember;
use App\Models\Mcuti;

use Carbon\Carbon;
use DB;
use Auth;


class CutiController extends Controller
{
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->mkaryawan = new Mkaryawan;
        $this->mcuti = new Mcuti;
    }

    public function storecutikaryawan(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mcuti->storecutikaryawan($request);
        
        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di input'
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function showcutikaryawan(Request $request){
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mcuti->showcutikaryawan($request);
        
        if(!empty($data->m_karyawan_idkaryawan)) {

            $bulan =[
                '1' =>'Januari',
                '2' =>'Februari',
                '3' =>'Maret',
                '4' =>'April',
                '5' =>'Mei',
                '6' =>'Juni',
                '7' =>'Juli',
                '8' =>'Agustus',
                '9' =>'September',
                '10' =>'Oktober',
                '11' =>'November',
                '12' =>'Desember'
            ];

            $explode1 = explode('-',$data->t_cutikaryawan_tanggalawal);
            $showtglawal = $explode1[2].' '.$bulan[(int)$explode1[1]].' '.$explode1[0];

            $explode2 = explode('-',$data->t_cutikaryawan_tanggalakhir);
            $showtglakhir = $explode2[2].' '.$bulan[(int)$explode2[1]].' '.$explode2[0];

            $dtarray = [
                'm_karyawan_idkaryawan'         => $data->m_karyawan_idkaryawan,
                'm_karyawan_nama'               => $data->m_karyawan_nama,
                "m_title_name"                  => $data->m_title_name,
                't_cutikaryawan_status'         => $data->t_cutikaryawan_status,
                't_cutikaryawan_tanggalawal'    => $showtglawal,
                't_cutikaryawan_tanggalakhir'   => $showtglakhir
            ];

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $dtarray
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

}
