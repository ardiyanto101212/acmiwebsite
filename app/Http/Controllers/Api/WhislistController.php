<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Http\Controllers\Shared\SharedController;
use App\Http\Controllers\Shared\NotifikasiController;

use Illuminate\Support\Facades\Storage;

use App\Models\Mabsensi;
use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\Mcategoryproduct;
use App\Models\Mcitie;
use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Mchatkaryawan;
use App\Models\Mdiskusi;
use App\Models\Mdiskusidetail;
use App\Models\Mkontakmember;
use App\Models\Mbroadcast;
use App\Models\Mchatcustomer;
use App\Models\Mcustomerakun;
use App\Models\Mwhislist;

use Carbon\Carbon;
use DB;
use Auth;
use URL;

class WhislistController extends Controller
{
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->mwhislist = new Mwhislist;
    }

    public function storewhislistproductlove(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mwhislist->cekwhislistproductlove($request);
        if(!empty($data->t_whislistproduk_id)){
            $this->mwhislist->deletewhislistproductlove($request,$data);
        }else{
            $this->mwhislist->createwhislistproductlove($request);
        }

        return response()->json([
            "response" => [
                "status"    => 200,
                "message"   => "Success"
            ],
            "data" => 'Berhasil Di Input'
        ], 200);
    }

    public function storewhislistproductbintang(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mwhislist->qstorewhislistproductbintang($request);
        
        if($data){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Berhasil Di Input'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => 'Gagal Di Input'
            ], 201);
        }
        
    }

    public function whislistvavorit(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['m_customerakun_id'] ?? null;
        
        $data = $this->mwhislist->whislistvavorit($id);
        
        if($data->count() > 0){

            $arraydata = [];

            foreach($data as $list){

                $img = URL::to('/storage/'.$list->m_productlist_image);

                $arraypush = [
                    'm_productlist_id'          => $list->m_productlist_id,
                    'm_productlist_name'        => $list->m_productlist_name,
                    'm_productlist_mbps'        => $list->m_productlist_mbps,
                    'm_productlist_price'       => $list->m_productlist_price,
                    'm_productlist_perangkat'   => $list->m_productlist_perangkat,
                    'm_productlist_spec'        => $list->m_productlist_spec,
                    'm_productlist_image'       => $img,
                    'categoryname'              => $list->categoryname,
                    'm_productlist_description' => $list->m_productlist_description,
                    'love'                      => $list->t_whislistproduk_nilai ?? 0
                ];

                array_push($arraydata,$arraypush);

            }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }
        
    }
    

    
}
