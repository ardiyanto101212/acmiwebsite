<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Http\Controllers\Shared\SharedController;
use App\Http\Controllers\Shared\CarbonperiodeController;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;

use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Mcustomer;

use App\Models\Mcitie;
use App\Models\District;
use App\Models\Village;
use App\Models\Province;

use App\Models\Morderteknisi;
use App\Models\Mordersales;
use App\Models\Midnumber;
use App\Models\Mnotifikasi;
use App\Models\Mnotifikasibroadcast;

use Carbon\Carbon;
use DB;
use Auth;


class NotifikasiController extends Controller
{   
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->mkaryawan            = new Mkaryawan;
        $this->mcustomer            = new Mcustomer;
        $this->mnotifikasi          = new Mnotifikasi;
        $this->mnotifikasibroadcast = new Mnotifikasibroadcast;
    }

    public function getnotifikasipengajuan()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['m_karyawan_idkaryawan'] ?? null;

        $data = $this->mnotifikasi->getpengajuan($id);

        if($data->count() > 0) {

            $arraydata = [];
            foreach($data as $list){

                $customer = $this->mcustomer->cekcustomerrequest($list->t_notifikasi_idpengirim);

                $array = [
                    't_notifikasi_id'       => $list->t_notifikasi_id,
                    'oleh'                  => $customer->m_customer_nama,
                    't_notifikasi_judul'    => $list->t_notifikasi_judul,
                    't_notifikasi_pesan'    => $list->t_notifikasi_pesan,
                    'tanggal'               => date('d/m/Y',strtotime($list->updated_at)),
                ];

                array_push($arraydata,$array);
            }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function updatepengajuanterbaca(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mnotifikasi->updatepengajuanterbaca($request);
        
        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => "Berhasil Update"
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function getnotifikasipesan(){
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['m_karyawan_idkaryawan'] ?? null;

        $data = $this->mnotifikasi->getnotifikasipesan($id);

        $qcekdata = $this->mnotifikasibroadcast->ceknotifbroadcast($data);
        
        if(empty($qcekdata->t_broadcash_id)) {

            $qkaryawan = DB::table('m_karyawan')->where('m_karyawan_idkaryawan',$data->m_karyawan_idkaryawan)->first();

            $dtarray = [
                't_notifikasi_id'       => $data->t_chatkaryawan_id,
                'oleh'                  => $qkaryawan->m_karyawan_nama,
                't_notifikasi_judul'    => 'Pesan',
                't_notifikasi_pesan'    => $data->t_chatkaryawan_message,
                'tanggal'               => date('d/m/Y',strtotime($data->created_at))
            ];

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $dtarray
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }

    public function updatepesanterbaca(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mnotifikasibroadcast->updatepesanterbaca($request,'Pesan');
        
        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => "Berhasil Update"
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
    public function getnotifikasibroadcast(){
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['m_karyawan_idkaryawan'] ?? null;

        $data = $this->mnotifikasi->getnotifikasibroadcast($id);

        $qcekdata = $this->mnotifikasibroadcast->ceknotifbroadcastdireksi($data,$id);
        
        if(empty($qcekdata->t_broadcash_id)) {

            $dtarray = [
                't_notifikasi_id'       => $data->t_broadcash_id,
                'oleh'                  => $data->t_broadcash_namapengirim,
                't_notifikasi_judul'    => 'Broadcast',
                't_notifikasi_pesan'    => $data->t_broadcash_message,
                'tanggal'               => date('d/m/Y',strtotime($data->created_at))
            ];

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $dtarray
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 200);

        }
    }
    
    public function updatenotifikasibroadcastterbaca(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mnotifikasibroadcast->updatepesanterbaca($request,'Broadcast');
        
        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => "Berhasil Update"
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
    
}
