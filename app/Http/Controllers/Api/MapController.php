<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Http\Controllers\Shared\SharedController;
use App\Http\Controllers\Shared\NotifikasiController;

use Illuminate\Support\Facades\Storage;

use App\Models\Mabsensi;
use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\Mcategoryproduct;

use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Mchatkaryawan;
use App\Models\Mdiskusi;
use App\Models\Mdiskusidetail;
use App\Models\Mkontakmember;

use App\Models\Mcitie;
use App\Models\District;
use App\Models\Village;
use App\Models\Province;

use Carbon\Carbon;
use DB;
use Auth;

class MapController extends Controller
{
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->mkaryawan = new Mkaryawan;
        $this->mcatkaryawan = new Mchatkaryawan;
        $this->mdiskusi = new Mdiskusi;
        $this->mdiskusidetail = new Mdiskusidetail;
        $this->mkontakmember = new Mkontakmember;
        $this->muser = new User;
        $this->m = new User;
        $this->mcitie = new Mcitie;
    }

    public function getallwitel(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mcitie->getallwitel($request);
        
        if($data->count() > 0) {

            $arraydata = [];
            foreach($data as $list){
                $dtpush = [
                    'city' => $list->citiname,
                    'cityandprovince' => $list->citiname.'-'.$list->provincename
                ];

                array_push($arraydata,$dtpush);
            }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function searchkaryawanbywitelname(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mkaryawan->searchkaryawanbywitelname($request);
       
        if($data->count() > 0) {

            // $arraydata = [];
            // foreach($data as $list){
            //     $dtpush = [
            //         'city' => $list->citiname,
            //         'cityandprovince' => $list->citiname.'-'.$list->provincename
            //     ];

            //     array_push($arraydata,$dtpush);
            // }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
}
