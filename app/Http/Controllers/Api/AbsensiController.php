<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Shared\SharedController;
use Illuminate\Support\Facades\Storage;

use App\Models\Mabsensi;
use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\Mcategoryproduct;
use App\Models\Mcitie;
use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;

use Carbon\Carbon;
use DB;
use Auth;

class AbsensiController extends Controller
{   
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->mabsensi = new Mabsensi;
        $this->mkaryawan = new Mkaryawan;
    }

    public function storeabsensi(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        if($datapost['status'] == 'Check In'){
            $this->mabsensi->saveabsensi($datapost);
            
        }else{
            $datenow = date('Y-m-d');
            
            $qcekabsensi = $this->mabsensi->cekabsensi($datapost,$datenow);
            
            if(!empty($qcekabsensi->t_absensi_id)){
                $in     = $qcekabsensi->t_absensi_in;
                $out    = date('Y-m-d H:i:s');
    
                $gettotalabsen = new SharedController;
                $totaltime = $gettotalabsen->totaltimeabsen($in,$out);
                
                $this->mabsensi->updateabsensi($qcekabsensi,$totaltime);

            }
            
        }

        $data = [
            "absensidata"    => Mabsensi::get()
        ];

        if($data) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Data ditemukan"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Data tidak ditemukan"
                ],
                "data" => null
            ], 201);

        }
    }

    public function getabsensibyid(){
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['iduser'];

        $datenow = date('Y-m-d');

        // $qcekkaryawan = $this->mkaryawan->cekkaryawanbynikforabsen($nik);
        $qcekabsensi  = $this->mabsensi->cekabsensigetid($id,$datenow);

        // $data = [
        //     "absensidata"    => $qcekabsensi
        // ];

        // $datanull = [
        //     "absensidata"    => ''
        // ];

        if($qcekabsensi->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $qcekabsensi
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function storeabsensidata(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();
        $datenow = date('Y-m-d');

        
        $qcekabsensi = $this->mabsensi->cekabsensi($datapost,$datenow);

        if($datapost['status'] == 'Jam Masuk'){
          
            $this->mabsensi->saveabsensi($datapost,$qcekabsensi);
        }else{
            
            if(!empty($qcekabsensi->t_absensi_id)){
                $in     = $qcekabsensi->t_absensi_in;
                $out    = date('Y-m-d H:i:s');
    
                $gettotalabsen = new SharedController;
                $totaltime = $gettotalabsen->totaltimeabsen($in,$out);
                
                $this->mabsensi->updateabsensi($qcekabsensi,$totaltime,$request);
            }
            
        }

        // $data = [
        //     "absensidata"    => Mabsensi::get()
        // ];

        // $datanull = [
        //     "absensidata"    => ''
        // ];

        // if($qcekabsensi) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di masukan'
            ], 200);

        // } else {

        //     return response()->json([
        //         "response" => [
        //             "status"    => 200,
        //             "message"   => "Failed"
        //         ],
        //         "data" => null
        //     ], 200);

        // }
    }
    
}
