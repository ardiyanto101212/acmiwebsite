<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Shared\SharedController;
use Illuminate\Support\Facades\Storage;

use App\Models\Mabsensi;
use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\Mcategoryproduct;
use App\Models\Mprovince;
use App\Models\Mcitie;
use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Morder;
use App\Models\Mcustomer;
use App\Models\Mconfigurasi;
use App\Models\Mcarts;
use App\Models\Mwhislist;

use App\Models\Mordercustomer;
use App\Models\Mordercustomerhistori;
use App\Models\Mordercustomerdetail;

use Carbon\Carbon;
use DB;
use Auth;

use URL;

class ProductController extends Controller
{
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->morder       = new Morder;
        $this->mcustomer    = new Mcustomer;
        $this->mconfigurasi = new Mconfigurasi;
        $this->mkaryawan    = new Mkaryawan;
        $this->mprovice     = new Mprovince;
        $this->mproduct     = new Mproductlist;
        $this->mcarts       = new Mcarts;

        $this->mordercustomer           = new Mordercustomer;
        $this->mordercustomerhistori    = new Mordercustomerhistori;
        $this->mordercustomerdetail     = new Mordercustomerdetail;
        $this->mwhislist     = new Mwhislist;

    }

    
    public function kecepataninet()
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mproduct->kecepataninet();

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    
    public function productbykecepataninet(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $data = $this->mproduct->productbymbps($datapost);

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function grupcategoryprod()
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mproduct->qgrupcategoryprod();

        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function showgrupcategoryprod(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $datapost = $request->all();

        $data = $this->mproduct->qshowgrupcategoryprod($datapost);

        if($data->count() > 0) {
            $arraydata = [];

            foreach($data as $list){
                $img = URL::to('/storage/'.$list->m_productlist_image);

                $arraypush = [
                    'm_productlist_name'        => $list->m_productlist_name,
                    'm_productlist_price'       => $list->m_productlist_price,
                    'm_productlist_perangkat'   => $list->m_productlist_perangkat,
                    'm_productlist_spec'        => $list->m_productlist_spec,
                    'm_productlist_image'       => $img
                ];

                array_push($arraydata,$arraypush);

            }
            

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function getprodforcustomer(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mproduct->qgetprodforcustomer($request);

        if($data->count() > 0) {
            $arraydata = [];

            foreach($data as $list){

                $img = URL::to('/storage/'.$list->m_productlist_image);

                $qtotallove = $this->mwhislist->ceklovecustomerprod($list->m_productlist_id,$request->m_customerakun_id);

                $arraypush = [
                    'm_productlist_id'          => $list->m_productlist_id,
                    'm_productlist_name'        => $list->m_productlist_name,
                    'm_productlist_mbps'        => $list->m_productlist_mbps,
                    'm_productlist_price'       => $list->m_productlist_price,
                    'm_productlist_perangkat'   => $list->m_productlist_perangkat,
                    'm_productlist_spec'        => $list->m_productlist_spec,
                    'm_productlist_image'       => $img,
                    'categoryname'              => $list->categoryname,
                    'm_productlist_description' => $list->m_productlist_description,
                    'love'                      => $qtotallove->t_whislistproduk_nilai ?? 0
                ];

                array_push($arraydata,$arraypush);

            }
            

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
    public function storekeranjangcustomer(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mcarts->qstorekeranjangcustomer($request);

        if($data) {
            
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => "Data berhasil di input"
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
    public function cekkeranjangcustomer(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mproduct->qcekkeranjangcustomer($request);

        if($data->count() > 0) {
            $arraydata = [];
            // $qtyxprice = 0;
            foreach($data as $list){

                $img = URL::to('/storage/'.$list->m_productlist_image);

                $arraypush = [
                    't_carts_id'                => $list->t_carts_id,
                    'm_productlist_id'          => $list->m_productlist_id,
                    'm_productlist_name'        => $list->m_productlist_name,
                    'm_productlist_mbps'        => $list->m_productlist_mbps,
                    'm_productlist_price'       => $list->m_productlist_price,
                    'm_productlist_perangkat'   => $list->m_productlist_perangkat,
                    'm_productlist_spec'        => $list->m_productlist_spec,
                    'm_productlist_image'       => $img,
                    'categoryname'              => $list->categoryname,
                    'm_productlist_description' => $list->m_productlist_description,
                    'm_productlist_stock'       => $list->m_productlist_stock-$list->t_carts_qty,
                    't_carts_qty'               => $list->t_carts_qty,
                    't_carts_pilih'             => $list->t_carts_pilih
                ];

                // $qtyxprice += $list->t_carts_qty*$list->t_carts_price;

                array_push($arraydata,$arraypush);

            }
            // $arraygabung = [
            //     'total_price' => 'Rp. '.number_format($qtyxprice),
            //     'listdata'    => $arraydata
            // ];
            // array_push($arraydata,$arraytotalprice);

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function cektotalpricekeranjangcustomer(Request $request){
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mcarts->qcektotalpricekeranjangcustomer($request);

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

    }

    public function updownqtykeranjangcustomer(Request $request){
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mcarts->qupdownqtykeranjangcustomer($request);

        if($data){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di update'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }

    }

    public function pilihprodukkeranjangcustomer(Request $request){
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mcarts->qpilihprodukkeranjangcustomer($request);

        if($data){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di update'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }

    }
    
    public function deleteprodukkeranjangcustomer(Request $request){
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mcarts->qdeleteprodukkeranjangcustomer($request);

        if($data){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di hapus'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }

    }

    public function checkoutprodukkeranjangcustomer(Request $request){
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mcarts->checkoutprodukkeranjangcustomer($request);

        if(!empty($data->m_customer_id)){
            $creatordercustomer = $this->mordercustomer->createorderdata($data);
            $idordercust = $creatordercustomer->t_ordercustomer_id;

            $this->mordercustomerhistori->createordersaleshistori($idordercust,$creatordercustomer->t_ordercustomer_status);
            $this->mordercustomerdetail->createorderdata($idordercust,$data);
            $this->mcarts->deletecartssuccessorder($request);

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di simpan'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
    public function cekorderprodukcustomerbystatus(Request $request){
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mordercustomer->cekorderprodukcustomerbystatus($request);

        if($data->count() > 0){
            $arraydata = [];
            // $qtyxprice = 0;
            foreach($data as $list){

                $img = URL::to('/storage/'.$list->m_productlist_image);

                $arraypush = [
                    't_ordercustomer_status'            => $list->t_ordercustomer_status,
                    't_ordercustomerdetail_id'          => $list->t_ordercustomerdetail_id,
                    't_ordercustomer_id'                => $list->t_ordercustomer_id,
                    't_ordercustomerdetail_qty'         => $list->t_ordercustomerdetail_qty,
                    't_ordercustomerdetail_price'       => $list->t_ordercustomerdetail_price,

                    'm_productlist_id'                  => $list->m_productlist_id,
                    'm_productlist_name'                => $list->m_productlist_name,
                    'm_productlist_mbps'                => $list->m_productlist_mbps,
                    'm_productlist_perangkat'           => $list->m_productlist_perangkat,
                    'm_productlist_spec'                => $list->m_productlist_spec,
                    'm_productlist_image'               => $img,
                    'm_productlist_description'         => $list->m_productlist_description,
                    'categoryname'                      => $list->categoryname
                ];

                // $qtyxprice += $list->t_carts_qty*$list->t_carts_price;

                array_push($arraydata,$arraypush);

            }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function getdeskripsiproduk(){
        $id = $_GET['m_productlist_id'] ?? null;

        $list = $this->mordercustomer->getdeskripsiproduk($id);

        if(!empty($list->m_productlist_id)){
            $img = URL::to('/storage/'.$list->m_productlist_image);

            $arraydata = [
                'm_productlist_id'                  => $list->m_productlist_id,
                'm_productlist_name'                => $list->m_productlist_name,
                'm_productlist_mbps'                => $list->m_productlist_mbps,
                'm_productlist_perangkat'           => $list->m_productlist_perangkat,
                'm_productlist_spec'                => $list->m_productlist_spec,
                'm_productlist_image'               => $img,
                'm_productlist_description'         => $list->m_productlist_description,
                'categoryname'                      => $list->categoryname
            ];

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
}
