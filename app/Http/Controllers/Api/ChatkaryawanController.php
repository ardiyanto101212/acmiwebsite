<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Shared\SharedController;
use App\Http\Controllers\Shared\NotifikasiController;

use Illuminate\Support\Facades\Storage;

use App\Models\Mabsensi;
use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\Mcategoryproduct;
use App\Models\Mcitie;
use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Mchatkaryawan;
use App\Models\Mdiskusi;
use App\Models\Mdiskusidetail;
use App\Models\Mkontakmember;
use App\Models\Mbroadcast;
use App\Models\Mnotifikasi;

use Carbon\Carbon;
use DB;
use Auth;

class ChatkaryawanController extends Controller
{
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->mkaryawan = new Mkaryawan;
        $this->mcatkaryawan = new Mchatkaryawan;
        $this->mdiskusi = new Mdiskusi;
        $this->mdiskusidetail = new Mdiskusidetail;
        $this->mkontakmember = new Mkontakmember;
        $this->muser = new User;
        $this->mbroadcast = new Mbroadcast;
        $this->mnotifikasi     = new Mnotifikasi;
    }

    public function storechatkaryawan(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
    
        $data = $this->mcatkaryawan->storechat($request);
        
        if($data){

            $qdttokendevice = $this->mkaryawan->gettokendevicekaryawan($request->m_karyawan_idkaryawanreceiver);
            if(!empty($qdttokendevice->master_users_devicekey)){
                $devicetoken = $qdttokendevice->master_users_devicekey;
                if($qdttokendevice->master_users_devicekeyonof == 'On'){
                    $message = [
                        "title" => "Pesan Baru", 
                        "body" => $request->t_chatkaryawan_message
                    ];
    
                    $shared = new NotifikasiController;
                    $shared->sendnotifchat($devicetoken,$message);
                }else{

                }
                
            }
            
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di input'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }

            
    }

    public function updatelatlongkaryawan(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $this->mkaryawan->updatelatlongkaryawan($request);
        // $this->muser->updatetokendevice($request);
        
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di update'
            ], 200);
    }

    public function getchatkaryawanlist(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mcatkaryawan->qgetchatkaryawanlist($request);
        
        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function getlatlongkaryawan()
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $id = $_GET['m_karyawan_idkaryawan'] ?? null;

        $data = $this->mkaryawan->getlonglatkaryawan($id);
        
        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function getallidsalesteknisi()
    {
        date_default_timezone_set('Asia/Jakarta');
        

        $data = $this->mkaryawan->qgetallidsalesteknisi();
        
        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
    public function getallidsalesteknisiforchat()
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $id = $_GET['m_karyawan_idkaryawan'] ?? null;

        $data = $this->mkaryawan->qgetallidsalesteknisiforchat($id);
        
        if($data->count() > 0) {

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $data
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function gethistorychatkaryawan()
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $id = $_GET['m_karyawan_idkaryawan'] ?? null;

        $data = $this->mkaryawan->qgethistorychatkaryawan($id);
        
        if($data->count() > 0) {

            $arraydt = [];
            foreach($data as $list){
                $idrecheiver = $list->m_karyawan_idkaryawan;
                $qmessage = DB::table('t_chatkaryawan')
                            ->where(
                                [
                                    ['m_karyawan_idkaryawan',$id],
                                    ['m_karyawan_idkaryawanreceiver',$idrecheiver]
                                ])
                            
                                ->orWhere(
                                    [
                                        ['m_karyawan_idkaryawan',$idrecheiver],
                                        ['m_karyawan_idkaryawanreceiver',$id]
                                    ])


                            // ->where('m_karyawan_idkaryawan',$id)
                            // ->where('m_karyawan_idkaryawanreceiver',$idrecheiver)
                            ->orderby('created_at','DESC')
                            ->select(['t_chatkaryawan_message'])
                            ->first();

                $arraypush = [
                    't_chatkaryawan_message'    => $qmessage->t_chatkaryawan_message ?? '',
                    'm_karyawan_idkaryawan'     => $idrecheiver,
                    'm_karyawan_nama'           => $list->m_karyawan_nama,
                    'title_id'                  => $list->title_id,
                    "title_name"                => $list->title_name,
                    'm_karyawan_lattitude'      => $list->m_karyawan_lattitude,
                    'm_karyawan_longtitude'     => $list->m_karyawan_longtitude
                ];
    
                array_push($arraydt,$arraypush);
            }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydt
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    
    public function ubahemailkaryawan(Request $request)//
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = DB::table('m_karyawan')->where('m_karyawan_idkaryawan',$request->m_karyawan_idkaryawan)->first();
        
        if(!empty($data->m_karyawan_email)) {
            $shared = new SharedController;

            // $randompw = $shared->generateRandomStringpwuser();
            
            // $this->mcatkaryawan->qubahpwforgotpw($request,$randompw);

            DB::table('master_users')->where('email',$data->m_karyawan_email)->update(['email'  => strtolower($request->m_karyawan_email)]);
            DB::table('m_karyawan')->where('m_karyawan_idkaryawan',$request->m_karyawan_idkaryawan)->update(['m_karyawan_email'  => strtolower($request->m_karyawan_email)]);

            $subject    = "Notifikasi Rubah Email";
            $to         = strtolower($request->m_karyawan_email);
            $receiver   = ucwords($data->m_karyawan_nama);
            $content    = '
                    <p>Kepada Yth.
                        <br>
                        Email anda berhasil dirubah sebagai berikut :
                        <br>
                        Email : '.strtolower($request->m_karyawan_email).'
                    </p>
                    <br>
                    Silahkan Jaga Kerahasiaan Akun Anda !
                    ';

            
            $shared->sendemail($subject,$to,$receiver,$content);

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Berhasil Dirubah'
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function ubahusernamekaryawan(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mcatkaryawan->qubahusernamekaryawan($request);
        
        if($data) {

            $qkaryawan = $this->mcatkaryawan->qallkaryawandetail($request->m_karyawan_idkaryawan);
            $emailkaryawan = $qkaryawan->m_karyawan_email;

            $subject    = "Notifikasi Ubah Username";
            $to         = $emailkaryawan;
            $receiver   = ucwords($qkaryawan->m_karyawan_nama);
            $content    = '
                    <p>Kepada Yth.
                        <br>
                        Username anda berhasil dirubah sebagai berikut :
                        <br>
                        Username : '.$request->m_karyawan_username.'
                    </p>
                    <br>
                    Silahkan Jaga Kerahasiaan Akun Anda !
                    ';

            $shared = new SharedController;
            $shared->sendemail($subject,$to,$receiver,$content);

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Berhasil Dirubah'
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function ubahpasswordkaryawan(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mcatkaryawan->qubahpasswordkaryawan($request);
        
        if($data) {

            $qkaryawan = $this->mcatkaryawan->qallkaryawandetail($request->m_karyawan_idkaryawan);
            $emailkaryawan = $qkaryawan->m_karyawan_email;

            $subject    = "Notifikasi Ubah Password";
            $to         = $emailkaryawan;
            $receiver   = ucwords($qkaryawan->m_karyawan_nama);
            $content    = '
                    <p>Kepada Yth.
                        <br>
                        Password anda berhasil dirubah sebagai berikut :
                        <br>
                        Password : '.$request->m_karyawan_password.'
                    </p>
                    <br>
                    Silahkan Jaga Kerahasiaan Akun Anda !
                    ';

            $shared = new SharedController;
            $shared->sendemail($subject,$to,$receiver,$content);

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Berhasil Dirubah'
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }


    public function forgotpassword(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $data = $this->mkaryawan->qcekemailkaryawan(strtolower($request->email));
        
        $shared = new SharedController;

        if(!empty($data->m_karyawan_idkaryawan)) {
            $randompw = $shared->generateRandomStringpwuser();
            $this->mcatkaryawan->qubahpwforgotpw($request,$randompw);
            $emailkaryawan = $data->m_karyawan_email;

            $subject    = "Notifikasi Ubah Password";
            $to         = $emailkaryawan;
            $receiver   = ucwords($data->m_karyawan_nama);
            $content    = '
                    <p>Kepada Yth.
                        <br>
                        Password anda berhasil dirubah sebagai berikut :
                        <br>
                        Password : '.$randompw.'
                    </p>
                    <br>
                    Silahkan Jaga Kerahasiaan Akun Anda !
                    ';

            $shared = new SharedController;
            $shared->sendemail($subject,$to,$receiver,$content);

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Berhasil Dirubah'
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
    public function getdiskusilokalkaryawan(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mcatkaryawan->qgetdiskusilokalkaryawan($request);
        
        if($data->count() > 0) {

            $arraydata = [];

            foreach($data as $list){
                $totaljawaban = $this->mcatkaryawan->countjawabandiskusi($list->t_diskusi_id);
                $uraiannya = strlen($list->t_diskusi_uraian);

                if($uraiannya >= 115){
                    $uraianpush = substr($list->t_diskusi_uraian,0,115).'....';
                }else{
                    $uraianpush = $list->t_diskusi_uraian;
                }

                $arraypush = [
                    't_diskusi_id'      => $list->t_diskusi_id,
                    't_diskusi_judul'   => $list->t_diskusi_judul,
                    'oleh'              => 'Oleh '.$list->m_karyawan_nama.', '.date('d F Y H:i',strtotime($list->tanggalcreatediskusi)).' - '.$list->witelname,
                    'uraian'            => $uraianpush,
                    'totaljawaban'      => $totaljawaban.' Jawaban'
                ];

                array_push($arraydata,$arraypush);
            }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }


    public function storediskusikaryawan(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
    
        $data = $this->mdiskusi->qstorediskusikaryawan($request);
        
        if($data){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di input'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }
            
    }

    public function gethalamandiskusikaryawan()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id = $_GET['t_diskusi_id'];

        $data = $this->mdiskusi->qgethalamandiskusikaryawan($id);
        
        if(!empty($data->t_diskusi_id)) {

                $totaljawaban = $this->mcatkaryawan->countjawabandiskusi($data->t_diskusi_id);
                $arraydata = [
                    't_diskusi_id'      => $data->t_diskusi_id,
                    't_diskusi_judul'   => $data->t_diskusi_judul,
                    'oleh'              => 'Oleh '.$data->m_karyawan_nama.', '.date('d F Y H:i',strtotime($data->tanggalcreatediskusi)).' - '.$data->witelname,
                    'uraian'            => $data->t_diskusi_uraian,
                    'totaljawaban'      => $totaljawaban.' Jawaban'
                ];

                $qdatadetail = $this->mdiskusidetail->qgetdetaildiskusi($data->t_diskusi_id);

                if($qdatadetail->count() > 0) {
                    $arraydatadetail = [];

                    foreach($qdatadetail as $list){
                        $arraypush = [
                            'm_karyawan_nama'   => $list->m_karyawan_nama,
                            'waktu'             => date('d F Y H:i',strtotime($list->tanggalcreatedetail)).' - '.$list->witelname,
                            'jawaban'           => $list->t_diskusidetail_jawaban
                        ];
        
                        array_push($arraydatadetail,$arraypush);
                    }
                }else{
                    $arraydatadetail = [];
                }

                $arraydata['datadetail'] = $arraydatadetail;

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
                // "datadetail"    => 
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
    public function storejawabankaryawan(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
    
        $data = $this->mdiskusidetail->qstorejawabankaryawan($request);
        
        if($data){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di input'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }
            
    }


    public function carikontakmember(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
    
        $data = $this->mkontakmember->qcarikontakmember($request);
        
        if($data->count() > 0) {
            
            $arraydata = [];
      
            foreach($data as $list){
                if($request->m_karyawan_idkaryawan == $list->m_karyawan_idkaryawan){

                }else{
                    $qcekkontak = $this->mkontakmember->qcekkontakifsame($request->m_karyawan_idkaryawan,$list->m_karyawan_idkaryawan);
                    if(empty($qcekkontak->m_karyawan_idkaryawankontak)){
                        $arraypush = [
                            'm_karyawan_idkaryawankontak'  => $list->m_karyawan_idkaryawan,
                            'm_karyawan_nama'              => $list->m_karyawan_nama,
                            'm_karyawan_nik'               => 'ID '.$list->m_karyawan_nik,
                            'witel'                        => $list->cityname.', '.$list->districtname
                        ];
        
                        array_push($arraydata,$arraypush);
                    }
                    
                }
                
            }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
    public function savekontakmember(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
    
        $data = $this->mkontakmember->qsavekontakmember($request);
        
        if($data){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di input'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }
            
    }
    
    public function daftarkontakmember()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id = $_GET['m_karyawan_idkaryawan'] ?? null;

        $data = $this->mkontakmember->qdaftarkontakmember($id);
        
        if($data->count() > 0) {
            
            $arraydata = [];
      
            foreach($data as $list){
                
                        $arraypush = [
                            'm_karyawan_idkaryawankontak'  => $list->m_karyawan_idkaryawan,
                            'm_karyawan_nama'              => $list->m_karyawan_nama,
                            'm_karyawan_nik'               => 'ID '.$list->m_karyawan_nik,
                            'witel'                        => $list->cityname.', '.$list->districtname,
                            'namajabatan'                  => $list->m_title_name
                        ];
        
                        array_push($arraydata,$arraypush);
                    
            }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function cekkaryawanbyid()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id = $_GET['m_karyawan_idkaryawan'] ?? null;

        $data = $this->mkaryawan->qallkaryawandetail($id);
        
        if(!empty($data->m_karyawan_idkaryawan)) {
          
                        $arraydata = [
                            'm_karyawan_idkaryawan'        => $data->m_karyawan_idkaryawan,
                            'm_karyawan_nama'              => $data->m_karyawan_nama,
                            'namajabatan'                  => $data->m_title_name
                        ];
            
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function cekkaryawanbyname(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $data = $this->mkaryawan->qcekkaryawanbyname($request);
        
        if(!empty($data->m_karyawan_idkaryawan)) {
          
                        $arraydata = [
                            'm_karyawan_idkaryawan'        => $data->m_karyawan_idkaryawan,
                            'm_karyawan_nama'              => $data->m_karyawan_nama,
                            'namajabatan'                  => $data->m_title_name
                        ];
            
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    

    public function getbroadcast(){
        date_default_timezone_set('Asia/Jakarta');
    
        $data = $this->mbroadcast->getbroadcastdesc();
        // dd($data);
        if(!empty($data->t_broadcash_id)) {
            
            $bulan =[
                '1' =>'Januari',
                '2' =>'Februari',
                '3' =>'Maret',
                '4' =>'April',
                '5' =>'Mei',
                '6' =>'Juni',
                '7' =>'Juli',
                '8' =>'Agustus',
                '9' =>'September',
                '10' =>'Oktober',
                '11' =>'November',
                '12' =>'Desember'
            ];

            $tgl = date('Y-m-d',strtotime($data->created_at));

            $explode1 = explode('-',$tgl);
            $showtglawal = $explode1[2].' '.$bulan[(int)$explode1[1]].' '.$explode1[0].' '.date('H:i:s',strtotime($data->created_at));

                        $arraydata = [
                            't_broadcash_title'             => $data->t_broadcash_title,
                            't_broadcash_message'           => $data->t_broadcash_message,
                            't_broadcash_namapengirim'      => $data->t_broadcash_namapengirim,
                            'created_at'                    => $showtglawal
                        ];
            
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
}
