<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Shared\SharedController;
use App\Http\Controllers\Shared\CarbonperiodeController;
use App\Http\Controllers\Shared\NotifikasiController;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;

use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Mcustomer;

use App\Models\Mcitie;
use App\Models\District;
use App\Models\Village;
use App\Models\Province;
use App\Models\Mkasbonrequest;

use App\Models\Morderteknisi;

use Carbon\Carbon;
use DB;
use Auth;
use URL;

class OrderteknisiController extends Controller
{
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->morderteknisi   = new Morderteknisi;
        $this->mcustomer       = new Mcustomer;
        $this->mkaryawan       = new Mkaryawan;
        $this->mkabonrequest   = new Mkasbonrequest;
        $this->mvillage         = new Village;
    }

    // 200 OK
    // 202 Accepted
    // 200 No Content
    // 400 Bad Request
    // 200 Not Found

    public function storeorderteknisi(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        // $datapost = $request->all();

        $cekdata = $this->morderteknisi->qceknosc($request);

        if(!empty($cekdata->t_order_nosc)){
            $returndata = 'Data gagal di input, karena No. Sc sudah ada';

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }else{
            $this->morderteknisi->saveorderteknisi($request);
            $this->mcustomer->saveorderteknisi($request);

            $returndata = 'Data berhasil di input';

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $returndata
            ], 200);
        }

        
    }

    public function updatestoreteknisi(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        // $datapost = $request->all();

        $cekdata = $this->morderteknisi->qceknosc($request);

        if(!empty($cekdata->t_order_nosc)){

            $imagedata = $request->file('t_orderteknisi_badigital');
            if($imagedata != ''){
                $imagedata->storeAs('public/badigital/', $imagedata->hashName());
                $filename      = 'badigital/'.$imagedata->hashName();
            }else{
                $filename = null;
            }

            $this->morderteknisi->uploaddatadigital($request,$cekdata,$filename);

            $returndata = 'Data berhasil di update';
            $resstat = '1';
        }else{
            $returndata = 'Data gagal di update, karena No. Sc tidak terdaftar';
            $resstat = '2';
        }

        if($resstat == '1'){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $returndata
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => $returndata
            ], 201);
        }

        
    }

    public function updatebadigitalteknisi(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        // $datapost = $request->all();

        $cekdata = $this->morderteknisi->qceknosc($request);

        if(!empty($cekdata->t_order_nosc)){

            $imagedata = $request->file('t_orderteknisi_badigital');
            if($imagedata != ''){

                if(!empty($cekdata->t_orderteknisi_badigital)){
                    $photo = 'public/'.$cekdata->t_orderteknisi_badigital;
                    Storage::disk('local')->delete($photo);
                }

                $imagedata->storeAs('public/badigital/', $imagedata->hashName());
                $filename      = 'badigital/'.$imagedata->hashName();
            }else{
                $filename = null;
            }

            $this->morderteknisi->updateuploaddatadigital($request,$cekdata->t_orderteknisi_id,$filename);

            $returndata = 'Data berhasil di update';
            $resstat = '1';
        }else{
            $returndata = 'Data gagal di update, karena No. Sc tidak terdaftar';
            $resstat = '2';
        }

        if($resstat == '1'){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $returndata
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => $returndata
            ], 201);
        }

        
    }

    

    public function laporanteknisiperiode(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $plusminus = $request->nextorprev;

        $shared = new CarbonperiodeController;
        $dtperiode = $shared->logikamingguan($plusminus);

        // $ref_date=strtotime( "$datenya" );
        // $showweeh = date( 'W', $ref_date );
        
        //---------------

        // dd($dtperiode);
        $tglawal  = $dtperiode['awal'];
        $tglakhir = $dtperiode['akhir'];

        $data = $this->morderteknisi->qlaporanteknisiperiode($request,$tglawal,$tglakhir);
        
        if($data->count() > 0) {
            $totalpasang = $this->morderteknisi->qcountlaporanteknisiperiode($request,$tglawal,$tglakhir);
            // $totalfee = $this->mordersales->totalfeesales($request);
            $arraydata = [];
            $feeplus = 0;
            foreach($data as $list){
       
                if(!empty($list->t_orderteknisi_stb2)){
                    $satu = 1; 
                    $feesatu = $list->m_feeaddons_stb2;
                }else{
                    $satu = 0; 
                    $feesatu = 0;
                }

                if(!empty($list->t_orderteknisi_wifiextender)){
                    $dua = 1; 
                    $feedua = $list->m_feeaddons_wifiextender;
                }else{
                    $dua = 0; 
                    $feedua = 0;
                }

                if(!empty($list->t_orderteknisi_plc)){
                    $tiga = 1; 
                    $feetiga = $list->m_feeaddons_plc;
                }else{
                    $tiga = 0; 
                    $feetiga = 0;
                }

                if(!empty($list->t_orderteknisi_smartkamera)){
                    $empat = 1; 
                    $feeempat = $list->m_feeaddons_smartcamera;
                }else{
                    $empat = 0; 
                    $feeempat = 0;
                }

                $totallayanan = $satu+$dua+$tiga+$empat;
                $totalfeerows = $feesatu+$feedua+$feetiga+$feeempat;

                $arraypush = [
                    'nosc'              => $list->t_order_nosc,
                    'tanggalpasang'     => date('d-m-Y',strtotime($list->t_order_tanggalpasang)),
                    'layanan'           => $list->t_order_layanan.' + '.$totallayanan.' Add On',
                    'fee'               => 'Rp. '.number_format($totalfeerows)
                ];

                array_push($arraydata,$arraypush);

                $feeplus += $totalfeerows;
            }

            $qcekkasbon = DB::table('t_karyawankasbon')
                        ->where('m_karyawan_idkaryawan',$request->m_karyawan_idkaryawan)
                        ->orderby('updated_at','DESC')
                        ->first();

                        $bulan =[
                            '1' =>'Januari',
                            '2' =>'Februari',
                            '3' =>'Maret',
                            '4' =>'April',
                            '5' =>'Mei',
                            '6' =>'Juni',
                            '7' =>'Juli',
                            '8' =>'Agustus',
                            '9' =>'September',
                            '10' =>'Oktober',
                            '11' =>'November',
                            '12' =>'Desember'
                        ];
            
                        $explode1 = explode('-',$tglawal);
                        $showtglawal = $explode1[2].' '.$bulan[(int)$explode1[1]].' '.$explode1[0];
            
                        $explode2 = explode('-',$tglakhir);
                        $showtglakhir = $explode2[2].' '.$bulan[(int)$explode2[1]].' '.$explode2[0];

            $datalist = [
                'totalpekerjaan' => $totalpasang.' INSTALASI',
                'totalfee'       => 'Rp. '.number_format($feeplus),
                'kasbon'         => 'Rp. '.number_format($qcekkasbon->t_karyawankasbon_balance),
                'tanggalawal'    => $showtglawal,
                'tanggalakhir'   => $showtglakhir,
                'datalist'       => $arraydata
            ];

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $datalist
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function orderpekerjaanteknisi(){
        $id = $_GET['m_karyawan_idkaryawan'];

        $datalist = $this->morderteknisi->qdataorderteknisi($id);

        if($datalist->count() > 0) {

            // $select = [
            //     't_orderteknisi.t_order_nosc',
            //     'm_customer.m_customer_nama',
            //     't_orderteknisi.t_order_layanan',
            //     DB::raw('DATE_FORMAT(t_orderteknisi.t_order_tanggalpasang, "%d%-%m%-%Y") as t_order_tanggalpasang'),
            //     // 't_orderteknisi.t_order_tanggalpasang',
            //     't_orderteknisi.t_orderteknisi_status'
            // ];

            $arraydt = [];
            foreach($datalist as $list){

                $qcekwitel = $this->mvillage->qgetareavillage($list->m_customer_witel);
                $wilayah = $list->m_customer_namajalan.', Kel.'.$qcekwitel->villagename.', Kec.'.$qcekwitel->districtname.', Kota.'.$qcekwitel->citiname.' - '.$qcekwitel->provincename.'. '.$list->m_customer_kodepos;
 
                $arraypush = [
                    't_order_nosc'           => $list->t_order_nosc,
                    'm_customer_nama'        => $list->m_customer_nama,
                    't_order_layanan'        => $list->t_order_layanan,
                    't_order_tanggalpasang'  => $list->t_order_tanggalpasang,
                    't_orderteknisi_status'  => $list->t_orderteknisi_status,
                    'm_customer_alamat'      => $wilayah,
                ];

                array_push($arraydt,$arraypush);
            }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydt
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }

        
    }

    public function lihatbadigital(){
        $id = $_GET['t_order_nosc'];

        $datalist = $this->morderteknisi->qceknoscget($id);

        // dd(URL::to('/storage'));

        if(!empty($datalist->t_orderteknisi_badigital)){
            $img = URL::to('/storage/'.$datalist->t_orderteknisi_badigital);

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $img
            ], 200);

        }else{
            return response()->json([
            "response" => [
                "status"    => 201,
                "message"   => "Failed"
            ],
            "data" =>  null
        ], 201);
        }

        
    }

    public function ceknoscteknisi(Request $request){
        $nosc = $request->t_order_nosc;
        $idkaryawan = $request->m_karyawan_idkaraywan;

        $datalist = $this->morderteknisi->qceknoscget($nosc);

        if(empty($datalist->t_order_nosc)){
        
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data Tidak Ditemukan'
            ], 200);

        }else{
            return response()->json([
            "response" => [
                "status"    => 201,
                "message"   => "Failed"
            ],
            "data" =>  "Data Ditemukan"
        ], 201);
        }

        
    }
    
    public function orderdetailteknisi()
    {
        date_default_timezone_set('Asia/Jakarta');

        $id = $_GET['t_order_nosc'];

        $qcekidbynosc = $this->morderteknisi->qceknoscget($id);

        if(!empty($qcekidbynosc->t_orderteknisi_id)){
            $idteknisiorder = $qcekidbynosc->t_orderteknisi_id;

            $detailorder    = $this->morderteknisi->qdetailorder($idteknisiorder);
            $customerorder  = $this->morderteknisi->qcustomerteknisi($idteknisiorder);

            $teknisi1 = $this->mkaryawan->qallkaryawandetail($detailorder->m_karyawan_idkaryawan1);
            $teknisi2 = $this->mkaryawan->qallkaryawandetail($detailorder->m_karyawan_idkaryawan2);
            $teknisi3 = $this->mkaryawan->qallkaryawandetail($detailorder->m_karyawan_idkaryawan3);
            $teknisi4 = $this->mkaryawan->qallkaryawandetail($detailorder->m_karyawan_idkaryawan4);
            $teknisi5 = $this->mkaryawan->qallkaryawandetail($detailorder->m_karyawan_idkaryawan5);
            
            if(!empty($teknisi1->m_karyawan_nama)){
                $tknisi1 = $teknisi1->m_karyawan_nama;
            }else{
                $tknisi1 = '';
            }

            if(!empty($teknisi2->m_karyawan_nama)){
                $tknisi2 = ', '.$teknisi2->m_karyawan_nama;
            }else{
                $tknisi2 = '';
            }

            if(!empty($teknisi3->m_karyawan_nama)){
                $tknisi3 = ', '.$teknisi3->m_karyawan_nama;
            }else{
                $tknisi3 = '';
            }

            if(!empty($teknisi4->m_karyawan_nama)){
                $tknisi4 = ', '.$teknisi4->m_karyawan_nama;
            }else{
                $tknisi4 = '';
            }

            if(!empty($teknisi5->m_karyawan_nama)){
                $tknisi5 = ', '.$teknisi5->m_karyawan_nama;
            }else{
                $tknisi5 = '';
            }

            if(!empty($detailorder->t_orderteknisi_stb2)){
                $satu = 'STB 2';
            }else{
                $satu = '';
            }

            if(!empty($detailorder->t_orderteknisi_wifiextender)){
                $dua = ', Extenter';
            }else{
                $dua = '';
            }

            if(!empty($detailorder->t_orderteknisi_plc)){
                $tiga = ', PLC';
            }else{
                $tiga = '';
            }

            if(!empty($detailorder->t_orderteknisi_smartkamera)){
                $empat = ', Smart Camera';
            }else{
                $empat = '';
            }

            $gabungteknisi  = $tknisi1.$tknisi2.$tknisi3.$tknisi4.$tknisi5;
            $gabungaddon    = $satu.$dua.$tiga.$empat;

            $qcekwitel = $this->mvillage->qgetareavillage($customerorder->m_customer_witel);

            $wilayah = $customerorder->m_customer_namajalan.', Kel.'.$qcekwitel->villagename.', Kec.'.$qcekwitel->districtname.', Kota.'.$qcekwitel->citiname.' - '.$qcekwitel->provincename.'. '.$customerorder->m_customer_kodepos;
 
            $datalist = [
                'nosc'              => $detailorder->t_order_nosc,
                'tanggalpasang'     => date('d-m-Y',strtotime($detailorder->t_order_tanggalpasang)),
                'nointernet'        => $detailorder->t_order_nointernet,
                'novoice'           => $detailorder->t_order_novoice,
                'namacustomer'      => $customerorder->m_customer_nama,
                'alamatcustomer'    => $wilayah,
                'layanan'           => $detailorder->t_order_layanan,
                'addon'             => $gabungaddon,
                'teknisi'           => $gabungteknisi,
                // "detailorder"   => $detailorder,
                // 'customer'      => $customerorder,
    
                // 'nikkaryawan1'   =>  $teknisi1->m_karyawan_nik ?? '',
                // 'nama1'          =>  $teknisi1->m_karyawan_nama ?? '',
                // 'title1'         =>  $teknisi1->m_title_name ?? '',
                // 'district1'      =>  $teknisi1->districtname ?? '',
                // 'city1'          =>  $teknisi1->cityname ?? '',
                // 'province1'      =>  $teknisi1->provincename ?? '',
    
                // 'nikkaryawan2'   =>  $teknisi2->m_karyawan_nik ?? '',
                // 'nama2'          =>  $teknisi2->m_karyawan_nama ?? '',
                // 'title2'         =>  $teknisi2->m_title_name ?? '',
                // 'district2'      =>  $teknisi2->districtname ?? '',
                // 'city2'          =>  $teknisi2->cityname ?? '',
                // 'province2'      =>  $teknisi2->provincename ?? '',
    
                // 'nikkaryawan3'   =>  $teknisi3->m_karyawan_nik ?? '',
                // 'nama3'          =>  $teknisi3->m_karyawan_nama ?? '',
                // 'title3'         =>  $teknisi3->m_title_name ?? '',
                // 'district3'      =>  $teknisi3->districtname ?? '',
                // 'city3'          =>  $teknisi3->cityname ?? '',
                // 'province3'      =>  $teknisi3->provincename ?? '',
    
                // 'nikkaryawan4'   =>  $teknisi4->m_karyawan_nik ?? '',
                // 'nama4'          =>  $teknisi4->m_karyawan_nama ?? '',
                // 'title4'         =>  $teknisi4->m_title_name ?? '',
                // 'district4'      =>  $teknisi4->districtname ?? '',
                // 'city4'          =>  $teknisi4->cityname ?? '',
                // 'province4'      =>  $teknisi4->provincename ?? '',
    
                // 'nikkaryawan5'   =>  $teknisi5->m_karyawan_nik ?? '',
                // 'nama5'          =>  $teknisi5->m_karyawan_nama ?? '',
                // 'title5'         =>  $teknisi5->m_title_name ?? '',
                // 'district5'      =>  $teknisi5->districtname ?? '',
                // 'city5'          =>  $teknisi5->cityname ?? '',
                // 'province5'      =>  $teknisi5->provincename ?? ''
            ];


            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $datalist
            ], 200);

        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }

        
    }


    public function ambildataeditorderteknisi(){
        $id = $_GET['t_order_nosc'] ?? null;

        $datapost = $this->morderteknisi->qceknoscget($id);

        if(!empty($datapost->t_orderteknisi_id)){

            $customerorder  = $this->morderteknisi->qcustomerteknisi($datapost->t_orderteknisi_id);

            $arraydata = [
                't_order_nosc'                      => str_replace(' ','',$datapost->t_order_nosc),
                // 't_order_tanggalpasang'             => date("Y-m-d",strtotime($tanggal)),
                't_order_nointernet'                => $datapost->t_order_nointernet,
                't_order_novoice'                   => $datapost->t_order_novoice,
                't_order_layanan'                   => $datapost->t_order_layanan,
                't_order_snont'                     => $datapost->t_order_snont,
                't_orderteknisi_stb1'               => $datapost->t_orderteknisi_stb1,
                't_orderteknisi_stb2'               => $datapost->t_orderteknisi_stb2,
                't_orderteknisi_wifiextender'       => $datapost->t_orderteknisi_wifiextender,
                't_orderteknisi_plc'                => $datapost->t_orderteknisi_plc,
                't_orderteknisi_smartkamera'        => $datapost->t_orderteknisi_smartkamera,
                't_orderteknisi_kabel'              => $datapost->t_orderteknisi_kabel,
                't_orderteknisi_dcroll'             => $datapost->t_orderteknisi_dcroll,
                't_orderteknisi_soc'                => $datapost->t_orderteknisi_soc,
                't_orderteknisi_sclamp'             => $datapost->t_orderteknisi_sclamp,
                't_orderteknisi_bracket'            => $datapost->t_orderteknisi_bracket,
                't_orderteknisi_roset'              => $datapost->t_orderteknisi_roset,
                't_orderteknisi_utp'                => $datapost->t_orderteknisi_utp,
                't_orderteknisi_barcodeodp'         => $datapost->t_orderteknisi_barcodeodp,
                't_orderteknisi_odp'                => $datapost->t_orderteknisi_odp,
                't_orderteknisi_portodp'            => $datapost->t_orderteknisi_portodp,

                'm_karyawan_idkaryawan1'            => $datapost->m_karyawan_idkaryawan1,

                'm_customer_nik'                    => $customerorder->m_customer_nik,
                'm_customer_nama'                   => $customerorder->m_customer_nama,
                'm_customer_alamat'                 => $customerorder->m_customer_alamat,
            ];
            

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }
    }

    public function updateorderteknisi(Request $request){
        
        $datapost = $this->morderteknisi->qceknoscget($request->t_order_nosc);
        if(!empty($datapost->t_orderteknisi_id)){

            $idorder = $datapost->t_orderteknisi_id;
                
            $this->morderteknisi->saveupdateorderteknisi($request,$idorder);
            $this->mcustomer->saveupdateorderteknisi($request,$idorder);

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data Berhasil Di Update'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => 'NO. SC Tidak Tersedia'
            ], 201);
        }

    }

    public function cekkasbonteknisi(){
        $id = $_GET['m_karyawan_idkaryawan'] ?? null;

        $data = $this->mkabonrequest->cekkasbonteknisi($id);

        if(!empty($data->m_karyawan_idkaryawan)){

            $status = $data->t_karyawankasbonrequest_status;

            $dtarray = [
                'm_karyawan_idkaryawan'             => $id,
                'm_title_name'                      => $data->m_title_name,
                't_karyawankasbonrequest_status'    => $data->t_karyawankasbonrequest_status,
                't_karyawankasbon_request'          => 'Rp. '.number_format($data->t_karyawankasbon_request),
                't_karyawankasbon_acc'              => 'Rp. '.number_format($data->t_karyawankasbon_acc),
                'tanggalpermohonan'                 => date('d F Y H:i',strtotime($data->tanggalpermohonan)),
                'tanggalkonfirmasi'                 => date('d F Y H:i',strtotime($data->tanggalkonfirmasi)),

            ];

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $dtarray
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }

    }

    public function storekasbonteknisi(Request $request){
   
        $data = $this->mkabonrequest->storekasbonteknisi($request);

        if($data){

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Berhasil Di Input'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
        }

    }
    
}
