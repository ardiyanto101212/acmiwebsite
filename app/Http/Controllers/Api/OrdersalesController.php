<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Shared\SharedController;
use App\Http\Controllers\Shared\CarbonperiodeController;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;

use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Mcustomer;

use App\Models\Mcitie;
use App\Models\District;
use App\Models\Village;
use App\Models\Province;

use App\Models\Morderteknisi;
use App\Models\Mordersales;
use App\Models\Midnumber;
use App\Models\Mnotifikasi;

use Carbon\Carbon;
use DB;
use Auth;

class OrdersalesController extends Controller
{
    protected $menus;
    private $modelbuilder;

    function __construct() {
        // $this->middleware(function ($request, $next) {
        //     $this->menus = $this->permission('User');
        //     return $next($request);
        // });

        $this->mordersales     = new Mordersales;
        $this->mcustomer       = new Mcustomer;
        $this->midnumber       = new Midnumber;
        $this->mvillage        = new Village;
        $this->mnotifikasi     = new Mnotifikasi;
    }

    public function storeordersales(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        // $datapost = $request->all();
        $idnumber = str_replace(' ','',$request->t_ordersales_idnumber);
        $qcekordernoifsame = $this->mordersales->cekidnumberifsama($idnumber);

        if(!empty($qcekordernoifsame->t_ordersales_idnumber)){
            $returnnya = 'Tidak tersimpan, Order ID sudah ada';
            $responstatus = '2';
        }else{
            $this->mordersales->storeordersalesnew($request);
            $this->mcustomer->storeordersalesnew($request);

            $qcekid = DB::table('t_ordersales')
                    ->where('m_karyawan_idkaryawan',$request->m_karyawan_idkaryawan)
                    ->where('m_productlist_id',$request->m_productlist_id)
                    ->where('t_ordersales_idnumber',$request->t_ordersales_idnumber)
                    ->get()
                    ->first();

            //------------------------
            $sharedhistoryorder = new SharedController;
            $sharedhistoryorder->savehistoryorder($qcekid->t_ordersales_id,$request->m_karyawan_idkaryawan,"Permohonan");
            //------------------------

            $returnnya = 'Data berhasil di input';
            $responstatus = '1';
        }

        

        // $this->mordersales->storeordersales($request);

        // //--------- upload data 
        // $fotocustomer = $request->file('m_customer_foto');
        // if($fotocustomer != ''){
        //     $fotocustomer->storeAs('public/fotocustomer/', $fotocustomer->hashName());
        //     $fotocustomername = 'fotocustomer/'.$fotocustomer->hashName();
        // }else{
        //     $fotocustomername = null;
        // }

        // $fotocustomerktp = $request->file('m_customer_fotoktp');
        // if($fotocustomerktp != ''){
        //     $fotocustomerktp->storeAs('public/ktpcustomer/', $fotocustomerktp->hashName());
        //     $fotocustomernamektp = 'ktpcustomer/'.$fotocustomerktp->hashName();
        // }else{
        //     $fotocustomernamektp = null;
        // }

        // $fotocustomerrumah = $request->file('m_customer_fotorumah');
        // if($fotocustomerrumah != ''){
        //     $fotocustomerrumah->storeAs('public/rumahcustomer/', $fotocustomerrumah->hashName());
        //     $fotocustomernamerumah = 'rumahcustomer/'.$fotocustomerrumah->hashName();
        // }else{
        //     $fotocustomernamerumah = null;
        // }

        // //---------------------

        // $this->mcustomer->storeordersales($request,$fotocustomername,$fotocustomernamektp,$fotocustomernamerumah);

        // $returndata = 'Data berhasil di input';
       if($responstatus == '1'){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $returnnya
            ], 200);
       }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => $returnnya
            ], 201);
       }
        
    }

    public function storemyir(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $qcekid = DB::table('t_ordersales')
                ->where('m_karyawan_idkaryawan',$request->m_karyawan_idkaryawan)
                ->where('t_ordersales_idnumber',str_replace(' ','',$request->t_ordersales_idnumber))
                ->where('t_ordersales_nomyir',str_replace(' ','',$request->t_ordersales_nomyir))
                ->first();

        if(empty($qcekid->t_ordersales_nomyir)){
            $datasave = $this->mordersales->storemyir($request);
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data Berhasil Di Update'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => 'No. MYIR Sudah Ada'
            ], 201);
        }

        // $datasave = $this->mordersales->storemyir($request);

        // $returndata = 'Data berhasil di update';
       
        // if($datasave){
        //     return response()->json([
        //         "response" => [
        //             "status"    => 200,
        //             "message"   => "Success"
        //         ],
        //         "data" => $returndata
        //     ], 200);
        // }else{
        //     return response()->json([
        //         "response" => [
        //             "status"    => 201,
        //             "message"   => "Failed"
        //         ],
        //         "data" => null
        //     ], 201);
        // }

        
    }

    public function storenosc(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $qcekid = DB::table('t_ordersales')
                ->where('m_karyawan_idkaryawan',$request->m_karyawan_idkaryawan)
                ->where('t_ordersales_idnumber',str_replace(' ','',$request->t_ordersales_idnumber))
                ->where('t_ordersales_nomyir',str_replace(' ','',$request->t_ordersales_nomyir))
                ->where('t_ordersales_nosc',str_replace(' ','',$request->t_ordersales_nosc))
                ->first();

        if(empty($qcekid->t_ordersales_nosc)){
            $this->mordersales->storenosc($request);

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di update'
            ], 200);
        }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => 'No. SC Sudah Ada'
            ], 201);
        }
        
    }

    public function historiordersales()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id = $_GET['t_ordersales_idnumber'];

        $data = $this->mordersales->historiordersales($id);

        if($data->count() > 0) {

            $bulan =[
                '1' =>'Januari',
                '2' =>'Februari',
                '3' =>'Maret',
                '4' =>'April',
                '5' =>'Mei',
                '6' =>'Juni',
                '7' =>'Juli',
                '8' =>'Agustus',
                '9' =>'September',
                '10' =>'Oktober',
                '11' =>'November',
                '12' =>'Desember'
            ];

            $arraydata = [];
            foreach($data as $list){

                $datecreatedat      = date('Y-m-d',strtotime($list->created_at));
                $explodecreatedat   = explode('-',$datecreatedat);
                $created_at         = $explodecreatedat[2].' '.$bulan[(int)$explodecreatedat[1]].' '.$explodecreatedat[0];

                $arraypush = [
                    't_order_status'    => $list->t_order_status,
                    'created_at'        => $created_at,
                    'time'              => date('H:i:s',strtotime($list->created_at))
                ];

                array_push($arraydata,$arraypush);
            }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydata
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function datapemesanansales()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id = $_GET['t_ordersales_idnumber'];

        $data = $this->mordersales->qdatapemesanansales($id);

        if(!empty($data->t_ordersales_idnumber)) {

            $qcekwitel = $this->mvillage->qgetareavillage($data->m_customer_witel);

            $wilayah = $data->m_customer_namajalan.', Kel.'.$qcekwitel->villagename.', Kec.'.$qcekwitel->districtname.', Kota.'.$qcekwitel->citiname.' - '.$qcekwitel->provincename.'. '.$data->m_customer_kodepos;
 
            $dtarray = [
                't_ordersales_idnumber' => $data->t_ordersales_idnumber,
                't_ordersales_tanggal'  => $data->t_ordersales_tanggal,
                'm_productlist_name'    => $data->m_productlist_name,
                'm_productlist_price'   => $data->m_productlist_price,
                'm_customer_nik'        => $data->m_customer_nik,
                'm_customer_nama'       => $data->m_customer_nama,
                'm_customer_alamat'     => $wilayah,
                'm_customer_email'      => strtolower($data->m_customer_email),
                'm_customer_hp'         => $data->m_customer_hp,
            ];

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $dtarray
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function dealershipsales()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id = $_GET['m_karyawan_idkaryawan'];

        $data = $this->mordersales->qdealershipsales($id);

        if($data->count() > 0) {

            $bulan =[
                '1' =>'Januari',
                '2' =>'Februari',
                '3' =>'Maret',
                '4' =>'April',
                '5' =>'Mei',
                '6' =>'Juni',
                '7' =>'Juli',
                '8' =>'Agustus',
                '9' =>'September',
                '10' =>'Oktober',
                '11' =>'November',
                '12' =>'Desember'
            ];

            $dtarray = [];
            foreach($data as $list){
                $datecreatedat      = date('Y-m-d',strtotime($list->created_at));
                $explodecreatedat   = explode('-',$datecreatedat);
                $created_at         = date("H:i:s",strtotime($list->created_at)).' '.$explodecreatedat[2].'-'.$bulan[(int)$explodecreatedat[1]].'-'.$explodecreatedat[0];

                $tordersalestanggal = date('Y-m-d',strtotime($list->t_ordersales_tanggal));
                $expld2             = explode('-',$tordersalestanggal);
                $tglorder           = $expld2[2].'-'.$bulan[(int)$expld2[1]].'-'.$expld2[0];

                $qcekwitel = $this->mvillage->qgetareavillage($list->m_customer_witel);

                $wilayah = $list->m_customer_namajalan.', Kel.'.$qcekwitel->villagename.', Kec.'.$qcekwitel->districtname.', Kota.'.$qcekwitel->citiname.' - '.$qcekwitel->provincename.'. '.$list->m_customer_kodepos;

                $arraypush = [
                    'm_customer_nama'                   => $list->m_customer_nama,
                    'm_customer_nik'                    => $list->m_customer_nik,
                    'm_customer_alamat'                 => $wilayah,
                    'm_customer_email'                  => strtolower($list->m_customer_email),
                    'm_customer_hp'                     => $list->m_customer_hp,
                    'm_productlist_name'                => $list->m_productlist_name,
                    'created_at'                        => $created_at,
                    't_ordersales_tanggal'              => $tglorder,
                    't_ordersales_nomyir'               => $list->t_ordersales_nomyir,
                    't_ordersales_nosc'                 => $list->t_ordersales_nosc,
                    't_ordersales_status'               => $list->t_ordersales_status,
                    't_ordersales_idnumber'             => $list->t_ordersales_idnumber,
                    'm_karyawan_idkaryawan'             => $list->m_karyawan_idkaryawan,
                ];

                array_push($dtarray,$arraypush);
            }

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $dtarray
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function detaillaporansales()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id = $_GET['t_ordersales_idnumber'];

        $data = $this->mordersales->qdetaillaporansales($id);

        if(!empty($data->t_ordersales_idnumber)) {

            $bulan =[
                '1' =>'Januari',
                '2' =>'Februari',
                '3' =>'Maret',
                '4' =>'April',
                '5' =>'Mei',
                '6' =>'Juni',
                '7' =>'Juli',
                '8' =>'Agustus',
                '9' =>'September',
                '10' =>'Oktober',
                '11' =>'November',
                '12' =>'Desember'
            ];

            $tgl1           = date('d-m-Y',strtotime($data->tanggalorder));
            $expld1         = explode('-',$tgl1);
            $tanggalorder   = $expld1[2].'-'.$bulan[(int)$expld1[1]].'-'.$expld1[0];


            $tgl2           = date('d-m-Y',strtotime($data->tanggalselesai));
            $expld2         = explode('-',$tgl2);
            $tanggalselesai = $expld2[2].'-'.$bulan[(int)$expld2[1]].'-'.$expld2[0];

            $qcekwitel = $this->mvillage->qgetareavillage($data->m_customer_witel);

            $wilayah = $data->m_customer_namajalan.', Kel.'.$qcekwitel->villagename.', Kec.'.$qcekwitel->districtname.', Kota.'.$qcekwitel->citiname.' - '.$qcekwitel->provincename.'. '.$data->m_customer_kodepos;
 

            $arraydt = [
                't_ordersales_idnumber' => $data->t_ordersales_idnumber,
                'tanggalorder'          => $tanggalorder,
                'tanggalselesai'        => $tanggalselesai,
                't_ordersales_nomyir'   => $data->t_ordersales_nomyir,
                'm_productlist_name'    => $data->m_productlist_name,
                'm_productlist_price'   => 'Rp. '.number_format($data->m_productlist_price),
                't_ordersales_fee'      => 'Rp. '.number_format($data->t_ordersales_fee),
                'm_customer_nama'       => $data->m_customer_nama,
                'm_customer_alamat'     => $wilayah,
                'm_customer_email'      => strtolower($data->m_customer_email),
                'm_customer_hp'         => $data->m_customer_hp,
                't_ordersales_nosc'     => $data->t_ordersales_nosc
            ];

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $arraydt
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }
    
    public function laporansalesperiode(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');

        $plusminus = $request->nextorprev;

        $shared = new CarbonperiodeController;
        $dtperiode = $shared->logikamingguan($plusminus);

        // $ref_date=strtotime( "$datenya" );
        // $showweeh = date( 'W', $ref_date );
        
        //---------------

        // dd($dtperiode);
        $tglawal  = $dtperiode['awal'];
        $tglakhir = $dtperiode['akhir'];

        $data = $this->mordersales->qlaporansalesperiode($request,$tglawal,$tglakhir);
        
        if($data->count() > 0) {
            $totalpenjualan = $this->mordersales->counttotalpenjualansales($request,$tglawal,$tglakhir);
            $totalfee = $this->mordersales->totalfeesales($request,$tglawal,$tglakhir);

            // $arrayup = [];
            // foreach($data as $list){
            //     $arraylist = [
            //         't_ordersales_idnumber' => $list->t_ordersales_idnumber,
            //         't_ordersales_nomyir'   => $list->t_ordersales_nomyir,
            //         'tanggalorder'          => date('d-m-Y',strtotime($list->tanggalorder)),
            //         'tanggalselesai'        => date('d-m-Y',strtotime($list->tanggalselesai)),
            //         'm_productlist_name'    => $list->m_productlist_name,
            //         't_ordersales_fee'      => 'Rp. '.number_format($list->t_ordersales_fee)
            //     ];
            //     array_push($arrayup,$arraylist);
            // }
            
            $bulan =[
                '1' =>'Januari',
                '2' =>'Februari',
                '3' =>'Maret',
                '4' =>'April',
                '5' =>'Mei',
                '6' =>'Juni',
                '7' =>'Juli',
                '8' =>'Agustus',
                '9' =>'September',
                '10' =>'Oktober',
                '11' =>'November',
                '12' =>'Desember'
            ];

            $explode1 = explode('-',$tglawal);
            $showtglawal = $explode1[2].' '.$bulan[(int)$explode1[1]].' '.$explode1[0];

            $explode2 = explode('-',$tglakhir);
            $showtglakhir = $explode2[2].' '.$bulan[(int)$explode2[1]].' '.$explode2[0];

            $datalist = [
                'totalpenjualan' => $totalpenjualan.' SSL',
                'totalfee'       => 'Rp. '.number_format($totalfee->totalfee),
                'tanggalawal'    => $showtglawal,
                'tanggalakhir'   => $showtglakhir,
                'datalist'       => $data
            ];

            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $datalist
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function getidnumbersales()
    {
        date_default_timezone_set('Asia/Jakarta');
        $id = $_GET['m_karyawan_idkaryawan'];

        $data = $this->midnumber->storedata($id);

        if(!empty($data->m_idnumber_id)) {

            $value = $data->m_idnumber_id;
            $lengthid = strlen($value);

            if($lengthid == 1){
                $nilai = '000'.$value;
            }else if($lengthid == 2){
                $nilai = '00'.$value;
            }else if($lengthid == 3){
                $nilai = '0'.$value;
            }else{
                $nilai = $value;
            }

            $idnumber = [
                'orderid' => $nilai
            ];
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $idnumber
            ], 200);

        } else {

            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);

        }
    }

    public function pengajuanpemasangancustomer(Request $request){
        date_default_timezone_set('Asia/Jakarta');

        $idnumber = str_replace(' ','',$request->t_ordersales_idnumber);
        // $qcekordernoifsame = $this->mordersales->cekidnumberifsama($idnumber);

        // if(!empty($qcekordernoifsame->t_ordersales_idnumber)){
        //     $returnnya = 'Tidak tersimpan, Order ID sudah ada';
        //     $responstatus = '2';
        // }else{
            $saveordersales = $this->mordersales->storeordersalespengajuancustomernew($request);

            $imagedata = $request->file('m_customer_foto');
            if($imagedata != ''){
                $imagedata->storeAs('public/fotocustomer/', $imagedata->hashName());
                $foto      = 'fotocustomer/'.$imagedata->hashName();
            }else{
                $foto = null;
            }

            $imagedataktp = $request->file('m_customer_fotoktp');
            if($imagedataktp != ''){
                $imagedataktp->storeAs('public/ktpcustomer/', $imagedataktp->hashName());
                $ktp      = 'ktpcustomer/'.$imagedataktp->hashName();
            }else{
                $ktp = null;
            }

            $imagedatarumah = $request->file('m_customer_fotorumah');
            if($imagedatarumah != ''){
                $imagedatarumah->storeAs('public/rumahcustomer/', $imagedatarumah->hashName());
                $rumah      = 'rumahcustomer/'.$imagedatarumah->hashName();
            }else{
                $rumah = null;
            }

            $idordersalesnya = $saveordersales->t_ordersales_id;

            $this->mcustomer->storeordersalesrequestfromcustomer($request,$foto,$ktp,$rumah,$idordersalesnya);

            // $qcekid = DB::table('t_ordersales')
            //         ->where('m_karyawan_idkaryawan',$request->m_karyawan_idkaryawan)
            //         ->where('m_productlist_id',$request->m_productlist_id)
            //         ->where('t_ordersales_idnumber',$request->t_ordersales_idnumber)
            //         ->get()
            //         ->first();

            //------------------------
            $sharedhistoryorder = new SharedController;
            $sharedhistoryorder->savehistoryorder($saveordersales->t_ordersales_id,$request->m_karyawan_idkaryawan,"Permohonan");
            //------------------------

            $returnnya = 'Data berhasil di input';
            $responstatus = '1';

            //------------- simpan notifikasi 
            $qcustomer = Mcustomer::where('m_customer_nik',$request->m_customer_nik)->first();
            $bulan = array (
                'januari'       => 1,
                'februari'      => 2,
                'maret'         => 3,
                'april'         => 4,
                'mei'           => 5,
                'juni'          => 6,
                'juli'          => 7,
                'agustus'       => 8,
                'september'     => 8,
                'oktober'       => 10,
                'november'      => 11,
                'desember'      => 12,
            );
    
            $explodebulan = explode(' ',$request->t_ordersales_tanggal);
    
            $bulannya = $bulan[strtolower($explodebulan[1])];
    
            $tanggal = $explodebulan[2].'-'.$bulannya.'-'.$explodebulan[0];

            $pesan = 'Pelanggan Dengan Nama : '.$request->m_customer_nama.', No. KTP : '.$request->m_customer_nik.'. Dengan Alamat : '.$request->m_customer_namajalan.', Kel.'.$request->kelurahanname.' Kec.'.$request->kecamatanname.' Kota/Kab.'.$request->cityname.' Prov.'.$request->provincename.'. '.$request->m_customer_kodepos.'. Melakukan Pengajuan Pemasangan Pada Tanggal '.date('d/m/Y',strtotime($tanggal));

            $isipesan = [
                'idpengirim'    => $qcustomer->m_customer_id ?? null,
                'idpenerima'    => $request->m_karyawan_idkaryawan,
                'judul'         => 'Pengajuan Pemasangan',
                'pesan'         => $pesan,
                'tipepesan'     => 'Pengajuan'
            ];
            $this->mnotifikasi->storenotifikasi($isipesan['idpengirim'],$isipesan['idpenerima'],$isipesan['judul'],$isipesan['pesan'],$isipesan['tipepesan']);
            //--------------------------
        // }

        if($responstatus == '2'){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $returnnya
            ], 200);
       }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Success"
                ],
                "data" => $returnnya
            ], 201);
       }
       
    }
    

    //----------------- edit order sales
    public function ambildataeditordersales(){
        $id = $_GET['nikcustomer'] ?? null;

        $nikcustomer = str_replace(' ','',$id);
        $cekdatanya = $this->mordersales->dtordersalesedit($nikcustomer);

        if(!empty($cekdatanya->t_ordersales_id)){

            // $select = [
            //     't_ordersales.*',
            //     'm_customer.*',
            //     'indoregion_villages.name as villagename',
            //     'indoregion_districts.name as districtname',
            //     'indoregion_regencies.name as citiname',
            //     'indoregion_provinces.name as provincename'
            // ];

            $dtarray = [
                't_ordersales_idnumber'     => $cekdatanya->t_ordersales_idnumber,
                't_ordersales_nomyir'       => $cekdatanya->t_ordersales_nomyir,
                't_ordersales_nosc'         => $cekdatanya->t_ordersales_nosc,
                'm_customer_nik'            => $cekdatanya->m_customer_nik,
                'm_customer_nama'           => $cekdatanya->m_customer_nama,
                'm_customer_email'          => strtolower($cekdatanya->m_customer_email),
                'm_customer_hp'             => $cekdatanya->m_customer_hp,
                'm_customer_namajalan'      => $cekdatanya->m_customer_namajalan,
                'm_customer_kodepos'        => $cekdatanya->m_customer_kodepos,
                'm_customer_foto'           => $cekdatanya->m_customer_foto,
                'm_customer_fotoktp'        => $cekdatanya->m_customer_fotoktp,
                'm_customer_fotorumah'      => $cekdatanya->m_customer_fotorumah,
                'm_productlist_mbps'        => $cekdatanya->m_productlist_mbps,
                'm_productlist_name'        => $cekdatanya->m_productlist_name,
                'provincename'              => $cekdatanya->provincename,
                'citiname'                  => $cekdatanya->citiname,
                'districtname'              => $cekdatanya->districtname,
                'villagename'               => $cekdatanya->villagename,
            ];
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => $dtarray
            ], 200);
       }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
       }
    }

    public function updateordersales(Request $request){
        date_default_timezone_set('Asia/Jakarta');

        // $datapost = $request->all();
            // $idnumber = str_replace(' ','',$request->t_ordersales_idnumber);
    
      
            $saveupdateordersales = $this->mordersales->updateordersales($request);
            $this->mcustomer->updateordersales($request);

            // $qcekid = DB::table('t_ordersales')
            //         ->where('m_karyawan_idkaryawan',$request->m_karyawan_idkaryawan)
            //         ->where('m_productlist_id',$request->m_productlist_id)
            //         ->where('t_ordersales_idnumber',$request->t_ordersales_idnumber)
            //         ->get()
            //         ->first();

            // //------------------------
            // $sharedhistoryorder = new SharedController;
            // $sharedhistoryorder->savehistoryorder($qcekid->t_ordersales_id,$request->m_karyawan_idkaryawan,"Permohonan");
            // //------------------------

       if($saveupdateordersales){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Data berhasil di update'
            ], 200);
       }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => null
            ], 201);
       }
    }
    
    public function ceknoktpcustomer(Request $request){
        date_default_timezone_set('Asia/Jakarta');

        $ceknoktpcustomer = $this->mordersales->qceknoktpcustomer($request);

       if(empty($ceknoktpcustomer->m_customer_nik)){
            return response()->json([
                "response" => [
                    "status"    => 200,
                    "message"   => "Success"
                ],
                "data" => 'Nik Tidak Tersedia'
            ], 200);
       }else{
            return response()->json([
                "response" => [
                    "status"    => 201,
                    "message"   => "Failed"
                ],
                "data" => 'Nik Tersedia'
            ], 201);
       }
    }
    
}
