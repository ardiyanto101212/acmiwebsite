<?php

namespace App\Http\Controllers\Shared;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\XMLWriter;

use App\Models\Mabsensi;
use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\Mcategoryproduct;
use App\Models\Mcitie;
use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Morder;
use App\Models\Mcustomer;
use App\Models\Mconfigurasi;
use App\Models\Mhistoryorder;

use Carbon\Carbon;
use Carbon\CarbonPeriod;

use DB;
use Auth;
use Mail;

//--------------- firebase
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
//---------------------------

class SharedController extends Controller
{
    protected $menus;
    private $mabsensi;
  
    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('User');
            return $next($request);
        });

        $this->mabsensi = new Mabsensi;
        $this->morder = new Morder;
        $this->mcustomer = new Mcustomer;
        $this->mconfigurasi = new Mconfigurasi;
        $this->mkaryawan = new Mkaryawan;
    }

    public function totaltimeabsen($in,$out){
        date_default_timezone_set('Asia/Jakarta');

        $awal  = date_create($in);
        $akhir = date_create($out);
        $diff  = date_diff( $awal, $akhir );
        
        // echo 'Selisih waktu: ';
        // echo $diff->y . ' tahun, ';
        // echo $diff->m . ' bulan, ';
        // echo $diff->d . ' hari, ';
        // echo $diff->h . ' jam, ';
        // echo $diff->i . ' menit, ';
        // echo $diff->s . ' detik, ';

        $totaltime = $diff->h.'h:'.$diff->i.'m:'.$diff->s.'s';

        return $totaltime;
    }

    public function updateabsensi($id,$in,$out){
        date_default_timezone_set('Asia/Jakarta');

        $qcekabsensi = Mabsensi::where('t_absensi_id',$id)
                            ->get()
                            ->first();

            if(!empty($qcekabsensi->t_absensi_id)){
    
                $totaltime = $this->totaltimeabsen($in,$out);
    
                $dataarray = [
                    't_absensi_in'          => $in,
                    't_absensi_out'         => $out,
                    't_absensi_status'      => '2',
                    't_absensi_totaltime'   => $totaltime
                ];
    
                Mabsensi::where('t_absensi_id',$qcekabsensi->t_absensi_id)->update($dataarray);
            }

        return true;
    }

    public function savehistoryorder($t_order_id,$m_karyawan_idkaryawan,$t_order_status){
        date_default_timezone_set("Asia/Jakarta");
        $dtarray = [
            't_order_id'            => $t_order_id,
            'm_karyawan_idkaryawan' => $m_karyawan_idkaryawan,
            't_order_status'        => $t_order_status
        ];

        Mhistoryorder::create($dtarray);

        return true;
    }

    public function updatasalesdanteknisi(){
        date_default_timezone_set('Asia/Jakarta');

        $pathfile = public_path().'/jsonfirebase/internal-vem-firebase-adminsdk-7wv6z-2127382de8.json';

        $serviceAccount = ServiceAccount::fromJsonFile($pathfile);
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // ->withDatabaseUri('https://laraveltesting-bd2b9.firebaseio.com/')
        ->withDatabaseUri('https://internal-vem-default-rtdb.firebaseio.com/')
        ->create();

        $database   = $firebase->getDatabase();

        $qdataupfirebase = $this->mkaryawan->datakarywanupfirebase(3);

        // dd($qdataupfirebase);
        foreach($qdataupfirebase as $list){
        $database->getReference('DataKaryawan')
                        ->push([
                        'alamat'        => $list->m_karyawan_alamat,
                        'email'         => strtolower($list->m_karyawan_email),
                        'idkaryawan'    => $list->m_karyawan_idkaryawan,
                        'jabatan'       => $list->m_title_name,
                        'jk'            => $list->m_karyawan_jeniskelamin,
                        'nama'          => $list->m_karyawan_nama,
                        'notelp'        => $list->m_karyawan_notelp,
                        'password'      => '12345678',
                        'username'      => strtolower($list->m_karyawan_email),
                        'witel'         => $list->districtname
                        ]);
        }
        

    }

    public function editjabatansales(){
        date_default_timezone_set('Asia/Jakarta');

        $pathfile = public_path().'/jsonfirebase/internal-vem-firebase-adminsdk-7wv6z-2127382de8.json';
        // dd($pathfile);
            // $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/Laraveltesting-6aeda3a963f2.json');
            $serviceAccount = ServiceAccount::fromJsonFile($pathfile);
            $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            // ->withDatabaseUri('https://laraveltesting-bd2b9.firebaseio.com/')
            ->withDatabaseUri('https://internal-vem-default-rtdb.firebaseio.com/')
            ->create();


            //DataReportSales
            //AbsensiKaryawan

            $database   = $firebase->getDatabase();
            $reference  = $database->getReference('/DataKaryawan');
            $snapshot   = $reference->getSnapshot();
            $k1         = $snapshot->getValue();
            
            //initialize Array
            $update = [];
            foreach($k1 as $k => $v)
            {
                // $update[$k. "/is_read"]="true";
                if($v['jabatan'] == 'Supervisor'){
                    $update[$k. "/password"]='123123';
                }
                
                
            }
            // dd($tester);

            //Update the Firebase Database
            $database->getReference('/DataKaryawan')->update($update);
    }

    public function editemailkaryawan($emaillama,$emailbaru){
        date_default_timezone_set('Asia/Jakarta');

        $pathfile = public_path().'/jsonfirebase/internal-vem-firebase-adminsdk-7wv6z-2127382de8.json';
        // dd($pathfile);
            // $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/Laraveltesting-6aeda3a963f2.json');
            $serviceAccount = ServiceAccount::fromJsonFile($pathfile);
            $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            // ->withDatabaseUri('https://laraveltesting-bd2b9.firebaseio.com/')
            ->withDatabaseUri('https://internal-vem-default-rtdb.firebaseio.com/')
            ->create();


            //DataReportSales
            //AbsensiKaryawan

            $database   = $firebase->getDatabase();
            $reference  = $database->getReference('/DataKaryawan');
            $snapshot   = $reference->getSnapshot();
            $k1         = $snapshot->getValue();
            
            //initialize Array
            $update = [];
            foreach($k1 as $k => $v)
            {
                // $update[$k. "/is_read"]="true";
                if($v['email'] == $emaillama){
                    $update[$k. "/email"]=$emailbaru;
                }
                
                
            }
            // dd($tester);

            //Update the Firebase Database
            $database->getReference('/DataKaryawan')->update($update);
    }

    public function editpwuser($email,$password){
        date_default_timezone_set('Asia/Jakarta');

        $pathfile = public_path().'/jsonfirebase/internal-vem-firebase-adminsdk-7wv6z-2127382de8.json';
        // dd($pathfile);
            // $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/Laraveltesting-6aeda3a963f2.json');
            $serviceAccount = ServiceAccount::fromJsonFile($pathfile);
            $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            // ->withDatabaseUri('https://laraveltesting-bd2b9.firebaseio.com/')
            ->withDatabaseUri('https://internal-vem-default-rtdb.firebaseio.com/')
            ->create();


            //DataReportSales
            //AbsensiKaryawan

            $database   = $firebase->getDatabase();
            $reference  = $database->getReference('/DataKaryawan');
            $snapshot   = $reference->getSnapshot();
            $k1         = $snapshot->getValue();
            
            //initialize Array
            $update = [];
            foreach($k1 as $k => $v)
            {
                // $update[$k. "/is_read"]="true";
                if($v['email'] == $email){
                    $update[$k. "/password"]=$password;
                }
                
                
            }
            // dd($tester);

            //Update the Firebase Database
            $database->getReference('/DataKaryawan')->update($update);
    }

    public function firebaseabsen(){
        date_default_timezone_set('Asia/Jakarta');

        $pathfile = public_path().'/jsonfirebase/internal-vem-firebase-adminsdk-7wv6z-2127382de8.json';
        // dd($pathfile);
            // $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/Laraveltesting-6aeda3a963f2.json');
            $serviceAccount = ServiceAccount::fromJsonFile($pathfile);
            $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            // ->withDatabaseUri('https://laraveltesting-bd2b9.firebaseio.com/')
            ->withDatabaseUri('https://internal-vem-default-rtdb.firebaseio.com/')
            ->create();


            //DataReportSales
            //AbsensiKaryawan

            $database   = $firebase->getDatabase();
            $reference  = $database->getReference('/AbsensiKaryawan');
            $snapshot   = $reference->getSnapshot();
            $k1         = $snapshot->getValue();
            
            if(!empty($k1)){
                foreach($k1 as $key => $value){
                    // dd($value['idkar']);
                    $idkar      = $value['idkar'];
                    // $tanggal    = date('Y-m-d',strtotime($value['tanggal']));
                    $status     = $value['status'];
                    // $tes .= date('Y-m-d',strtotime($value['tanggal'])).'<br>';
                    $lokasi     = $value['lokasi'];
                    $this->uptodateabsensi($idkar,$status,$lokasi,$value);
                }
            }
            // $tes = '';
            

            // dd($tes);

            // dd($database);
            // $newPost = $database
            // ->getReference('blog/posts')
            // ->push([
            // 'title' => 'Post title',
            // 'body' => 'This should probably be longer.'
            // ]);
        //$newPost->getKey(); // => -KVr5eu8gcTv7_AHb-3-
        //$newPost->getUri(); // => https://my-project.firebaseio.com/blog/posts/-KVr5eu8gcTv7_AHb-3-
        //$newPost->getChild('title')->set('Changed post title');
        //$newPost->getValue(); // Fetches the data from the realtime database
        //$newPost->remove();

        // echo"<pre>";
        // print_r($newPost->getvalue());
    }

    public function uptodateabsensi($idkar,$status,$lokasi,$value){

        date_default_timezone_set('Asia/Jakarta');

        $datenow = date('Y-m-d',strtotime($value['tanggal']));//date('Y-m-d');

        if($status == 'Jam Masuk'){
            $dtkaryawan = Mkaryawan::where('m_karyawan_nik',$idkar)->get()->first();
            $qcekabsensi = $this->mabsensi->cekabsensiforebase($dtkaryawan,$datenow);
            
            if(empty($qcekabsensi->t_absensi_id)){
                $this->mabsensi->saveabsensifirebase($dtkaryawan,$lokasi);
            }
            
            
        }else{
            
            $dtkaryawan = Mkaryawan::where('m_karyawan_nik',$idkar)->get()->first();

            $qcekabsensi = $this->mabsensi->cekabsensiforebase($dtkaryawan,$datenow);
            
            if(!empty($qcekabsensi->t_absensi_id)){
                $in     = $qcekabsensi->t_absensi_in;
                $out    = date('Y-m-d H:i:s');
    
                $totaltime = $this->totaltimeabsen($in,$out);
                
                $this->mabsensi->updateabsensi($qcekabsensi,$totaltime);

            }
            
        }

        return true;
    }



    public function firebaseorder(){
        date_default_timezone_set('Asia/Jakarta');

        $pathfile = public_path().'/jsonfirebase/internal-vem-firebase-adminsdk-7wv6z-2127382de8.json';
        // dd($pathfile);
            // $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/Laraveltesting-6aeda3a963f2.json');
            $serviceAccount = ServiceAccount::fromJsonFile($pathfile);
            $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            // ->withDatabaseUri('https://laraveltesting-bd2b9.firebaseio.com/')
            ->withDatabaseUri('https://internal-vem-default-rtdb.firebaseio.com/')
            ->create();


            //DataReportSales
            //AbsensiKaryawan

            $database   = $firebase->getDatabase();
            $reference  = $database->getReference('/DataReportSales');
            $snapshot   = $reference->getSnapshot();
            $k1         = $snapshot->getValue();
            
            // dd($k1);

            if(!empty($k1)){
                foreach($k1 as $key => $value){
                    if(!empty($value['snoorder'])){
                        $orderno      = $value['snoorder'];
                        $qcekorderno  = Morder::where('t_order_idnumber',$orderno)->get()->first();
                        if(empty($qcekorderno->t_order_idnumber)){
                            $this->morder->saveorderfirebase($value);
    
                            $qcekorderid = Morder::where('t_order_idnumber',$orderno)->get()->first();
                            // dd($qcekorderid->t_order);
                            $this->mcustomer->saveorderfirebase($value,$qcekorderid->t_order_id);
                        }else{
                            $dtaray = [
                                't_order_status'  => $value['status']
                            ];
                            Morder::where('t_order_id',$qcekorderno->t_order_id)->update($dtaray);
                        }
                    }
                }
            }
           
    }

    public function udatestatusfirebase($ordernumber,$status){
        date_default_timezone_set('Asia/Jakarta');

        $pathfile = public_path().'/jsonfirebase/internal-vem-firebase-adminsdk-7wv6z-2127382de8.json';
        // dd($pathfile);
            // $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/Laraveltesting-6aeda3a963f2.json');
            $serviceAccount = ServiceAccount::fromJsonFile($pathfile);
            $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            // ->withDatabaseUri('https://laraveltesting-bd2b9.firebaseio.com/')
            ->withDatabaseUri('https://internal-vem-default-rtdb.firebaseio.com/')
            ->create();


            //DataReportSales
            //AbsensiKaryawan

            $database   = $firebase->getDatabase();
            $reference  = $database->getReference('/DataReportSales');
            $snapshot   = $reference->getSnapshot();
            $k1         = $snapshot->getValue();
            
            //initialize Array
            $update = [];
            foreach($k1 as $k => $v)
            {
                // $update[$k. "/is_read"]="true";
                if($v['snoorder'] == $ordernumber){
                    $update[$k. "/status"]=$status;
                }
                
                
            }
            // dd($tester);

            //Update the Firebase Database
            $database->getReference('/DataReportSales')->update($update);
    }

    public function updateconfigurationfirebase($idorder){
        date_default_timezone_set("Asia/Jakarta");
        $qcekorder      = $this->morder->qdetailorder($idorder);
        $ordernumber    = $qcekorder->t_order_idnumber;
        // dd($ordernumber);

        date_default_timezone_set('Asia/Jakarta');

        $pathfile = public_path().'/jsonfirebase/internal-vem-firebase-adminsdk-7wv6z-2127382de8.json';
        // dd($pathfile);
            // $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/Laraveltesting-6aeda3a963f2.json');
            $serviceAccount = ServiceAccount::fromJsonFile($pathfile);
            $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            // ->withDatabaseUri('https://laraveltesting-bd2b9.firebaseio.com/')
            ->withDatabaseUri('https://internal-vem-default-rtdb.firebaseio.com/')
            ->create();


            //DataReportSales
            //AbsensiKaryawan

            $database = $firebase->getDatabase();
            $reference = $database->getReference('/LaporanTeknisi');
            $snapshot = $reference->getSnapshot();
            $k1 = $snapshot->getValue();
            
            // dd($k1);

            if(!empty($k1)){
                $qcekconfigurasi = Mconfigurasi::where('t_order_id',$idorder)->get()->first();
                if(empty($qcekconfigurasi->t_order_id)){
                   
                    foreach($k1 as $key => $value){
                        if($value['nomorOrder'] == $ordernumber){
                            $this->mconfigurasi->saveconfiguration($value,$qcekorder);
                        }
                    }
                }
                
            }
    }


    public function exceltotimestamp($date){
        date_default_timezone_set("Asia/Jakarta");
        if(preg_match("/ /",$date)){
           $up = null;
        }else{
            if(preg_match("/\//",$date)){
                $up = null;
            }else if(preg_match("/-/",$date)){
                $up = null;
            }else{
                $datenya = date("Y-m-d",$date);

                if($datenya > '1970-01-01'){
                    $up = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($datenya);;
                }else{
                    $up = null;
                } 
            }
            
        }
        
        
        return $up;
    }


    //----------------------- for email 

    public function sendemail($subject,$to,$receiver,$content){

            try{

                $subjecemail    = $subject;
                $toemail        = $to;

                $dataarray      = array(
                    'nama'   => $receiver, 
                    'pesan'  => $content
                );

                Mail::send('email.notifemail', $dataarray, function ($message) use ($toemail,$content,$subjecemail) {
                    
                    $message->to($toemail)
                    ->subject($subjecemail);
                });   
                
                $responsemessage = 'Success <br>'.$receiver.' : <br>'.$toemail.'<br><br>';
            }
            catch (Exception $e){
                return response (['status' => false,'errors' => $e->getMessage()]);
                $responsemessage = 'Failed <br>'.$receiver.' : <br>'.$toemail.'<br><br>';
            }
            
            //$responsemessage = 'Success <br>'.$receiver.' : <br>'.$to.'<br><br>';
            return $responsemessage;
        }

    //------------------------ array option mbps product

    public function arraymbps(){
        $array = [
            '-',
            '15 Mbps',
            '20 Mbps',
            '21.6 Mbps',
            '30 Mbps',
            '40 Mbps',
            '50 Mbps',
            '100 Mbps'
        ];

        return $array;
    }

    //------------------------ array option category2 product

    public function arraycat2prod(){
        $array = [
            null,
            '2 Play',
            '3 Play',
            'Digital Channel',
            'Gamer',
            'Orbit',
            'Reguler',
            'Smart Home'
        ];

        return $array;
    }

    function generateRandomStringpwuser() {
        $length = 6;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function trigerkurangstock($idproduk,$qty){
        $qprod = DB::table('m_productlist')->where('m_productlist_id',$idproduk)->first();

        $stokawal = $qprod->m_productlist_stock;
        $hitng = $stokawal-$qty;
        // dd($hitng);
        $query = DB::table('m_productlist')->where('m_productlist_id')->update(['m_productlist_stock' => $hitng]);
        // dd($query);
        return true;
    }

    public function nominaluang($uang){
        // $uang = '1123456789';
        $komaberapa = [
            '1' => substr($uang,0,1),
            '2' => substr($uang,0,2),
            '22' => substr($uang,0,1).','.substr($uang,1,1),
            '222' => substr($uang,0,2).','.substr($uang,2,2),
            '3' => substr($uang,0,3),
            '8' => substr($uang,0,2).','.substr($uang,2,1),
            '9' => substr($uang,0,3).','.substr($uang,3,1),
        ];

        if($uang == 0){
            $nominal = '0';
        }else if($uang <= 999){
            $nominal = $komaberapa[1].' Rp';  //ok
        }else if($uang <= 9999){ 
            $nominal = $komaberapa[1].' K';  //ok
        }else if($uang <= 99999){
            $nominal = $komaberapa[2].' K'; //ok
        }else if($uang <= 999999){
            $nominal = $komaberapa[3].' K'; //ok
        }else if($uang <= 9999999){
            $nominal = $komaberapa[22].' Jt'; //0k
        }else if($uang <= 99999999){
            $nominal = $komaberapa[8].' Jt'; //ok
        }else if($uang <= 999999999){
            $nominal = $komaberapa[9].' Jt'; //ok
        }else if($uang <= 9999999999){
            $nominal = $komaberapa[22].' M'; //ok
        }else if($uang <= 99999999999){
            $nominal = $komaberapa[8].' M'; //ok
        }else if($uang <= 999999999999){
            $nominal = $komaberapa[9].' M'; //ok
        }else if($uang <= 9999999999999){
            $nominal = $komaberapa[22].' T'; //ok
        }else if($uang <= 99999999999999){
            $nominal = $komaberapa[8].' T'; //ok
        }else if($uang <= 999999999999999){
            $nominal = $komaberapa[9].' T';
        }

        return $nominal;
    }

}
