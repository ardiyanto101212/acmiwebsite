<?php

namespace App\Http\Controllers\Shared;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\XMLWriter;

use App\Models\Mabsensi;
use App\Models\MapGroupMenu;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\Mcategoryproduct;
use App\Models\Mcitie;
use App\Models\User;
use App\Models\Mproductlist;
use App\Models\Mproductlistdetail;
use App\Models\Mkaryawan;
use App\Models\Morder;
use App\Models\Mcustomer;
use App\Models\Mconfigurasi;
use App\Models\Mhistoryorder;

use Carbon\Carbon;
use Carbon\CarbonPeriod;

use DB;
use Auth;
use Mail;

//--------------- firebase
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
//---------------------------

class CarbonperiodeController extends Controller
{
    public function getWeekRange($year,$month){
        date_default_timezone_set('Asia/Jakarta');
 
        //format string
        $f = 'Y-m-d';
    
        //if you want to record time as well, then replace today() with now()
        //and remove startOfDay()
        $today = Carbon::createFromDate($year,$month);//Carbon::today();
        $date = $today->copy()->firstOfMonth()->startOfDay();
        $eom = $today->copy()->endOfMonth()->startOfDay();
   
        $dates = [];
  
        for($i = 1; $date->lte($eom); $i++){
            
            //record start date 
            $startDate = $date->copy();
            
            //loop to end of the week while not crossing the last date of month
           
            while($date->dayOfWeek != Carbon::SUNDAY && $date->lte($eom)){
                    $date->addDay(); 
                }
            
            
            if($startDate->format($f) == $date->format($f)){

            }else{
                $dates['w'.$i] = $startDate->format($f) . '&&' . $date->format($f);
            }

            
            $date->addDay();
          }
      
        return $dates;
    }

    public function getWeekRangetwo(){
        date_default_timezone_set('Asia/Jakarta');

        $year = Carbon::now()->year;
        $month = Carbon::now()->month;

        $date = Carbon::createFromDate($year,$month);
        $numberOfWeeks = floor($date->daysInMonth / Carbon::DAYS_PER_WEEK);
        $start = [];
        $end = [];
        $j=1;
        for ($i=1; $i <= $date->daysInMonth ; $i++) {
            Carbon::createFromDate($year,$month,$i); 
            $start['Week: '.$j.' Start Date']= (array)Carbon::createFromDate($year,$month,$i)->startOfWeek()->toDateString();
            $end['Week: '.$j.' End Date']= (array)Carbon::createFromDate($year,$month,$i)->endOfweek()->toDateString();
            $i+=7;
            $j++; 
        }
        $result = array_merge($start,$end);
        $result['numberOfWeeks'] = ["$numberOfWeeks"];
        
        return $result;
    }

    public function logikamingguan($plusminus){
        date_default_timezone_set('Asia/Jakarta');
        $datenya = date('Y-m-d');
        
        //----------
        $start = Carbon::createFromDate(date("Y"),date('m'),date('d'))->startOfWeek();
        $end   = Carbon::createFromDate(date("Y"),date('m'),date('d'))->endOfWeek();

        $dateawal  = date("Y-m-d",strtotime($start));
        $dateakhir = date("Y-m-d",strtotime($end));

        // $minus = date('Y-m-d', strtotime($plusminus." days",strtotime($datenya))); //Carbon::now()->subDays(7);

        $jmlhari = 7;

        if($plusminus == 0){
            $awal  = $dateawal;
            $akhir = $dateakhir;
        }else if($plusminus > 0){
            // $replace = $plusminus;

            $hitungnext = 7*$plusminus;
            $awal = date('Y-m-d', strtotime('+'.$hitungnext." days",strtotime($dateawal)));
            $akhir = date('Y-m-d', strtotime('+'.$hitungnext." days",strtotime($dateakhir)));
            
        }else if($plusminus < 0){
            $strreplace = str_replace('-','',$plusminus);
            $hitungnext = 7*$strreplace;
            $awal = date('Y-m-d', strtotime('-'.$hitungnext." days",strtotime($dateawal)));
            $akhir = date('Y-m-d', strtotime('-'.$hitungnext." days",strtotime($dateakhir)));
        }

     

        $data = [
            'awal'      => $awal,
            'akhir'     => $akhir
        ];

        return $data;
    }
}
