<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use App\Models\MapGroupMenu;
use App\Models\User;

use Auth;
use Route;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $this->performLogout($request);
        // $request->user()->revoke();
        // return response()->json([
        //     "response" => [
        //         "status"    => 200,
        //         "message"   => "Success"
        //     ],
        //     "data" => null
        // ], 200);

        return redirect()->route('login');
    }

    public function logoutapi(Request $request)
    {
        // $this->performLogout($request);
        // return redirect()->route('login');
        // $request->user()->revoke();
        // Auth::user()->AauthAcessToken()->delete();
        // $tes = $request->user()->Token()->revoke();
        // dd($tes);
        // return response()->json([
        //     "response" => [
        //         "status"    => 200,
        //         "message"   => "Success"
        //     ],
        //     "data" => null
        // ], 200);

        $request->user()->token()->revoke();
        return response()->json([
            'message'=> 'Successfully logged out'
            ]);
    }

    public function login(Request $request)
    {   

        // dd(\Route::current());
        //dd(str_split(\Route::current()->uri, 3)[0]);
        //dd('test');
        $input = $request->all();
        
        $this->validate($request, [
            'email' => 'required',
            // 'username' => 'required',
            'password' => 'required',
        ]);

        $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $arrayauthuser = [
            $fieldType => $input['email'],
            'password' => $input['password'],
            'deleted_at' => null
        ];
       
        if(auth()->attempt($arrayauthuser))
        {

            
            // return response()->json(['admin_sucsess']);
            // if (\Route::current()->getName() == 'LoginApi') {
            //dd(str_split(Route::current()->uri, 3));
            if (str_split(Route::current()->uri, 3)[0] == 'api') {
                
                $hakakses   = MapGroupMenu::where('id_groups',auth()->user()->id_groups)->get();
                $emailuser  = auth()->user()->email;
                // $karyawan   = $this->mkaryawanbuilder->qallkaryawanbyemail($emailuser,$input);

                $iduser     = auth()->user()->id;

                $token      = auth()->user()->createToken('aksesvem')->accessToken;
                
                // User::where('id',$iduser)
                // ->update(['master_users_devicekey' => $input['master_users_devicekey']]);

                // dd($emailuser);
                // return response()->json(
                //     [
                //         'user'      => auth()->user(),
                //         'karyawan'  => $karyawan,
                //         // 'hakakses'  => $hakakses,
                //         'token'     => $token
                //     ], 200);
                // return response()->json(['status' => 200, 'message' => 'Login Successed']);
                
                $customer   = $this->mcustomerakun->qalldatabyemail($emailuser,$input);

                $arraydataauth = [
                    'user'      => auth()->user(),
                    
                    // 'hakakses'  => $hakakses,
                    'token'     => $token
                ];

                return response()->json([
                    "response" => [
                        "status"    => 200,
                        "message"   => "Success"
                    ],
                    "data" => $arraydataauth
                ], 200);

            }
            
            return redirect()->route('welcome_view_index');
        }else{
            if (str_split(Route::current()->uri, 3)[0] == 'api') {
                return response()->json([
                    "response" => [
                        "status"    => 200,
                        "message"   => "Failed"
                    ],
                    "data" => null
                ], 200);
            }else{
                return redirect()->route('login')->with(['alertprogress' => 'Email-Address And Password Are Wrong.']);
            }
            
            // return redirect()
            //         ->route('login')
            //         ->with('error','Email-Address And Password Are Wrong.');
        }
    }
}
