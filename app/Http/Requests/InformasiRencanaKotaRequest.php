<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class InformasiRencanaKotaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        // dd($request->all());

        $validate = [
            'nama_pemohon'          => 'required',
            'nomor_telp'            => 'required',
            'nik_pemohon'           => 'required',
            'email_pemohon'         => 'required',
            'alamat_pemohon'        => 'required',
            'lokasi_yang_dicari'    => 'required',
            'kategori_pemohon'      => 'required',
        ];

        return $validate;
    }

    public function messages()
    {
        $messages = [
            'nama_pemohon.required'                 => 'Kolom :attribute tidak boleh kosong',
            'nomor_telp.required'                   => 'Kolom :attribute tidak boleh kosong',
            'nik_pemohon.required'                  => 'Kolom :attribute tidak boleh kosong',
            'email_pemohon.required'                => 'Kolom :attribute tidak boleh kosong',
            'alamat_pemohon.required'               => 'Kolom :attribute tidak boleh kosong',
            'lokasi_yang_dicari.required'           => 'Kolom :attribute tidak boleh kosong',
            'kategori_pemohon.required'             => 'Kolom :attribute tidak boleh kosong',
        ];
        return $messages;
    }
}
