<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use URL;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {   
        // $tes = URL::current();
        
        // dd();
        if($request->segment(1) == 'api'){
              
            // return response()->json([
            //     "response" => [
            //         "status"    => 200,
            //         "message"   => "Failed"
            //     ],
            //     "data" => null
            // ], 200);

            return route('sessionapihabis');
            
        }else{
            if (! $request->expectsJson()) {
                return route('login');
            }
        }

        
    }
}
