<?php

namespace App\Helper;

use Request;
use Illuminate\Support\Facades\Session;

use Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\UrlGenerator;
use App\Models\MasterMenu;
use App\Models\MapGroupMenu;
use App\Models\Enum;
use App\Models\TicketHeader;
use App\Models\TicketDetail;
use Illuminate\Support\Facades\Cache;
use DB;

class ComponentBuilder {

        //     <li class="active">
        //         <a href="index.html">
        //             <span class="link-title">Dashboard</span> 
        //             <i class="mdi mdi-gauge link-icon"></i>
        //         </a>
        //     </li>
       

        // <li>
        //     <a href="#menu1head" data-toggle="collapse" aria-expanded="false">
        //         <span class="link-title">UIElements</span>
        //         <i class="mdi mdi-bullseye link-icon"></i>
        //     </a>
        //     <ul class="collapse navigation-submenu" id="menu1head">
        //         <li><a href="pages/ui-components/buttons.html">Buttons</a></li>
        //         <li><a href="pages/ui-components/dropdowns.html">Dropdowns</a></li>
        //     </ul>
        // </li>

        public static function sideMenuBuilder() {
            // $route = Route::currentRouteName();
            $currentroute = Route::currentRouteName(); // routename_action_functioncontroller , example : purchase_view_index

            // example user_view, and explode
           
            $explodeRouteName       = explode("_", $currentroute); // pisah 
            $routenameRequest       = $explodeRouteName[0] ?? null; // dapat routename
            $routenameActionRequest = $explodeRouteName[1] ?? null; // dapat action
            
            $user = Auth::user(); // auth user active

            $id_groups      = $user->id_groups; // dapat id group
            $name_groups    = $user->group->name; // dapat name di table master_groups
            
            Cache::forget($name_groups);
            if (Cache::has($name_groups)) {
                $parentMenu = Cache::get($name_groups);
            }else{
                $parentMenu = MasterMenu::where('category', '!=', 'Z')
                            ->where('sort', '=', '#')
                            ->orderBy('category', 'ASC')
                            ->get();
                Cache::put($name_groups, $parentMenu, $seconds = 1200); // 2 jam
            }
            // $parentMenu = Cache::get('listMenuParent');
            // dd($parentMenu);
            // dd($parentMenu);
            //dd($routenameActionRequest);
            $printParentMenu = '';
            $no = 1;
        
            foreach ($parentMenu as $menu) {
                $noplus = $no++; // buat id plus untuk head child menu

                    //-- query child menu
                    $childMenu = MasterMenu::whereHas('mapgroupmenu', function($q) use ($id_groups){
                            $q->where('allow_view', '=', true)
                            ->where('id_groups', $id_groups);
                        })
                        ->where('category', '=', $menu->category)
                        ->where('sort', '!=', '#')
                        ->orderBy('sort', 'ASC')
                        ->get();
               
                        if(count($childMenu) <= 0){ //-- jika gaada sub menunya
                            //continue;
                            
                            $idgroup = Auth::user()->id_groups;
                           
                            $idmapgroupmenu = MapGroupMenu::where('allow_view', '=', true)
                                            ->where('id_groups', $idgroup)
                                            ->where('id_menus',$menu->id)
                                            ->select('allow_view')
                                            ->get()
                                            ->first();

                            //if($menu->sort != '#'){ // jika bukan # yaitu master sub menu
                                if($menu->routename == $routenameRequest){
                                    $activemnuhead = "active";
                                }else{
                                    $activemnuhead = "";
                                }
                                
                                $alow = $idmapgroupmenu->allow_view ?? false;

                                if($alow == true){
                                    $printParentMenu .= '
                                    <li class="'.$activemnuhead.'">
                                        <a href="' . url("/") . $menu->urlname . '">
                                            <span class="link-title">'.$menu->menuname.'</span> 
                                            <i class="'.$menu->icon.' link-icon"></i>
                                        </a>
                                    </li>';
                                }else{
                                    
                                }

                                
                                   
                            //}
                            
                                
                        }else{
                            
                            
                            $qcekcategory = MasterMenu::where("routename",$routenameRequest)->first(); // cek table mastermenu dimana routenamenya yg aktif
                           
                            if(!empty($qcekcategory->category)){ //-- jika categorynya ada
                                
                                if($menu->category == $qcekcategory->category){ // jika kategorynya sama maka akan open head child menu
                                    $activehead     = 'active';
                                    $expandedhead   = 'true';
                                    $colapsehead    = "";
                                    $ulchild        = "show";
                                    
                                }else{
                                    $activehead     = '';
                                    $expandedhead   = 'false';
                                    $colapsehead    = "collapsed";
                                    $ulchild        = "";
                                    
                                }

                            }else{

                                $activehead     = '';
                                $expandedhead   = 'false';
                                $colapsehead    = "collapsed";
                                $ulchild        = "";

                            }

                            //--- head menu child
                            $printParentMenu .= '
                            <li class="'.$activehead.'">
                                <a href="#menuhead'.$noplus.'" data-toggle="collapse" aria-expanded="'.$expandedhead.'" class="'.$colapsehead.'">
                                    <span class="link-title">'.$menu->menuname.'</span>
                                    <i class="'.$menu->icon.' link-icon"></i>
                                </a>
                                <ul class="collapse navigation-submenu '.$ulchild.'" id="menuhead'.$noplus.'">
                            ';

                            //--- loop menu child

                            foreach($childMenu as $child){
                                //dd($child->routename);
                                if($routenameRequest == $child->routename){
                                    $activechild = "active";
                                }else{
                                    $activechild = "";
                                }
                                $printParentMenu .= '<li><a href="' . url("/") . $child->urlname . '" class="'.$activechild.'">'.$child->menuname.'</a></li>';
                            }

                            $printParentMenu .= '</ul></li>';
                        }

                    
            }

            //-- lempar return
            return $printParentMenu;

        }

    public static function sideMenuBuilderlama() {
        // $route = Route::currentRouteName();
        $currentroute = Route::currentRouteName();


        // example user_view, and explode
        $explodeRouteName = explode("_", $currentroute);
        $routenameRequest = $explodeRouteName[0] ?? null;
        $routenameActionRequest = $explodeRouteName[1] ?? null;
        
        $user = Auth::user();
        $id_groups = $user->id_groups;
        $name_groups = $user->group->name;
        Cache::forget($name_groups);
        if (Cache::has($name_groups)) {
            $parentMenu = Cache::get($name_groups);
        }else{
        $parentMenu = MasterMenu::
                    where('category', '!=', 'Z')->
                    where('sort', '=', '#')->
                    get();
            Cache::put($name_groups, $parentMenu, $seconds = 1200); // 2 jam
        }
        // $parentMenu = Cache::get('listMenuParent');
        // dd($parentMenu);
        // dd($parentMenu);
        $printParentMenu = '';
        $printMenu = '';
        foreach ($parentMenu as $menu) {
                $childMenu = MasterMenu::
                                whereHas('mapgroupmenu', function($q) use ($id_groups){
                                    $q->
                                    where('allow_view', '=', true)->
                                    where('id_groups', $id_groups);
                                })->
                                where('category', '=', $menu->category)->
                                where('sort', '!=', '#')->
                                orderBy('sort', 'ASC')->
                                get();
                if(count($childMenu) <= 0){
                    continue;
                }
                
                $active = false;
                $printMenuChild = '';
                foreach ($childMenu as $child) {
                    # code...
                    // echo $child;
                    if($routenameRequest == $child->routename ){
                        $active = true;
                        $printMenuChild .= '<li class="active"><a class="menu-item" href="' . url("/") . $child->urlname . '"><i></i><span data-i18n="' . $child->menuname . '">' . $child->menuname . '</span></a></li>';
                        
                        // '<li class="kt-menu__item  kt-menu__item--active" aria-haspopup="true"><a href="' . url("/") . $child->urlname . '" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">' . $child->menuname . '</span></a></li>';
                    }else{
                        $printMenuChild .= '<li><a class="menu-item" href="' . url("/") . $child->urlname . '"><i></i><span data-i18n="' . $child->menuname . '">' . $child->menuname . '</span></a></li>';
                        
                        // '<li class="kt-menu__item " aria-haspopup="true"><a href="' . url("/") . $child->urlname . '" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">' . $child->menuname . '</span></a></li>';
                    }
                }
                
                if($active == true){
                    
                    $printParentMenu .= '<li class=" nav-item"><a href="#"><i class="'. $menu->icon .'"></i><span class="menu-title" data-i18n="' . $menu->menuname . '">' . $menu->menuname . '</span></a>' .
                                            '<ul class="menu-content">';
                    
                    // '<li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--open kt-menu__item--here" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-browser-2"></i><span class="kt-menu__link-text">'. $menu->menuname .'</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>' . 
                    // '<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>' .
                    //     '<ul class="kt-menu__subnav">' .
                    //         '<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">' . $menu->menuname . '</span></span></li>';

                }else{
                    $printParentMenu .= '<li class=" nav-item"><a href="#"><i class="'. $menu->icon .'"></i><span class="menu-title" data-i18n="' . $menu->menuname . '">' . $menu->menuname . '</span></a>' .
                                            '<ul class="menu-content">';
                    // $printParentMenu .= '<li class="kt-menu__item  kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-browser-2"></i><span class="kt-menu__link-text">'. $menu->menuname .'</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>' . 
                    // '<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>' .
                    //     '<ul class="kt-menu__subnav">' .
                    //         '<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">' . $menu->menuname . '</span></span></li>';

                }       
                // var_dump($menu->icon);   
                // print_r($printParentMenu) ;     
                
                $printParentMenu .= $printMenuChild;
                // $printParentMenu .= '</ul></div></li>';
                $printParentMenu .= '</ul></li>';
                $printMenu .= $printParentMenu;
                $active = false;
                $printParentMenu = '';
                $printMenuChild = '';
            // };
            // echo $menu;
        }
        // echo $printMenu;
        return $printMenu;
    }

    // public static function sideMenuBuilder() {
    //     // $route = Route::currentRouteName();
    //     $currentroute = Route::currentRouteName();


    //     // example user_view, and explode
    //     $explodeRouteName = explode("_", $currentroute);
    //     $routenameRequest = $explodeRouteName[0] ?? null;
    //     $routenameActionRequest = $explodeRouteName[1] ?? null;
    //     // dd($routenameRequest);
        
    //     $user = Auth::user();
    //     $id_groups = $user->id_groups;
    //     $name_groups = $user->group->name;
    //     // dd($name_groups);
    //     // dd(Auth::user()->group->name);
    //     // $dataMasterMenu = MasterMenu::
    //     //                     whereHas('mapgroupmenu', function($q) use ($id_groups){
    //     //                         $q->
    //     //                         where('allow_view', '=', true)->
    //     //                         where('id_groups', $id_groups);
    //     //                     })->
    //     //                     where('category', '!=', 'Z')->
    //     //                     orWhere('sort', '=', '#')->
    //     //                     get();
    //     // $parentMenu = $dataMasterMenu->where('sort', '=', '#');
    //     // Cache::pull('listMenuParent');
    //     if (Cache::has($name_groups)) {
    //         $parentMenu = Cache::get($name_groups);
    //     }else{
    //     $parentMenu = MasterMenu::
    //                 // whereHas('mapgroupmenu', function($q) use ($id_groups){
    //                 //     $q->
    //                 //     // where('allow_view', '=', true)->
    //                 //     where('id_groups', $id_groups);
    //                 // })->
    //                 where('category', '!=', 'Z')->
    //                 where('sort', '=', '#')->
    //                 get();
    //         Cache::put($name_groups, $parentMenu, $seconds = 1200); // 2 jam
    //     }
    //     // $parentMenu = Cache::get('listMenuParent');
    //     // dd($parentMenu);
    //     // dd($parentMenu);
    //     $printParentMenu = '';
    //     $printMenu = '';
    //     foreach ($parentMenu as $menu) {
    //         // echo MasterMenu::where('category', '=', $menu->category);
    //         // if ($menu->sort == '#') {
    //             $childMenu = MasterMenu::
    //                             whereHas('mapgroupmenu', function($q) use ($id_groups){
    //                                 $q->
    //                                 where('allow_view', '=', true)->
    //                                 where('id_groups', $id_groups);
    //                             })->
    //                             where('category', '=', $menu->category)->
    //                             where('sort', '!=', '#')->
    //                             orderBy('sort', 'ASC')->
    //                             get();
    //             if(count($childMenu) <= 0){
    //                 continue;
    //             }
                
    //             $active = false;
    //             $printMenuChild = '';
    //             foreach ($childMenu as $child) {
    //                 # code...
    //                 // echo $child;
    //                 if($routenameRequest == $child->routename ){
    //                     $active = true;
    //                     $printMenuChild .= '<li class="kt-menu__item  kt-menu__item--active" aria-haspopup="true"><a href="' . url("/") . $child->urlname . '" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">' . $child->menuname . '</span></a></li>';
    //                 }else{
    //                     $printMenuChild .= '<li class="kt-menu__item " aria-haspopup="true"><a href="' . url("/") . $child->urlname . '" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">' . $child->menuname . '</span></a></li>';
    //                 }
    //             }

    //             if($active == true){
    //                 $printParentMenu .= '<li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--open kt-menu__item--here" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-browser-2"></i><span class="kt-menu__link-text">'. $menu->menuname .'</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>' . 
    //                 '<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>' .
    //                     '<ul class="kt-menu__subnav">' .
    //                         '<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">' . $menu->menuname . '</span></span></li>';

    //             }else{
    //                 $printParentMenu .= '<li class="kt-menu__item  kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-browser-2"></i><span class="kt-menu__link-text">'. $menu->menuname .'</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>' . 
    //                 '<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>' .
    //                     '<ul class="kt-menu__subnav">' .
    //                         '<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">' . $menu->menuname . '</span></span></li>';

    //             }              
                
    //             $printParentMenu .= $printMenuChild;
    //             $printParentMenu .= '</ul></div></li>';
    //             $printMenu .= $printParentMenu;
    //             $active = false;
    //             $printParentMenu = '';
    //             $printMenuChild = '';
    //         // };
    //         // echo $menu;
    //     }
    //     // echo $printMenu;
    //     return $printMenu;
    // }

    public static function masterMenuBuilder() {
        $id_groups = Auth::user()->id_groups;
        // dd($id_groups);
        $dataMasterMenu = MasterMenu::
                            whereHas('mapgroupmenu', function($q) use ($id_groups){
                                $q->where('allow_view', '=', true)->where('id_groups', $id_groups);
                            })->
                            where('category', 'Z')->
                            get();
        // dd($dataMasterMenu);
        $masterMenu = "";
        if($dataMasterMenu->count() == 0){
            $masterMenu = "<h4 class='col-md-12 col-lg-12 text-center mb-4'>No Menu Found</h4>";
        }
        // dd($dataMasterMenu->count());
        for ($i=0; $i < count($dataMasterMenu); $i++) { 
            # code...
            $masterMenu = $masterMenu . 
            '<div class="col-md-3 col-lg-3">
                <a href="'. url("/") . $dataMasterMenu[$i]['urlname'] .'">
                    <div class="kt-portlet kt-callout kt-callout--brand kt-callout--diagonal-bg">
                        <div class="kt-portlet__body">
                            <div class="kt-callout__body">
                                <div class="kt-callout__content">
                                    <h3 class="kt-callout__title text-center">'. $dataMasterMenu[$i]['menuname'] .'</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>';
        }

        // $masterMenu = '<div class="col-md-3 col-lg-3">
        //                 <a href="http://www.google.com">
        //                     <div class="kt-portlet kt-callout kt-callout--brand kt-callout--diagonal-bg">
        //                         <div class="kt-portlet__body">
        //                             <div class="kt-callout__body">
        //                                 <div class="kt-callout__content">
        //                                     <h3 class="kt-callout__title text-center">User</h3>
        //                                 </div>
        //                             </div>
        //                         </div>
        //                     </div>
        //                 </a>
        //             </div>';

        return $masterMenu;
    }

    public static function getMenuName(){
        $current_url = Request::segment(1);
        $menus = Menus::where('url', $current_url)->select('nama')->first();
        if($menus){
            return $menus->nama;
        } else {
            return "";
        }
    }

    public static function getMessageType(){
        $option = Enum::getEnumbycategory('message_type');

        $html = '';
        foreach ($option as $key) {
            $html .= '<option value="'. $key->enum_code  .'">'. $key->enum_name .'</option>';
        }
        return $html;
    }

    public static function getMessagePriority(){
        $option = Enum::getEnumByCategory('message_priority');

        $html = '';
        foreach ($option as $key) {
            $html .= '<option value="'. $key->enum_code  .'">'. $key->enum_name .'</option>';
        }
        return $html;
    }

    public static function getLastMessage($number){
        $ticketHeader = TicketHeader::where('ticket_number', app('request')->input('number'))->first();
        $ticketDetail = TicketDetail::where('id_ticket_header', $ticketHeader->id)->get();


        $html = '';
        foreach ($option as $key) {
            $html .= '<option value="'. $key->enum_code  .'">'. $key->enum_name .'</option>';
        }
        return $html;
    }

    public static function getKategoriPemohon(){
        $option = Enum::getEnumByCategory('kategori_pemohon');

        $html = '';
        foreach ($option as $key) {
            $html .=    '<label class="kt-radio">
                            <input type="radio" name="kategori_pemohon" value="'. $key->enum_code  .'">'. $key->enum_name .'
                            <span></span>
                        </label>';
            // $html .= '<option value="'. $key->enum_code  .'">'. $key->enum_name .'</option>';
        }
        return $html;
    }

    public static function countMasterMenu(){
        $id_groups = Auth::user()->id_groups;

        $dataMasterMenu = MasterMenu::
                            whereHas('mapgroupmenu', function($q) use ($id_groups){
                                $q->where('allow_view', '=', true)->where('id_groups', $id_groups);
                            })->
                            where('category', 'Z')->
                            get();

        return count($dataMasterMenu);
    }
}
