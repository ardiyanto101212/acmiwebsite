<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterGroup extends Model
{
    //
    protected $table = 'master_groups';
    public $fillable = [ 
        'name'
    ];
    
    public $timestamps = true;
 

    public function user(){
        return $this->hasMany('App\Models\User', 'id', 'id_groups');
    }

    public function documentrule(){
        return $this->hasMany('App\Models\DocumentRule', 'id', 'ruleExisting');
    }
   
}
