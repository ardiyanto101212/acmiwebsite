<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use DB;
use Auth;
class Mprovince extends Model
{
    use SoftDeletes;
    //
    protected $table = 'indoregion_provinces';
    protected $dates = ['deleted_at'];
    
    public $fillable = [ 
        'province_id',
        'name'
    ];

    public $timestamps = true;

    public function qalldata(){
        date_default_timezone_set('Asia/Jakarta');

        $arayselect = [
            'id',
            'name'
        ];

        $searchdata = DB::table('indoregion_provinces')
        ->orderby('indoregion_provinces.name','ASC')
        ->select($arayselect)
        ->when(request()->search, function($searchdata) {
            $searchdata = $searchdata
            ->where(function($searchdata){
                $searchdata
                ->where('indoregion_provinces.name', 'like', '%'. request()->search . '%' )
                ;
            });
        })
        ->whereNull('indoregion_provinces.deleted_at')
        ->paginate(50);

        return $searchdata; 
    }

    public function qdataprovince(){
        date_default_timezone_set('Asia/Jakarta');

        $arayselect = [
            // 'id',
            'id as province_id',
            'name as provincename',
        ];

        $searchdata = DB::table('indoregion_provinces')
                    ->orderby('indoregion_provinces.name','ASC')
                    ->where('indoregion_provinces.name', '!=','-')
                    ->whereNull('indoregion_provinces.deleted_at')
                    ->select($arayselect)
                    ->get();

        return $searchdata; 
    }

    public function qteknisibyprovcity($value){
        date_default_timezone_set('Asia/Jakarta');

        $idcity     = $value['cityid'];
        $namakar    = $value['m_karyawan_nama'];

        $arayselect = [
            'm_karyawan.m_karyawan_idkaryawan',
            'm_karyawan.m_karyawan_nik',
            'm_karyawan.m_karyawan_nama',
            "m_title.m_title_name",
            "indoregion_provinces.name as provincename",
            "indoregion_regencies.name as cityname"
        ];

        $searchdata = DB::table('m_karyawan')
        ->join('m_title','m_karyawan.m_karyawan_title_id','m_title.m_title_id')
        ->join('indoregion_regencies','m_karyawan.m_karyawan_witel','indoregion_regencies.id')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        
        ->where('m_karyawan.m_karyawan_title_id','3')
        // ->where('m_karyawan.m_karyawan_witel',$idcity)
        ->where('indoregion_regencies.name',$idcity)
        ->whereNull('m_karyawan.deleted_at')
        ->where('m_karyawan.m_karyawan_nama', 'like', '%'. $namakar . '%' )
        ->select($arayselect)
        ->get();

        return $searchdata; 
    }
    

    public function qalldatatrash(){
        date_default_timezone_set('Asia/Jakarta');

        $arayselect = [
            'id',
            'name',
        ];

        $searchdata = DB::table('indoregion_provinces')
        ->orderby('indoregion_provinces.name','ASC')
        ->select($arayselect)
        ->when(request()->search, function($searchdata) {
            $searchdata = $searchdata
            ->where(function($searchdata){
                $searchdata
                ->where('indoregion_provinces.name', 'like', '%'. request()->search . '%' )
                ;
            });
        })
        ->whereNotNull('indoregion_provinces.deleted_at')
        ->paginate(50);

        return $searchdata; 

    }

    public function insertdata($data){
        date_default_timezone_set('Asia/Jakarta');
        $norandom = mt_rand(1000, 9999);
        $dataarray = [
            "id"           => date('His').$norandom,
            "name"         => $data['name']
        ];
        Mprovince::create($dataarray);

        return true;
    }

    public function updatedata($data){
        date_default_timezone_set('Asia/Jakarta');

        $dataarray = [
            "name"         => $data['name']
        ];
        Mprovince::where('id',$data['id'])->update($dataarray);

        return true;
    }

    public function deletedata($id){
        date_default_timezone_set('Asia/Jakarta');

        DB::table('indoregion_provinces')
        ->where('id',$id)
        ->update(['deleted_at' => Carbon::now()]);

        return true;
    }

    public function restoreprovince($id){
        date_default_timezone_set('Asia/Jakarta');

        Mprovince::withTrashed()
        ->where('id', $id)
        ->restore();

        return true;
    }

    public function getprovincebyid($id){
        date_default_timezone_set('Asia/Jakarta');
        
        $query = Mprovince::where('id', $id)->first();

        return $query;
    }
}
