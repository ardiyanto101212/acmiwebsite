<?php

/*
 * This file is part of the IndoRegion package.
 *
 * (c) Azis Hapidin <azishapidin.com | azishapidin@gmail.com>
 *
 */

namespace App\Models;

use AzisHapidin\IndoRegion\Traits\VillageTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\District;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use DB;
use Auth;

/**
 * Village Model.
 */
class Village extends Model
{
    use VillageTrait;
    use SoftDeletes;
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'indoregion_villages';
    public $fillable = [ 
        'id',
        'district_id',
        'name'
    ];
    
    public $timestamps = true;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'district_id'
    ];

	/**
     * Village belongs to District.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function qalldata(){
        date_default_timezone_set('Asia/Jakarta');

        $arayselect = [
            'indoregion_villages.id',
            'indoregion_villages.district_id',
            'indoregion_villages.name',
            'indoregion_districts.name as districtname',
            'indoregion_regencies.name as citiname',
            'indoregion_provinces.name as provincename'
        ];

        $searchdata = DB::table('indoregion_villages')
        ->join('indoregion_districts','indoregion_villages.district_id','indoregion_districts.id')
        ->join('indoregion_regencies','indoregion_districts.regency_id','indoregion_regencies.id')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->orderby('indoregion_villages.name','ASC')
        ->orderby('indoregion_districts.name','ASC')
        ->orderby('indoregion_regencies.name','ASC')
        ->orderby('indoregion_provinces.name','ASC')
        ->select($arayselect)
        ->when(request()->search, function($searchdata) {
            $searchdata = $searchdata
            ->where(function($searchdata){
                $searchdata
                ->where('indoregion_regencies.name', 'like', '%'. request()->search . '%' )
                ->orwhere('indoregion_provinces.name', 'like', '%'. request()->search . '%' )
                ->orwhere('indoregion_districts.name', 'like', '%'. request()->search . '%' )
                ->orwhere('indoregion_villages.name', 'like', '%'. request()->search . '%' )
                ;
            });
        })
        ->whereNull('indoregion_villages.deleted_at')
        ->paginate(50);

        return $searchdata; 
    }

    public function qalldatatrash(){
        date_default_timezone_set('Asia/Jakarta');

        $arayselect = [
            'indoregion_villages.id',
            'indoregion_villages.district_id',
            'indoregion_villages.name',
            'indoregion_districts.name as districtname',
            'indoregion_regencies.name as citiname',
            'indoregion_provinces.name as provincename'
        ];

        $searchdata = DB::table('indoregion_villages')
        ->join('indoregion_districts','indoregion_villages.district_id','indoregion_districts.id')
        ->join('indoregion_regencies','indoregion_districts.regency_id','indoregion_regencies.id')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->orderby('indoregion_villages.name','ASC')
        ->orderby('indoregion_districts.name','ASC')
        ->orderby('indoregion_regencies.name','ASC')
        ->orderby('indoregion_provinces.name','ASC')
        ->select($arayselect)
        ->when(request()->search, function($searchdata) {
            $searchdata = $searchdata
            ->where(function($searchdata){
                $searchdata
                ->where('indoregion_regencies.name', 'like', '%'. request()->search . '%' )
                ->orwhere('indoregion_provinces.name', 'like', '%'. request()->search . '%' )
                ->orwhere('indoregion_districts.name', 'like', '%'. request()->search . '%' )
                ->orwhere('indoregion_villages.name', 'like', '%'. request()->search . '%' )
                ;
            });
        })
        ->whereNotNull('indoregion_villages.deleted_at')
        ->paginate(50);

        return $searchdata; 
    }

    public function insertdata($data){
        date_default_timezone_set('Asia/Jakarta');
        $norandom = mt_rand(1000, 9999);
        $dataarray = [
            "id"           => date('His').$norandom,
            'district_id'  => $data['district_id'],
            "name"         => $data['name']
        ];
        Village::create($dataarray);

        return true;
    }

    public function updatedata($data){
        date_default_timezone_set('Asia/Jakarta');

        $dataarray = [
            'district_id'  => $data['district_id'],
            "name"         => $data['name']
        ];
        Village::where('id',$data['id'])->update($dataarray);

        return true;
    }

    public function deletedata($id){
        date_default_timezone_set('Asia/Jakarta');

        DB::table('indoregion_villages')
        ->where('id',$id)
        ->update(['deleted_at' => Carbon::now()]);

        return true;
    }

    public function restoredata($id){
        date_default_timezone_set('Asia/Jakarta');

        Village::withTrashed()
        ->where('id', $id)
        ->restore();

        return true;
    }

    public function getdatabyid($id){
        date_default_timezone_set('Asia/Jakarta');

        $query = Village::where('id', $id)->first();

        return $query;
    }

    public function getcitiprovincedistrict(){
        date_default_timezone_set('Asia/Jakarta');
        
        $select = [
            'indoregion_districts.id',
            'indoregion_districts.name as districtname',
            'indoregion_regencies.name as citiname',
            'indoregion_provinces.name as provincename'
        ];
        $qdata = DB::table('indoregion_districts')
        ->join('indoregion_regencies','indoregion_districts.regency_id','indoregion_regencies.id')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->whereNull('indoregion_districts.deleted_at')
        ->select($select)
        ->get();

        return $qdata;
    }

    public function getcitiprovincedistrictbyid($id){
        date_default_timezone_set('Asia/Jakarta');
        
        $select = [
            'indoregion_districts.id',
            'indoregion_districts.name as districtname',
            'indoregion_regencies.name as citiname',
            'indoregion_provinces.name as provincename'
        ];
        $qdata = DB::table('indoregion_districts')
        ->join('indoregion_regencies','indoregion_districts.regency_id','indoregion_regencies.id')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->where('indoregion_districts.id',$id)
        ->select($select)
        ->first();

        return $qdata;
    }

    public function qkelurahanbykecamatanname($request){
        date_default_timezone_set('Asia/Jakarta');
        
        $arayselect = [
            'indoregion_villages.id',
            'indoregion_villages.name as villagename',
            // 'indoregion_districts.name as districtname',
            // 'indoregion_regencies.name as citiname',
            // 'indoregion_provinces.name as provincename'
        ];

        $searchdata = DB::table('indoregion_villages')
        ->join('indoregion_districts','indoregion_villages.district_id','indoregion_districts.id')
        ->join('indoregion_regencies','indoregion_districts.regency_id','indoregion_regencies.id')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->whereNull('indoregion_villages.deleted_at')
        ->where('indoregion_districts.name',$request->districtname)
        ->groupby('indoregion_villages.name')
        ->orderby('indoregion_villages.name','ASC')
        ->select($arayselect)
        ->get();

        return $searchdata;
    }

    public function qgetareavillage($id){
        date_default_timezone_set('Asia/Jakarta');
        
        $arayselect = [
            'indoregion_villages.id',
            'indoregion_villages.name as villagename',
            'indoregion_districts.name as districtname',
            'indoregion_regencies.name as citiname',
            'indoregion_provinces.name as provincename'
        ];

        $searchdata = DB::table('indoregion_villages')
        ->join('indoregion_districts','indoregion_villages.district_id','indoregion_districts.id')
        ->join('indoregion_regencies','indoregion_districts.regency_id','indoregion_regencies.id')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->where('indoregion_villages.id',$id)
        ->orderby('indoregion_villages.name','ASC')
        ->select($arayselect)
        ->first();

        return $searchdata;
    }
}
