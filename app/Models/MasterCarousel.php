<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterCarousel extends Model
{
    protected $table = 'master_carousel';
    public $fillable = [ 
        'path_image',
        'title',
        'description'
    ];
    
    public $timestamps = true;
}
