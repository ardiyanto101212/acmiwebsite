<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enum extends Model
{
    protected $table = 'enum';
    public $fillable = [ 
        'enum_code',
        'enum_name', 
        'enum_category'
    ];
    
    public $timestamps = true;

    static function getEnumbycategory($category){
        // dd($category);
        return self::where('enum_category', $category)
            ->orderBy('enum_code', 'ASC')->get();
    }
}
