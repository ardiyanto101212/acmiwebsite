<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use DB;
use Auth;
class Mcitie extends Model
{
    use SoftDeletes;
    //
    protected $table = 'indoregion_regencies';
    protected $dates = ['deleted_at'];
    
    public $fillable = [ 
        'province_id',
        'name'
    ];

    public $timestamps = true;

    
    public function qalldata(){
        date_default_timezone_set('Asia/Jakarta');

        $arayselect = [
            'indoregion_regencies.id',
            'indoregion_regencies.name',
            'indoregion_regencies.province_id',
            'indoregion_provinces.name as provincename'
        ];

        $searchdata = DB::table('indoregion_regencies')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->orderby('indoregion_regencies.name','ASC')
        ->orderby('indoregion_provinces.name','ASC')
        ->select($arayselect)
        ->when(request()->search, function($searchdata) {
            $searchdata = $searchdata
            ->where(function($searchdata){
                $searchdata
                ->where('indoregion_regencies.name', 'like', '%'. request()->search . '%' )
                ->orwhere('indoregion_provinces.name', 'like', '%'. request()->search . '%' )
                ;
            });
        })
        ->whereNull('indoregion_regencies.deleted_at')
        ->paginate(50);

        return $searchdata; 
    }

    public function qalldatatrash(){
        date_default_timezone_set('Asia/Jakarta');

        $arayselect = [
            'indoregion_regencies.id',
            'indoregion_regencies.province_id',
            'indoregion_regencies.name',
            'indoregion_provinces.name as provincename'
        ];

        $searchdata = DB::table('indoregion_regencies')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->orderby('indoregion_regencies.name','ASC')
        ->orderby('indoregion_provinces.name','ASC')
        ->select($arayselect)
        ->when(request()->search, function($searchdata) {
            $searchdata = $searchdata
            ->where('indoregion_regencies.name', 'like', '%'. request()->search . '%' )
            ->orwhere('indoregion_provinces.name', 'like', '%'. request()->search . '%' )
           ;
        })
        ->whereNotNull('indoregion_regencies.deleted_at')
        ->paginate(50);

        return $searchdata; 
    }

    public function insertdata($data){
        date_default_timezone_set('Asia/Jakarta');
        $norandom = mt_rand(1000, 9999);
        $dataarray = [
            "id"           => date('His').$norandom,
            'province_id'  => $data['province_id'],
            "name"         => $data['name']
        ];
        Mcitie::create($dataarray);

        return true;
    }

    public function updatedata($data){
        date_default_timezone_set('Asia/Jakarta');

        $dataarray = [
            "province_id"  => $data['province_id'],
            "name"         => $data['name']
        ];
        Mcitie::where('id',$data['id'])->update($dataarray);

        return true;
    }

    public function deletedata($id){
        date_default_timezone_set('Asia/Jakarta');

        DB::table('indoregion_regencies')
        ->where('id',$id)
        ->update(['deleted_at' => Carbon::now()]);

        return true;
    }

    public function restoreprovince($id){
        date_default_timezone_set('Asia/Jakarta');

        Mcitie::withTrashed()
        ->where('id', $id)
        ->restore();

        return true;
    }

    public function getdatabyid($id){
        date_default_timezone_set('Asia/Jakarta');

        $query = Mcitie::where('id', $id)->first();

        return $query;
    }

    public function getdistrict(){
        date_default_timezone_set('Asia/Jakarta');

        $arrayselect = [
            'indoregion_districts.id',
            'indoregion_districts.name as districtname',
            'indoregion_provinces.name as province',
            'indoregion_regencies.name as namecity'
        ];

        $city   = Mcitie::join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
                ->orderby('indoregion_provinces.name','ASC')
                ->orderby('indoregion_regencies.name','ASC')
                ->select($arrayselect)
                ->get();
        
        return $city;
    }

    public function getcityprovince(){
        date_default_timezone_set('Asia/Jakarta');

        $arrayselect = [
            'indoregion_regencies.id',
            'indoregion_provinces.name as province',
            'indoregion_regencies.name as namecity'
        ];

        $city   = Mcitie::join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
                ->orderby('indoregion_provinces.name','ASC')
                ->orderby('indoregion_regencies.name','ASC')
                ->select($arrayselect)
                ->get();
        
        return $city;
    }

    public function getcitibyidprovince($id){
        date_default_timezone_set('Asia/Jakarta');
        
        $select = [
            'id as idcity',
            'name as namecity'
        ];
        
        $query = Mcitie::where('province_id',$id)
                ->whereNull('deleted_at')
                ->orderby('name','ASC')
                ->select($select)
                ->get();

        return $query;
    }

    public function qcitybyprovincename($value){
        date_default_timezone_set('Asia/Jakarta');

        $qcekprov = DB::table('indoregion_provinces')->where('name',$value['province_id'])->first();

        $select = [
            'id as idcity',
            'name as namecity'
        ];
     
        $query = Mcitie::where('province_id',$qcekprov->id)
                ->whereNull('deleted_at')
                ->orderby('name','ASC')
                ->select($select)
                ->get();

        return $query;
    }

    public function deletecitie($id){
        date_default_timezone_set('Asia/Jakarta');

        DB::table('indoregion_regencies')
        ->where('id',$id)
        ->update(['deleted_at' => Carbon::now()]);

        return true;
    }

    public function getprovince(){
        $qdata = DB::table('indoregion_provinces')
        ->whereNull('indoregion_provinces.deleted_at')
        ->get();

        return $qdata;
    }

    public function getcitybyid($id){
        date_default_timezone_set('Asia/Jakarta');
        
        $arrayselect = [
            'indoregion_regencies.id',
            'indoregion_provinces.name as province',
            'indoregion_regencies.name as namecity'
        ];

        $city   = Mcitie::join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
                ->where('indoregion_regencies.id',$id)
                ->select($arrayselect)
                ->first();
        
        return $city;
    }
    
    public function getallwitel(){
        date_default_timezone_set('Asia/Jakarta');

        $select = [
            'indoregion_regencies.id',
            'indoregion_regencies.name as citiname',
            'indoregion_provinces.name as provincename'
        ];
        $qdata = DB::table('indoregion_regencies')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->whereNull('indoregion_regencies.deleted_at')
        ->orderby('indoregion_regencies.name','ASC')
        ->orderby('indoregion_provinces.name','ASC')
        // ->where('indoregion_regencies.name', 'like', '%'. $request->witelname . '%' )
        ->select($select)
        ->get();

        return $qdata;
    }
}   
