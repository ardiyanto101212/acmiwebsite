<?php

/*
 * This file is part of the IndoRegion package.
 *
 * (c) Azis Hapidin <azishapidin.com | azishapidin@gmail.com>
 *
 */

namespace App\Models;

use AzisHapidin\IndoRegion\Traits\ProvinceTrait;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use DB;
use Auth;


/**
 * Province Model.
 */
class Province extends Model
{
    use ProvinceTrait;
    use SoftDeletes;
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'indoregion_provinces';

    public $fillable = [ 
        'name'
    ];
    
    public $timestamps = true;
    /**
     * Province has many regencies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function regencies()
    {
        return $this->hasMany(Regency::class);
    }
}
