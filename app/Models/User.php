<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use DB;
use Auth;

use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens, Notifiable;

    protected $table = 'master_users';
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'fullname', 'email', 
        'password', 'telpnumber', 'isActive', 
        'isVerify', 'tokenVerify', 'id_groups',
        'master_users_devicekey','master_users_devicekeyonof',
        'karyawanorother'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function group(){
        return $this->belongsTo('App\Models\MasterGroup', 'id_groups', 'id');
    }

    public function log(){
        return $this->hasMany('App\Models\Log', 'id', 'id_users');
    }

    public function qdaftarcustomerlogin($datapost){
        $dtarray = [
            'username'      => $datapost['m_customer_username'],
            'fullname'      => ucwords(strtolower($datapost['m_customer_nama'])),
            'email'         => strtolower($datapost['m_customer_email']),
            'password'      => bcrypt($datapost['m_customer_password']),
            'telpnumber'    => $datapost['m_customer_hp'],
            'id_groups'     => 5,
            'master_users_devicekeyonof'  => 'On',
            'karyawanorother' => '2'
        ];

        User::create($dtarray);

        return true;
    }

    public function  updatetokendevice($iduser){
        $select = [
            'master_users.id'
        ];
        $qkaryawan = DB::table('m_karyawan')
                    ->join('master_users','m_karyawan.m_karyawan_email','master_users.email')
                    ->where('m_karyawan.m_karyawan_idkaryawan',$request->m_karyawan_idkaryawan)
                    ->select($select)
                    ->first();

        if(!empty($qkaryawan->id)){
            User::where('id',$qkaryawan->id)
            ->update(['master_users_devicekey' => $request->master_users_devicekey]);
        }

        return true;
    }

    public function qgetonofnotification($id){
        $select = [
            'm_karyawan.m_karyawan_idkaryawan',
            'master_users.master_users_devicekeyonof'
        ];
        $qkaryawan = DB::table('m_karyawan')
                    ->join('master_users','m_karyawan.m_karyawan_email','master_users.email')
                    ->where('m_karyawan.m_karyawan_idkaryawan',$id)
                    ->select($select)
                    ->first();

        return $qkaryawan;
    }

    public function  qupdateonofnotification($request){
        $select = [
            'master_users.id'
        ];
        $qkaryawan = DB::table('m_karyawan')
                    ->join('master_users','m_karyawan.m_karyawan_email','master_users.email')
                    ->where('m_karyawan.m_karyawan_idkaryawan',$request->m_karyawan_idkaryawan)
                    ->select($select)
                    ->first();

        if(!empty($qkaryawan->id)){
            User::where('id',$qkaryawan->id)
            ->update(['master_users_devicekeyonof' => $request->master_users_devicekeyonof]);
        }

        return true;
    }

    public function editusernamebyemail($email,$username){
        User::where('email',$email)
            ->update(['username' => $username]);

        return true;
    }

    public function editpasswordbyemail($email,$password){
        User::where('email',$email)
            ->update(['password' => bcrypt($password)]);

        return true;
    }

    public function editemailcustomer($emaillama,$email){
        User::where('email',$emaillama)
            ->update(['email' => strtolower($email) ]);

        return true;
    }
    
}
