<?php

/*
 * This file is part of the IndoRegion package.
 *
 * (c) Azis Hapidin <azishapidin.com | azishapidin@gmail.com>
 *
 */

namespace App\Models;

use AzisHapidin\IndoRegion\Traits\DistrictTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Regency;
use App\Models\Village;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use DB;
use Auth;

/**
 * District Model.
 */
class District extends Model
{
    use DistrictTrait;
    use SoftDeletes;
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'indoregion_districts';
    public $fillable = [ 
        'regency_id',
        'name'
    ];
    
    public $timestamps = true;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'regency_id'
    ];

    /**
     * District belongs to Regency.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }

    /**
     * District has many villages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function villages()
    {
        return $this->hasMany(Village::class);
    }

    public function qalldata(){
        date_default_timezone_set('Asia/Jakarta');

        $arayselect = [
            'indoregion_districts.id',
            'indoregion_districts.regency_id',
            'indoregion_districts.name',
            'indoregion_regencies.name as citiname',
            'indoregion_provinces.name as provincename'
        ];

        $searchdata = DB::table('indoregion_districts')
        ->join('indoregion_regencies','indoregion_districts.regency_id','indoregion_regencies.id')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->orderby('indoregion_districts.name','ASC')
        ->orderby('indoregion_regencies.name','ASC')
        ->orderby('indoregion_provinces.name','ASC')
        ->select($arayselect)
        >when(request()->search, function($searchdata) {
            $searchdata = $searchdata
            ->where(function($searchdata){
                $searchdata
            ->where('indoregion_regencies.name', 'like', '%'. request()->search . '%' )
            ->orwhere('indoregion_provinces.name', 'like', '%'. request()->search . '%' )
            ->orwhere('indoregion_districts.name', 'like', '%'. request()->search . '%' )
            ;
            });
        })
        ->whereNull('indoregion_districts.deleted_at')
        ->paginate(50);

        return $searchdata; 
    }

    public function qalldatatrash(){
        date_default_timezone_set('Asia/Jakarta');
        $arayselect = [
            'indoregion_districts.id',
            'indoregion_districts.regency_id',
            'indoregion_districts.name',
            'indoregion_regencies.name as citiname',
            'indoregion_provinces.name as provincename'
        ];

        $searchdata = DB::table('indoregion_districts')
        ->join('indoregion_regencies','indoregion_districts.regency_id','indoregion_regencies.id')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->orderby('indoregion_districts.name','ASC')
        ->orderby('indoregion_regencies.name','ASC')
        ->orderby('indoregion_provinces.name','ASC')
        ->select($arayselect)
        ->when(request()->search, function($searchdata) {
            $searchdata = $searchdata
            ->where(function($searchdata){
                $searchdata
            ->where('indoregion_regencies.name', 'like', '%'. request()->search . '%' )
            ->orwhere('indoregion_provinces.name', 'like', '%'. request()->search . '%' )
            ->orwhere('indoregion_districts.name', 'like', '%'. request()->search . '%' )
            ;
            });
        })
        ->whereNotNull('indoregion_districts.deleted_at')
        ->paginate(50);

        return $searchdata; 
    }

    public function insertdata($data){
        date_default_timezone_set('Asia/Jakarta');
        $norandom = mt_rand(1000, 9999);
        $dataarray = [
            "id"           => date('His').$norandom,
            'regency_id'   => $data['regency_id'],
            "name"         => $data['name']
        ];
        District::create($dataarray);

        return true;
    }

    public function updatedata($data){
        date_default_timezone_set('Asia/Jakarta');

        $dataarray = [
            'regency_id'   => $data['regency_id'],
            "name"         => $data['name']
        ];
        District::where('id',$data['id'])->update($dataarray);

        return true;
    }

    public function deletedata($id){
        date_default_timezone_set('Asia/Jakarta');

        DB::table('indoregion_districts')
        ->where('id',$id)
        ->update(['deleted_at' => Carbon::now()]);

        return true;
    }

    public function restoredata($id){
        date_default_timezone_set('Asia/Jakarta');

        District::withTrashed()
        ->where('id', $id)
        ->restore();

        return true;
    }

    public function getdatabyid($id){
        date_default_timezone_set('Asia/Jakarta');

        $query = District::where('id', $id)->first();

        return $query;
    }

    public function getcitiprovince(){
        date_default_timezone_set('Asia/Jakarta');

        $select = [
            'indoregion_regencies.id',
            'indoregion_regencies.name as citiname',
            'indoregion_provinces.name as provincename'
        ];
        $qdata = DB::table('indoregion_regencies')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->whereNull('indoregion_regencies.deleted_at')
        ->select($select)
        ->get();

        return $qdata;
    }

    public function getcitiprovincedistrict(){
        date_default_timezone_set('Asia/Jakarta');
        $select = [
            'indoregion_districts.id',
            'indoregion_districts.name as districtname',
            'indoregion_regencies.name as citiname',
            'indoregion_provinces.name as provincename'
        ];
        $qdata = DB::table('indoregion_districts')
        ->join('indoregion_regencies','indoregion_districts.regency_id','indoregion_regencies.id')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->whereNull('indoregion_districts.deleted_at')
        ->select($select)
        ->get();

        return $qdata;
    }

    public function qkecamatanbycityname($request){
        date_default_timezone_set('Asia/Jakarta');
        $select = [
            'indoregion_districts.id',
            'indoregion_districts.name as districtname'
        ];
        $qdata = DB::table('indoregion_districts')
        ->join('indoregion_regencies','indoregion_districts.regency_id','indoregion_regencies.id')
        ->join('indoregion_provinces','indoregion_regencies.province_id','indoregion_provinces.id')
        ->whereNull('indoregion_districts.deleted_at')
        ->where('indoregion_regencies.name',$request->cityname)
        ->orderby('indoregion_districts.name','ASC')
        ->select($select)
        ->get();

        return $qdata;
    }

}
